# AccreditationService

main branch build
https://www.onestepprojects.tk/accreditation/

test branch build
https://test.onestepprojects.tk/accreditation/


How to start the AccreditationService application locally
---

1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/accreditation-1.0-SNAPSHOT.jar server config.yml`
1. To check that your application is running enter url `http://localhost:8080`

Health Check
---

To see your applications health enter url `http://localhost:8081/healthcheck`

## React JS
files located in `.../java/resources/assets/accreditation-react`
