package integration;

import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.testing.DropwizardTestSupport;
import io.dropwizard.testing.ResourceHelpers;
import org.eclipse.jetty.http.HttpStatus;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.HttpUrlConnectorProvider;
import org.junit.Ignore;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.onestep.relief.utility.accreditation.AccreditationServiceApplication;
import org.onestep.relief.utility.accreditation.AccreditationServiceConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

public class IntegrationTestIT {
    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("test-config.yml");

    public static final DropwizardTestSupport<AccreditationServiceConfiguration> SUPPORT =
            new DropwizardTestSupport<>(AccreditationServiceApplication.class,
                    CONFIG_PATH);

    private static Logger logger = LoggerFactory.getLogger(IntegrationTestIT.class);

    static Client client = null;

    @BeforeAll
    static public void setUp() {
        try {
            SUPPORT.before();

            client = new JerseyClientBuilder(SUPPORT.getEnvironment()).build("test client");
            client.property(ClientProperties.CONNECT_TIMEOUT, 4000);
            client.property(ClientProperties.READ_TIMEOUT, 4000 );

        } catch (Exception e) {
            logger.info(e.getMessage());

        }
    }

    @AfterAll
    static public void tearDown() {
        SUPPORT.after();
    }

    @Disabled
    @org.junit.jupiter.api.Test
    public void testGetApplication() {
        Response response = client.target(
                String.format("http://localhost:%d/onestep/accreditation/application?state=Pending", SUPPORT.getLocalPort()))
                .request().get();
        assertEquals(HttpStatus.OK_200, response.getStatus());
    }

    @org.junit.jupiter.api.Test
    public void testSubmitApplication() {
        // TODO: to be implemented.
//        Response response = client.target(
//                String.format("http://localhost:%d/o/onestep/accreditation/application", SUPPORT.getLocalPort()))
//                .request().post(Entity.json(""));
//        assertEquals(HttpStatus.OK_200, response.getStatus());
    }

    @org.junit.jupiter.api.Test
    public void testDeleteApplication() {
        Response response = client.target(
                String.format("http://localhost:%d/onestep/accreditation/application/b00e-18411c72", SUPPORT.getLocalPort()))
                .request().delete();
        assertEquals(HttpStatus.NO_CONTENT_204, response.getStatus());

        // TODO: Test the successful path.
    }

    @org.junit.jupiter.api.Test
    public void testRoleDocumentation() {
        Response response = client.target(
                String.format("http://localhost:%d/onestep/accreditation/application/roleDocumentation", SUPPORT.getLocalPort()))
                .request().get();
        assertEquals(HttpStatus.OK_200, response.getStatus());
    }

    @Ignore
    public void testGetUserInfo() {
        Response response = client.target(
                String.format("http://localhost:%d/onestep/accreditation/application/user/b00e-18411c72", SUPPORT.getLocalPort()))
                .request().get();
        assertEquals(HttpStatus.OK_200, response.getStatus());
    }

    @Ignore
    public void testUserGetUserInfo() {
        Response response = client.target(
                String.format("http://localhost:%d/user/b00e-18411c72", SUPPORT.getLocalPort()))
                .request().get();
        assertEquals(HttpStatus.OK_200, response.getStatus());
    }

}