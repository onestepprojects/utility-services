package org.onestep.relief.utility.accreditation.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import static org.onestep.relief.utility.accreditation.util.AccreditationConstants.*;

public class EnvironVarTest {



    /**
     * Basic regression test to ensure that they are working correctly and constants are being set correctly
     */
    @Test
    public void getEnvironTest() {

        String result = Config.getEnv("ACCREDITATION_S3_ACCESS_KEY");
        Assertions.assertSame(ACCREDITATION_S3_ACCESS_KEY,result);


        result = Config.getEnv("ACCREDITATION_S3_SECRET_ACCESS_KEY");
        Assertions.assertSame(ACCREDITATION_S3_SECRET_ACCESS_KEY,result);

        result = Config.getEnv("ACCREDITATION_DB_ACCESS_KEY");
        Assertions.assertSame(ACCREDITATION_DB_ACCESS_KEY,result);

        result = Config.getEnv("ACCREDITATION_DB_SECRET_ACCESS_KEY");
        Assertions.assertSame(ACCREDITATION_DB_SECRET_ACCESS_KEY,result);

        result = Config.getEnv("BLOCKCHAIN_SECRETSMANAGER_ID");
        Assertions.assertSame(BLOCKCHAIN_SECRETSMANAGER_ID,result);

        result = Config.getEnv("BLOCKCHAIN_SECRETSMANAGER_SECRET");
        Assertions.assertSame(BLOCKCHAIN_SECRETSMANAGER_SECRET,result);

        result = Config.getEnv("ACCREDITATION_KMS_ACCESS_KEY");
        Assertions.assertSame(ACCREDITATION_KMS_ACCESS_KEY,result);

        result = Config.getEnv("ACCREDITATION_KMS_SECRET_ACCESS");
        Assertions.assertSame(ACCREDITATION_KMS_SECRET_ACCESS,result);

        result = Config.getEnv("ACCREDITATION_KMS_CMK_STRING");
        Assertions.assertSame(ACCREDITATION_KMS_CMK_STRING,result);

        result = Config.getEnv("ACCREDITATION_SERVICE_LOGIN_ID");
        Assertions.assertSame(ACCREDITATION_SERVICE_LOGIN_ID,result);

        result = Config.getEnv("ACCREDITATION_SERVICE_LOGIN_KEY");
        Assertions.assertSame(ACCREDITATION_SERVICE_LOGIN_KEY,result);
        

    }

    @Test
    public void getEnvironVarNotNullTest() {

        Assertions.assertNotNull(ACCREDITATION_S3_ACCESS_KEY);

        Assertions.assertNotNull(ACCREDITATION_S3_SECRET_ACCESS_KEY);

        Assertions.assertNotNull(ACCREDITATION_DB_ACCESS_KEY);

        Assertions.assertNotNull(ACCREDITATION_DB_SECRET_ACCESS_KEY);

        Assertions.assertNotNull(BLOCKCHAIN_SECRETSMANAGER_ID);

        Assertions.assertNotNull(BLOCKCHAIN_SECRETSMANAGER_SECRET);

        Assertions.assertNotNull(ACCREDITATION_KMS_ACCESS_KEY);

        Assertions.assertNotNull(ACCREDITATION_KMS_SECRET_ACCESS);

        Assertions.assertNotNull(ACCREDITATION_KMS_CMK_STRING);

        Assertions.assertNotNull(ACCREDITATION_SERVICE_LOGIN_ID);

        Assertions.assertNotNull(ACCREDITATION_SERVICE_LOGIN_KEY);
        
    }
}
