package org.onestep.relief.utility.accreditation.service.impl;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.onestep.relief.utility.accreditation.db.ApplicationDBService;
import org.onestep.relief.utility.accreditation.dto.RolesResponse;
import org.onestep.relief.utility.accreditation.service.IAccreditationService;
import org.onestep.relief.utility.accreditation.util.IssueReward;
import org.onestep.relief.utility.accreditation.util.RewardsClient;

import java.io.IOException;

public class ServiceTest {

    IAccreditationService service = new AccreditationService(new ApplicationDBService(),
            new IssueReward(new RewardsClient()));

    /**
     * Simple test to ensure that it's returning proper response type
     */
    @Test
    public void getUniqueRolesTest() throws IOException {

        // Pass in any string to return all roles
        RolesResponse results = service.listUniqueRoles("test");

        Assertions.assertNotNull(results);
    }


}
