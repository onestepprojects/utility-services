package org.onestep.relief.utility.accreditation.db;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import org.onestep.relief.utility.accreditation.model.Accreditation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static org.onestep.relief.utility.accreditation.util.AccreditationConstants.*;


public class ApplicationDBService implements IApplicationDBService {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationDBService.class);
    DynamoDBMapper dynamoDb;
    AmazonDynamoDB client;

    @Inject
    public ApplicationDBService() {

        client = AmazonDynamoDBClient.builder()
                .withRegion(DB_REGION)
                .withCredentials(new AWSStaticCredentialsProvider(new
                        BasicAWSCredentials(ACCREDITATION_DB_ACCESS_KEY, ACCREDITATION_DB_SECRET_ACCESS_KEY)))
                .build();

        this.dynamoDb = new DynamoDBMapper(client);
    }

    /**
     * Retrieves specific application
     *
     * @param accreditationId Accreditation object to return. This contains all of the application information
     * @return The request Accreditation object
     */
    @Override
    public Accreditation load(String accreditationId) {
        return this.dynamoDb.load(Accreditation.class, accreditationId);
    }


    /**
     * Saves the application object to the database
     *
     * @param accreditation Accreditation object to save. This contains all of the application information
     * @return The accreditationId (primary key) of the saved object.
     */
    @Override
    public String save(Accreditation accreditation) {
        this.dynamoDb.save(accreditation);
        return accreditation.getAccreditationId();
    }


    /**
     * DynamoDB call to delete an application based upon the accreditationId (app ID)
     *
     * @param AccreditationId Application to delete
     */
    @Override
    public void delete(String AccreditationId) {
        Accreditation accreditation = load(AccreditationId);
        if (accreditation != null) {
            this.dynamoDb.delete(accreditation);
        }
    }


    /**
     * Retrieves List of applications based upon state; option query parameters for entity type and
     * starting date of application.
     *
     * @param state           String representing value of enumerated states, i.e. Pending, Approved
     * @param entityType      Optional string representing either ORG or PER to only return
     *                        applications from those entity types
     * @param applicationDate Optional string in yyyy-mm-dd format to filter by applications that
     *                        are from that date and later
     * @return List of Accreditation objects meeting query parameters
     */
    @Override
    public List<Accreditation> queryByState(String state, String entityType, String applicationDate) {

        final String INDEX = ACCREDITATION_ENTITYTYPE_INDEX;
        // User can't access these applications
        if (state.equalsIgnoreCase(DELETED)) {
            return Collections.emptyList();
        }

        Accreditation a = new Accreditation();
        a.setAccreditationState(Accreditation.AccreditationStateEnum.fromValue(state));

        DynamoDBQueryExpression<Accreditation> queryExpression =
                new DynamoDBQueryExpression<Accreditation>()
                        .withHashKeyValues(a)
                        .withIndexName(INDEX)
                        .withConsistentRead(false);

        // Modify query expression if ORG or PER is passed in
        if (entityType != null) {
            entityType = "\"" + entityType.toUpperCase() + "\""; //Hack for the way our enums are stored in DynamoDb
            queryExpression.withRangeKeyCondition("entityType",
                    new Condition()
                            .withComparisonOperator(ComparisonOperator.EQ)
                            .withAttributeValueList(new AttributeValue().withS(entityType)));
        }
        // Modify query expression to filter on date if this is passed in.
        if (applicationDate != null) {
            HashMap<String, String> nameMap = new HashMap<>();
            nameMap.put("#dt", "date");

            HashMap<String, AttributeValue> valueMap = new HashMap<>();
            valueMap.put(":startDate", new AttributeValue().withS(applicationDate));


            queryExpression
                    .withFilterExpression("#dt >= :startDate")
                    .withExpressionAttributeNames(nameMap)
                    .withExpressionAttributeValues(valueMap);
        }
        return this.dynamoDb.query(Accreditation.class, queryExpression);
    }


    /**
     * Retrieves List of applications for specified entity.
     *
     * @param entityId Entity (PER or ORG) to retrieve based upon their ID
     * @return List of all applications for that entity
     */
    @Override
    public List<Accreditation> queryByEntityId(String entityId) {

        final String INDEX = ENTITY_ID_INDEX;
        Accreditation accreditation = new Accreditation();
        accreditation.setEntityId(entityId);

        // Don't show "Revoked" applications...

        HashMap<String, AttributeValue> valueMap = new HashMap<>();
        valueMap.put(":status", new AttributeValue().withS("\"" + DELETED + "\""));


        DynamoDBQueryExpression<Accreditation> queryExpression =
                new DynamoDBQueryExpression<Accreditation>()
                        .withHashKeyValues(accreditation)
                        .withIndexName(INDEX)
                        .withFilterExpression("accreditationState <> :status")
                        .withExpressionAttributeValues(valueMap)
                        .withConsistentRead(false);

        return this.dynamoDb.query(Accreditation.class, queryExpression);
    }

    /**
     * Retrieves specific application
     *
     * @param applicationId Returns a single Accreditation application based upon its ID
     * @return Single accreditation application
     * @see #load(String) - Appears to be duplicate of functionality
     */
    @Override
    public Accreditation queryByApplicationId(String applicationId) throws IndexOutOfBoundsException {

        List<Accreditation> results;
        Accreditation a = new Accreditation();
        a.setAccreditationId(applicationId);

        // If app is Revoked then don't return it
        HashMap<String, AttributeValue> valueMap = new HashMap<>();
        valueMap.put(":status", new AttributeValue().withS("\"" + DELETED + "\""));


        DynamoDBQueryExpression<Accreditation> queryExpression =
                new DynamoDBQueryExpression<Accreditation>()
                        .withHashKeyValues(a)
                        .withConsistentRead(false)
                        .withFilterExpression("accreditationState <> :status")
                        .withExpressionAttributeValues(valueMap)
                        .withLimit(20);

        results = this.dynamoDb.query(Accreditation.class, queryExpression);


        try {
            return results.get(0);
        } catch (IndexOutOfBoundsException e) {
            throw new IndexOutOfBoundsException("Accreditation ID is invalid");
        }
    }

    @Override
    public List<Accreditation> retrieveAllApplications() {

        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression();

        List<Accreditation> accreditations = this.dynamoDb.scan(Accreditation.class, scanExpression);

        //Scan does lazy loading so need to iterate for eager loading
        for (Accreditation accreditation : accreditations) {
            logger.info("Loaded accreditation " + accreditation.getAccreditationId());
        }

        return accreditations;
    }

    //TODO Add pagination
    @Override
    public List<Accreditation> queryByRole(String role) {

        final String INDEX = ROLE_INDEX;
        Accreditation accreditation = new Accreditation();
        accreditation.setRole(Accreditation.RoleEnum.fromValue(role));

        // If app is Revoked then don't return it
        HashMap<String, AttributeValue> valueMap = new HashMap<>();
        valueMap.put(":status", new AttributeValue().withS("\"" + DELETED + "\""));

        DynamoDBQueryExpression<Accreditation> queryExpression =
                new DynamoDBQueryExpression<Accreditation>()
                        .withHashKeyValues(accreditation)
                        .withIndexName(INDEX)
                        .withFilterExpression("accreditationState <> :status")
                        .withExpressionAttributeValues(valueMap)
                        .withConsistentRead(false);

        return this.dynamoDb.query(Accreditation.class, queryExpression);
    }

    /**
     * This query is used by the Accreditation UI (user) to populate the dropdown
     * list of available roles to apply for. If a user/org has already applied for a role
     * then it won't be shown to the end user (so they can't apply a second time).
     * Accreditation service takes this list and refines it to unique roles.
     *
     * @param entityId Entity ID of user or organization
     * @return List of roles user/org already has applied for. these will be
     * subtracted from the master list of roles.
     */
    @Override
    public List<String> queryByEntityIdUniqueRoles(String entityId) {

        final String INDEX = ENTITY_ID_INDEX;
        entityId = (entityId == null) ? " " : entityId; // Possible that no entityId will be passed in

        Accreditation accreditation = new Accreditation();
        accreditation.setEntityId(entityId);

        HashMap<String, String> nameMap = new HashMap<>();
        nameMap.put("#userRole", "role");


        DynamoDBQueryExpression<Accreditation> queryExpression =
                new DynamoDBQueryExpression<Accreditation>()
                        .withHashKeyValues(accreditation)
                        .withIndexName(INDEX)
                        .withProjectionExpression("#userRole")
                        .withExpressionAttributeNames(nameMap)
                        .withConsistentRead(false);

        return this.dynamoDb.query(Accreditation.class, queryExpression).stream()
                .filter(r -> r.getRole() != null)
                .map(r -> r.getRole().toString())
                .collect(Collectors.toList());
    }
}

