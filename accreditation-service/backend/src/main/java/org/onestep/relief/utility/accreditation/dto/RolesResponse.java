package org.onestep.relief.utility.accreditation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RolesResponse {

    private List<RoleResponse> roles = new ArrayList<>();

    public void setRoles(List<RoleResponse> aRoles) {
        this.roles = aRoles;
    }

}
