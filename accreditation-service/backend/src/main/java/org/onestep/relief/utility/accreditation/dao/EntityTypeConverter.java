package org.onestep.relief.utility.accreditation.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.onestep.relief.utility.accreditation.model.Accreditation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EntityTypeConverter implements DynamoDBTypeConverter<String, Accreditation.EntityTypeEnum> {

    private static final ObjectMapper mapper = new ObjectMapper();
    private static Logger logger = LoggerFactory.getLogger(EntityTypeConverter.class);

    @Override
    public String convert(Accreditation.EntityTypeEnum entityTypeEnum) {
        try {
            return mapper.writeValueAsString(entityTypeEnum);
        } catch (JsonProcessingException e) {
            logger.info(e.getMessage());
        }
        return null;
    }

    @Override
    public Accreditation.EntityTypeEnum unconvert(String s) {

        try {
            return mapper.readValue(s, Accreditation.EntityTypeEnum.class);
        } catch (JsonProcessingException e) {
            logger.info(e.getMessage());
        }
        return null;
    }
}
