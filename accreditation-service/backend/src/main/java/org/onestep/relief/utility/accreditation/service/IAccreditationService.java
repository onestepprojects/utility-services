package org.onestep.relief.utility.accreditation.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.onestep.relief.utility.accreditation.dto.AccreditationRequest;
import org.onestep.relief.utility.accreditation.dto.AccreditationResponse;
import org.onestep.relief.utility.accreditation.dto.NoteDTO;
import org.onestep.relief.utility.accreditation.dto.RolesResponse;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface IAccreditationService {

    AccreditationResponse createOrUpdateApplication(List<FormDataBodyPart> parts,
                                                    AccreditationRequest accreditationRequest);

    AccreditationResponse accreditationApplicationApprove(String accreditationId, NoteDTO adminNote)
            throws IndexOutOfBoundsException, JsonProcessingException;

    AccreditationResponse accreditationApplicationReject(String accreditationId, NoteDTO adminNote)
            throws IndexOutOfBoundsException, JsonProcessingException;

    void accreditationApplicationDelete(String accreditationId) throws Exception;

    List<AccreditationResponse> listApplicationsByState(String state, String entityType,
                                                String startDate);

    List<AccreditationResponse> listApplicationsByEntity(String entityId);

    List<AccreditationResponse> retrieveAllApplications();

    List<AccreditationResponse> listApplicationsByRole(String role);

    AccreditationResponse listByApplicationId(String accreditationId) throws IOException;

    RolesResponse listUniqueRoles(String entityId) throws IOException;

    boolean applicationAudit(String fileName, String accreditationId);

}
