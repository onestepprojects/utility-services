package org.onestep.relief.utility.accreditation.util;

public class Constants {

    public static final String BUCKETSUFFIX = Config.getProperty("BUCKETSUFFIX");
    public static final String TEMPFILES = Config.getProperty("TEMPFILES");
    public static final String BLOCKCHAIN_URI = Config.getProperty("BLOCKCHAIN_URI");
    public static final String OUTPUT_FORMAT = Config.getProperty("OUTPUT_FORMAT");
    public static final Integer OUTPUT_SIZE = Integer.parseInt(Config.getProperty("OUTPUT_SIZE"));
    public static final String KEY_SUFFIX = Config.getProperty("KEY_SUFFIX");
    public static final String PATH_SLASH = Config.getProperty("PATH_SLASH");
    public static final String TXN_URI = Config.getProperty("TXN_URI");
    public static final String TXN_PREFIX = Config.getProperty("TXN_PREFIX");
    public static final String SEND_ALGO_ROUTE = Config.getProperty("SEND_ALGO_ROUTE");
}
