package org.onestep.relief.utility.accreditation.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.onestep.relief.utility.accreditation.model.Accreditation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoleEnumTypeConverter implements DynamoDBTypeConverter<String, Accreditation.RoleEnum> {

    private static final ObjectMapper mapper = new ObjectMapper();
    private static Logger logger = LoggerFactory.getLogger(RoleEnumTypeConverter.class);



    @Override
    public String convert(Accreditation.RoleEnum roleEnum) {
        try {
            return mapper.writeValueAsString(roleEnum);
        } catch (JsonProcessingException e) {
            logger.info(e.getMessage());
        }
        return null;
    }

    @Override
    public Accreditation.RoleEnum unconvert(String s) {

        try {
            return mapper.readValue(s, Accreditation.RoleEnum.class);
        } catch (JsonProcessingException e) {
            logger.info(e.getMessage());
        }
        return null;
    }
}
