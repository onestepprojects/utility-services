package org.onestep.relief.utility.accreditation.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Transaction implements Entity{

    String address;
    String mnemonic;
    Integer amount = 0;
    String transactionType = "pay";
    String note;
    @JsonIgnore
    int date;

    public Transaction(String address, String mnemonic, String note) {
        this.address = address;
        this.mnemonic = mnemonic;
        this.note = note;
    }

    @Override
    public String toString() {
        return "{\"sender\":{"
                + "\"address\":\"" + address + "\""
                + ",\"mnemonic\":\"" + mnemonic + "\"}"
                + ",\"receiver\":{"
                + "\"address\":\"" + address + "\"}"
                + ",\"amount\":\"" + amount + "\""
                + ",\"note\":\"" + note + "\""
                + "}}";
    }
}

