package org.onestep.relief.utility.accreditation.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DocumentResponse {

    private String date;

    private String docType;

    private String fileName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String tempLink;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String tempLinkThumb;

}
