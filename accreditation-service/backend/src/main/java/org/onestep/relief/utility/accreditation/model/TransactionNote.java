

package org.onestep.relief.utility.accreditation.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Transaction note.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionNote implements Entity {

    @JsonProperty("hash")
    private String hash = null;

    @JsonProperty("fileName")
    private String fileName = null;

    /**
     * Builder pattern to make setting multiple items easier
     */
    public TransactionNote withHash(String hash) {
        this.hash = hash;
        return this;
    }

    public TransactionNote withFilename(String fileName) {
        this.fileName = fileName;
        return this;
    }


    @Override
    public String toString() {
        return "{"
                + "\\\"hash\\\":\\\"" + hash + "\\\""
                + ",\\\"fileName\\\":\\\"" + fileName + "\\\""
                + "}";
    }
}

