package org.onestep.relief.utility.accreditation.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.onestep.relief.utility.accreditation.model.Accreditation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AccreditationStateConverter implements DynamoDBTypeConverter<String, Accreditation.AccreditationStateEnum> {

    private static final ObjectMapper mapper = new ObjectMapper();
    private static Logger logger = LoggerFactory.getLogger(AccreditationStateConverter.class);

    @Override
    public String convert(Accreditation.AccreditationStateEnum accreditationStateEnum) {
        try {
            return mapper.writeValueAsString(accreditationStateEnum);
        } catch (JsonProcessingException e) {
            logger.info(e.getMessage());
        }
        return null;
    }

    @Override
    public Accreditation.AccreditationStateEnum unconvert(String s) {

        try {
            return mapper.readValue(s, Accreditation.AccreditationStateEnum.class);
        } catch (JsonProcessingException e) {
            logger.info(e.getMessage());
        }
        return null;
    }
}
