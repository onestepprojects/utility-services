package org.onestep.relief.utility.accreditation.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.kms.KmsClient;
import software.amazon.awssdk.services.kms.model.*;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.ArrayList;
import java.util.List;

import static org.onestep.relief.utility.accreditation.util.AccreditationConstants.*;


public class KMSOperations {

    private static final Logger logger = LoggerFactory.getLogger(KMSOperations.class);

    final static String key = ACCREDITATION_KMS_ACCESS_KEY;
    final static String secretKey = ACCREDITATION_KMS_SECRET_ACCESS;
    final static String keyArn = ACCREDITATION_KMS_CMK_STRING;
    final static String ALGORITHM = "AES";

    public static KmsClient kmsClient;


    static {
        AwsBasicCredentials awsCreds = AwsBasicCredentials.create(key,
                secretKey);
        Region region = Region.of(KMS_REGION_RT);
        kmsClient = KmsClient.builder()
                .credentialsProvider(StaticCredentialsProvider.create(awsCreds))
                .region(region).build();
    }


    /**
     * @param fileToEncrypt byte array of file we are going to encrypt
     * @return List containing two encrypted objects. The encrypted encryption key at position 0
     *          and the encrypted file at position 1. We need to save the key with the file to
     *          decrypted later.
     */
    public static List<byte[]> encryptUsingDataKey(byte[] fileToEncrypt) {
        GenerateDataKeyResponse generateDataKeyResponse;
        List<byte[]> encryptedData= new ArrayList<>();

        try {
            GenerateDataKeyRequest generateDataKeyRequest = GenerateDataKeyRequest
                    .builder().keyId(keyArn)
                    .keySpec(DataKeySpec.AES_256).build();
            generateDataKeyResponse = kmsClient
                    .generateDataKey(generateDataKeyRequest);


            SecretKeySpec key = new
                    SecretKeySpec(generateDataKeyResponse.plaintext().asByteArray(),
                    ALGORITHM);
            Cipher cipher;
            cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, key);
//            encryptedData.add(0,key.getEncoded());
            encryptedData.add(0,generateDataKeyResponse.ciphertextBlob().asByteArray());
            encryptedData.add(1,cipher.doFinal(fileToEncrypt));

        } catch (Exception ex) {
            logger.warn("Unable to perform encrypt " + ex.getMessage());
        }
        return encryptedData;

    }


    /**
     * @param encryptedKey Byte array of encrypted encryption key
     * @param encryptedFile Byte array of encrypted file
     * @return byte array of de-crypted file
     */
    public static  byte[] decryptUsingDataKey(byte[] encryptedKey, byte[] encryptedFile) {

        byte[] decryptedFileName = null;

        try {

            SdkBytes key = SdkBytes.fromByteArray(encryptedKey);
            System.out.println(key);
            DecryptRequest decryptRequest = DecryptRequest.builder()
                    .keyId(keyArn)
                    .ciphertextBlob(key).build();
            DecryptResponse decryptResponse = kmsClient
                    .decrypt(decryptRequest);

            SecretKeySpec secretKeySpec = new SecretKeySpec(decryptResponse
                    .plaintext().asByteArray(), ALGORITHM);

            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            decryptedFileName = cipher.doFinal(encryptedFile);


        } catch(Exception ex) {
            logger.warn("Unable to perform decryption " +  ex.getMessage());
        }
        return decryptedFileName;
    }
}

