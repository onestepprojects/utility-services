package org.onestep.relief.utility.accreditation.service.impl;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpStatus;
import org.dozer.DozerBeanMapper;
import org.glassfish.jersey.media.multipart.BodyPartEntity;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.onestep.relief.utility.accreditation.db.IApplicationDBService;
import org.onestep.relief.utility.accreditation.dto.*;
import org.onestep.relief.utility.accreditation.model.Accreditation;
import org.onestep.relief.utility.accreditation.model.AccreditationNote;
import org.onestep.relief.utility.accreditation.model.Document;
import org.onestep.relief.utility.accreditation.service.IAccreditationService;
import org.onestep.relief.utility.accreditation.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.services.s3.S3Client;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.onestep.relief.utility.accreditation.util.AccreditationConstants.REWARD_SMALL;
import static org.onestep.relief.utility.accreditation.util.Constants.*;
import static org.onestep.relief.utility.accreditation.util.S3Operations.calculateHash;

public class AccreditationService implements IAccreditationService {

    private static final Logger logger = LoggerFactory.getLogger(AccreditationService.class);
    private final DozerBeanMapper dozerMapper = new DozerBeanMapper();
    IApplicationDBService dbService;
    IssueReward issueRewardClient;

    public AccreditationService(IApplicationDBService dbService, IssueReward issueRewardClient) {

        this.dbService = dbService;
        this.issueRewardClient = issueRewardClient;
    }

    /**
     * Creates a new a  application or updates an existing application.
     *
     * @param parts                Multi-part form upload of files
     * @param accreditationRequest JSON DTO of required info. Mapped to Accreditation model object
     * @return AccreditationResponse of info to the caller
     */
    @Override
    public AccreditationResponse createOrUpdateApplication(List<FormDataBodyPart> parts,
                                                           AccreditationRequest accreditationRequest) {

        // Map DTO to model object Accreditation
        Accreditation accreditation = dozerMapper.map(accreditationRequest, Accreditation.class);
        //Set enums manually
        accreditation.setRole(Accreditation.RoleEnum.fromValue(accreditationRequest.getRole()));
        accreditation.setEntityType(Accreditation.EntityTypeEnum.fromValue(accreditationRequest.getEntityType()));
        accreditation.setAccreditationState(Accreditation.AccreditationStateEnum.fromValue(accreditationRequest.getAccreditationState()));

        if (accreditationRequest.getAccreditationId() == null) {
            logger.info(accreditation.toString());
            //...so upload documents and save new accreditation Object
            dbService.save(uploadDocuments(parts, accreditation));

            //Now if a person then try to issue rewards
            if (accreditationRequest.getEntityType().equals("PER")) {
                try {
                    issueRewardClient.issueReward("Accreditation application", REWARD_SMALL, accreditation.getEntityId());
                } catch (Exception e) {
                    logger.warn("Unable to issue reward " + e.getMessage());
                }
            }

        } else {
            // update existing application
            dbService.save(updateDocuments(parts, accreditation));
        }

        return ResponseMapper.accreditationResponseMapper(accreditation);
    }


    /**
     * Used by Admin to approve a specific accreditation request. Changes accreditationState to 'Approved'
     * stores the hash of the documents in a blockchain transaction, stores the transaction ID in the
     * documents section of the Accreditation object in the db.
     *
     * @param accreditationId Unique ID for the accreditation request
     * @return AccreditationResponse object of application
     */
    @Override
    public AccreditationResponse accreditationApplicationApprove(String accreditationId,
                                                                 NoteDTO adminNote)
            throws IndexOutOfBoundsException, JsonProcessingException {

        // Used to ensure that we don't have duplicate transactions if the documents are the same
        // i.e. they have identical hashes.
        Long amount = 0L;

        // Map DTO to model object AccreditationNote
        AccreditationNote noteToAdd = dozerMapper.map(adminNote, AccreditationNote.class);
        Map<String, Object> secretsResponse = getSavedSigningKey();
        String mnemonic = secretsResponse.get("mnemonic").toString();
        String address = secretsResponse.get("address").toString();

        Accreditation accreditation = dbService.queryByApplicationId(accreditationId);
        List<Document> documents = accreditation.getDocuments();
        logger.info("App id "+ accreditationId);
        logger.info("App "+ accreditation.toString());
        logger.info("Docs "+documents.toString());

        // Get documents associated with this application
        for (Document document : documents) {
            String note = document.getHash();

            SendTransactionRequest blockchainTransaction = new SendTransactionRequest();
            blockchainTransaction.setSender(new AccountDTO(address, mnemonic))
                    .setReceiver(new AccountDTO(address))
                    .setAmount(amount)
                    .setNote(note);

            amount++;

            logger.info("transaction "+blockchainTransaction);

            Response response = HTTPRequests.PostRequest(BLOCKCHAIN_URI, SEND_ALGO_ROUTE,
                    blockchainTransaction);

            Optional<Response> optionalResponse = Optional.ofNullable(response);


            if (optionalResponse.isPresent() && optionalResponse.get().getStatus() == HttpStatus.SC_OK) {
                logger.info("submit Transaction Envelope Request was successful");

                Map<String, Object> blockchainResponse;
                blockchainResponse = optionalResponse.get().readEntity(HashMap.class);

                logger.info("Txn id" + blockchainResponse.get("data").toString());
                String txId = blockchainResponse.get("data").toString();

                // Add transaction ID to each document so we can audit later
                document.setTxId(txId);


            } else {
                logger.error("Error submitting transaction");
                logger.info("Response "+response.getStatus());
                logger.info("Response "+response.toString());
                throw new NullPointerException("Error submitting Blockchain Transaction");
            }
        }

        /*
         * Only update the database status and add transaction ID if we are able to post to the
         * blockchain, otherwise throw error back to end user.
         */

        // Change status to 'Approved'
        accreditation.setAccreditationState(Accreditation.AccreditationStateEnum.APPROVED);
        accreditation.setAdminNote(noteToAdd);
        dbService.save(accreditation);

        //Now if a person then try to issue rewards
        if (accreditation.getEntityType().toString().equals("PER")) {
            try {
                issueRewardClient.issueReward("Accreditation approval", REWARD_SMALL, accreditation.getEntityId());
            } catch (Exception e) {
                logger.warn("Unable to issue reward " + e.getMessage());
            }
        }
        //TODO make this an exception that is passed to the client.

        return ResponseMapper.accreditationResponseMapper(accreditation);
    }

    /**
     * Used by Admin to reject an accreditation request. Changes accreditationState to 'Rejected'.
     *
     * @param accreditationId Unique ID for the accreditation request
     * @return AccreditationResponse object of application
     */
    @Override
    public AccreditationResponse accreditationApplicationReject(String accreditationId,
                                                                NoteDTO adminNote) throws IndexOutOfBoundsException {

        // Map DTO to model object AccreditationNote
        AccreditationNote noteToAdd = dozerMapper.map(adminNote, AccreditationNote.class);

        Accreditation accreditation = dbService.queryByApplicationId(accreditationId);

        // Change status to 'Rejected'
        accreditation.setAccreditationState(Accreditation.AccreditationStateEnum.REJECTED);
        accreditation.setAdminNote(noteToAdd);
        dbService.save(accreditation);

        return ResponseMapper.accreditationResponseMapper(accreditation);
    }


    // TODO fix generic exceptions??
    @Override
    public void accreditationApplicationDelete(String accreditationId) {
        Accreditation toDelete = dbService.queryByApplicationId(accreditationId);
        toDelete.setAccreditationState(Accreditation.AccreditationStateEnum.fromValue("Deleted"));
        dbService.save(toDelete);

    }

    /**
     * @param state      Application state: "Pending", "Approved", etc
     * @param entityType Optional - "ORG" or "PER", although query will accept lower case
     * @param startDate  Optional - Format yyyy-mm-dd
     * @return List of AccreditationResponse objects
     * @see org.onestep.relief.utility.accreditation.db.ApplicationDBService#queryByState(String, String, String)
     */
    @Override
    public List<AccreditationResponse> listApplicationsByState(String state, String entityType,
                                                               String startDate) {
        return dbService.queryByState(state, entityType, startDate).stream()
                .filter(accreditation -> !accreditation.getAccreditationState().equals(Accreditation.AccreditationStateEnum.DELETED))
                .map(ResponseMapper::accreditationResponseMapper)
                .collect(Collectors.toList());
    }

    /**
     * Passed an entityId, returns all non-deleted applications
     *
     * @param entityId Either PER or ORG's ID
     * @return List of AccreditationResponse objects representing their applications
     */
    @Override
    public List<AccreditationResponse> listApplicationsByEntity(String entityId) {

        return dbService.queryByEntityId(entityId).stream()
                .map(ResponseMapper::accreditationResponseMapper)
                .collect(Collectors.toList());
    }

    /**
     * @return All applications, except deleted ones.
     */
    @Override
    public List<AccreditationResponse> retrieveAllApplications() {

        List<AccreditationResponse> response = dbService.retrieveAllApplications().stream()
                .filter(accreditation -> !accreditation.getAccreditationState().equals(Accreditation.AccreditationStateEnum.DELETED))
                .map(ResponseMapper::accreditationResponseMapper)
                .collect(Collectors.toList());

        return response;
    }

    /**
     * Retrieves one specific application
     *
     * @param accreditationId ID of the application to retrieve
     * @return AccreditationResponse object representing the application
     * @throws IOException if unable to retrieve
     */
    @Override
    public AccreditationResponse listByApplicationId(String accreditationId) throws IOException {

        Accreditation app = dbService.queryByApplicationId(accreditationId);

        // Loop thru our documents and create temp links for them all
        // Client needs to be able to link to application encrypted documents, we we de-crypt, move to a temp
        // folder in S3, and create time sensative links
        for (Document currentDocument : app.getDocuments()) {
            String bucketName = currentDocument.getBucket();
            String folderName = currentDocument.getFolder();
            String fileName = currentDocument.getFileName();

            // but only if the file exists for the document object...
            if (bucketName != null && folderName != null && fileName != null) {

                S3Client s3Client = S3Operations.createS3Client();
                byte[] encryptedFile = S3Operations.getFile(s3Client, folderName + "/" + fileName, bucketName);
                byte[] encryptedKey = S3Operations.getFile(s3Client, folderName + "/" + fileName + "-key", bucketName);

                byte[] decryptedFile = KMSOperations.decryptUsingDataKey(encryptedKey, encryptedFile);

                S3Operations.putFile(s3Client, folderName + "/" + fileName, TEMPFILES, decryptedFile);

                //Create 'temporary' URLs that expire.
                String url;
                String fileType = fileName.substring(fileName.length() - 3).toLowerCase();
                if (fileType.equals("jpg") || fileType.equals("png") || fileType.equals("jpeg")) {
                    byte[] thumbnail = CreateThumbnail.thumbnail(decryptedFile);
                    S3Operations.putFile(s3Client, folderName + "/" + "thumb-" + fileName + ".png", TEMPFILES, thumbnail);

                    url = CreateURL.createPresignedURL(folderName + "/" + "thumb-" + fileName + ".png");
                    logger.info("Url " + url);
                    currentDocument.setTempLinkThumb(url);
                }

                url = CreateURL.createPresignedURL(folderName + "/" + fileName);
                logger.info("Url " + url);
                currentDocument.setTempLink(url);

            }
        }
        return ResponseMapper.accreditationResponseMapper(app);
    }

    /**
     * @param role String value of Roles enum, e.g. "Community Admin" vice COMMUNITYADMIN
     * @return List of AccreditationResponse objects with only specified role
     */
    @Override
    public List<AccreditationResponse> listApplicationsByRole(String role) {

        return dbService.queryByRole(role).stream()
                .filter(accreditation -> !accreditation.getAccreditationState().equals(Accreditation.AccreditationStateEnum.DELETED))
                .map(ResponseMapper::accreditationResponseMapper)
                .collect(Collectors.toList());
    }

    /**
     * Returns roles that entity has not yet applied for. Use case is that Accreditation application page
     * list of roles should only show roles that are available for application, which does not include the
     * entity's previous applied for roles.
     *
     * @param entityId Id of PER or ORG
     * @return RolesResponse object, which is is list of RoleResponse. This contains the role name, list of documents
     * as well as a description of the role.
     * @throws JsonProcessingException if JSON mapping fails.
     */
    @Override
    public RolesResponse listUniqueRoles(String entityId) throws IOException {

        final ObjectMapper mapper = new ObjectMapper();

        // Start with our Enum values of roles. This is our source.
        List<String> allRoles = Arrays.stream(Accreditation.RoleEnum.values())
                .map(Accreditation.RoleEnum::toString)
                .collect(Collectors.toList());

        // Remove roles that have already been used in this user's applications. Only showing
        // available roles remaining.
        allRoles.removeAll(dbService.queryByEntityIdUniqueRoles(entityId));

        //Get the JSON of roles -> required documents. Don't want to put into code required docs
        RolesResponse rolesWithDocumentsJson = mapper.readValue(fixture("roles.json"), RolesResponse.class);

        // Create DTO Response object
        RolesResponse responseWithDocuments = new RolesResponse();
        List<RoleResponse> list = rolesWithDocumentsJson.getRoles().stream()
                .filter(r -> allRoles.contains(r.getRole()))
                .collect(Collectors.toList());

        // Add filtered roles with documents list
        responseWithDocuments.setRoles(list);

        return responseWithDocuments;
    }


    /**
     * Tests that the current hash of a file is the same as the hash that was stored on the blockchain
     * when the application was apporved.
     *
     * @param fileToAudit     To audit file
     * @param accreditationId ID of the application. Needed to get the blockchain transaction ID
     * @return Boolean indicating success or failure
     */
    public boolean applicationAudit(String fileToAudit, String accreditationId) {

        Accreditation toAudit = dbService.queryByApplicationId(accreditationId);

        // First check that the application in question is approved, otherwise return false.
        // UI shouldn't implement this functionality unless the app is approved but this
        // should ensure we don't have issues
        if (toAudit.getAccreditationState() == Accreditation.AccreditationStateEnum.APPROVED) {
            return AuditFile.auditFile(fileToAudit, accreditationId);
        } else {
            return false;
        }
    }

    /**
     * Used to upload documents when application is initially created
     *
     * @param parts         Multiform parts which contains the file
     * @param accreditation Accreditation object
     * @return Updated Accreditation object after files are stored
     */
    /*
     ************** Utility Functions **************
     */
    Accreditation uploadDocuments(List<FormDataBodyPart> parts, Accreditation accreditation) {

        Optional<List<FormDataBodyPart>> optional = Optional.ofNullable(parts);

        optional.ifPresent(formDataBodyParts -> {

            byte[] uploadedFile;
            String entityId;
            List<byte[]> keyAndFile;
            byte[] encryptedKey;
            byte[] encryptedFile;
            String bucketName;

            String hash;
            String folderName;

            /*
             * Loop through the data sent and upload each file, add its info to Dynamo DB
             */
            /* Upload the key first; it will have file name appended with "-key"
             * then upload the file
             */
            for (FormDataBodyPart formDataBodyPart : formDataBodyParts) {

                /* Create random prefix to prevent collisions with identical file names */
                String filePrefix = UUID.randomUUID().toString().substring(0, 7);

                /*
                 * Casting FormDataBodyPart to BodyPartEntity, which can give us
                 * InputStream for uploaded file
                 */
                BodyPartEntity fileData = (BodyPartEntity) formDataBodyPart.getEntity();
                String fileName = formDataBodyPart.getContentDisposition().getFileName();


                // 1) Convert multi-part form file to byte array
                uploadedFile = SdkBytes.fromInputStream(fileData.getInputStream()).asByteArray();


                // 2) Retrieve data (user ID, user URL,etc) from request object and store in variables until we store in db.
                entityId = accreditation.getEntityId();

                folderName = accreditation.getEntityType().toString() + "/" + entityId;


                // 3) Calculate hash of file to upload and save in temp variable. db functions will use to store in db
                // We eventually store has in the blockchain once the application is approved.
                hash = calculateHash(uploadedFile);

                // 4) Encrypt uploaded file which is now a byte array
                keyAndFile = KMSOperations.encryptUsingDataKey(uploadedFile);
                encryptedKey = keyAndFile.get(0);
                encryptedFile = keyAndFile.get(1);


                // 5) Upload encrypted file and encryption key (which is encrypted) to S3. Note location
                bucketName = S3Operations.getBucketName();
                S3Client s3Client = S3Operations.createS3Client();

                /* Upload the key first; it will have file name appended with "-key"
                 * then upload the file
                 */
                S3Operations.putFile(s3Client, folderName + "/" + filePrefix + fileName + "-key", bucketName, encryptedKey);
                S3Operations.putFile(s3Client, folderName + "/" + filePrefix + fileName, bucketName, encryptedFile);

                S3Operations.closeS3Client(s3Client);

                // 7) db code to store a) file location, b) file name c) file hash d) user ID e) any other info

                // Update doc with hash and bucket
                // Find the document object that contains this file name and update it...

                int index = IntStream.range(0, accreditation.getDocuments().size())
                        .filter(j -> accreditation.getDocuments().get(j).getFileName().equals(fileName))
                        .findFirst()
                        .orElse(-1);

                if (index != -1) {
                    Document document = accreditation.getDocuments().get(index);
                    document.withHash(hash)
                            .withBucket(bucketName)
                            .withFolder(folderName)
                            .withFileName(filePrefix + fileName);
                }

            }
        });
        return accreditation;
    }

    /**
     * Used to update documents of a present, previously submitted application. Logic is different
     *
     * @param parts         Multipart form of new files to upload
     * @param accreditation Accreditation object
     * @return Updated Accreditation object after files are updated
     */
    Accreditation updateDocuments(List<FormDataBodyPart> parts, Accreditation accreditation) {

        Optional<List<FormDataBodyPart>> optional = Optional.ofNullable(parts);


        optional.ifPresent(formDataBodyParts -> {

            byte[] uploadedFile;
            String entityId;
            List<byte[]> keyAndFile;
            byte[] encryptedKey;
            byte[] encryptedFile;
            String bucketName;

            String hash;
            String folderName;
            List<Document> documents = accreditation.getDocuments();

            /*
             * Loop through the data sent and upload each file, add its info to Dynamo DB
             */
            for (FormDataBodyPart formDataBodyPart : formDataBodyParts) {

                /* Create random prefix to prevent collisions with identical file names */
                String filePrefix = UUID.randomUUID().toString().substring(0, 7);

                /*
                 * Casting FormDataBodyPart to BodyPartEntity, which can give us
                 * InputStream for uploaded file
                 */
                BodyPartEntity fileData = (BodyPartEntity) formDataBodyPart.getEntity();
                String fileName = formDataBodyPart.getContentDisposition().getFileName();


                // 1) Convert multi-part form file to byte array
                uploadedFile = SdkBytes.fromInputStream(fileData.getInputStream()).asByteArray();


                // 2) Retrieve data (user ID, user URL,etc) from request object and store in variables until we store in db.
                entityId = accreditation.getEntityId();

                folderName = accreditation.getEntityType().toString() + "/" + entityId;


                // 3) Calculate hash of file to upload and save in temp variable. db functions will use to store in db
                // We eventually store has in the blockchain once the application is approved.
                hash = calculateHash(uploadedFile);

                // 4) Encrypt uploaded file which is now a byte array
                keyAndFile = KMSOperations.encryptUsingDataKey(uploadedFile);
                encryptedKey = keyAndFile.get(0);
                encryptedFile = keyAndFile.get(1);


                // 5) Upload encrypted file and encryption key (which is encrypted) to S3. Note location
                bucketName = S3Operations.getBucketName();
                S3Client s3Client = S3Operations.createS3Client();

                /* Upload the key first; it will have file name appended with "-key"
                 * then upload the file
                 */
                S3Operations.putFile(s3Client, folderName + "/" + filePrefix + fileName + "-key", bucketName, encryptedKey);
                S3Operations.putFile(s3Client, folderName + "/" + filePrefix + fileName, bucketName, encryptedFile);

                S3Operations.closeS3Client(s3Client);

                // 7) db code to store a) file location, b) file name c) file hash d) user ID e) any other info

                // Assuming that we could update more than 1 file passed in, first find the doc type
                // for the current uploaded file, based on file name.


                logger.info("filename " + fileName);
                logger.info("documents " + documents.toString());
                int index = IntStream.range(0, documents.size())
                        .filter(j -> documents.get(j).getFileName().equals(fileName))
                        .findFirst()
                        .orElse(-1);
                logger.info("index " + index);
                String docType = documents.get(index).getDocType();
                logger.info("docType " + docType);

                //Now that we have the correct docType for the current uploaded file, now find its index
                // in the database object. This is what we'll need to update to reflect new file.

                Accreditation savedAccreditation = dbService.queryByApplicationId(accreditation.getAccreditationId());


                // Update doc with hash and bucket

                // First find the document in the stored accreditation object that is this
                // document type, i.e. "passport", "license", etc.
                // We'll need to update it.
                index = IntStream.range(0, savedAccreditation.getDocuments().size())
                        .filter(j -> savedAccreditation.getDocuments().get(j).getDocType().equals(docType))
                        .findFirst()
                        .orElse(-1);

                System.out.println("database Index " + index);

                Document document = new Document();

                //If we find it then we'll just update the 'documents' field for this object
                if (index != -1) {

                    document.withHash(hash)
                            .withDocType(docType)
                            .withBucket(bucketName)
                            .withFolder(folderName)
                            .withFileName(filePrefix + fileName);
                    accreditation.setDocuments(savedAccreditation.getDocuments());
                    accreditation.getDocuments().set(index, document);
                    //If we previously haven't saved this docType then it's new so we will append
                } else {
                    document.withHash(hash)
                            .withDocType(docType)
                            .withBucket(bucketName)
                            .withFolder(folderName)
                            .withFileName(filePrefix + fileName);
                    accreditation.setDocuments(savedAccreditation.getDocuments());
                    accreditation.addDocumentsItem(document);
                }

            }
        });

        return updateAccreditation(accreditation);
    }


    /**
     * Updates fields of an application
     *
     * @param accreditation Object with new data to update
     * @return Updated Accreditation object, already saved in db.
     */
    Accreditation updateAccreditation(Accreditation accreditation) {

        Accreditation savedAccreditation = dbService.queryByApplicationId(accreditation.getAccreditationId());

        if (accreditation.getRole() != null) {
            savedAccreditation.setRole(accreditation.getRole());
        }
        if (accreditation.getEntityType() != null) {
            savedAccreditation.setEntityType(accreditation.getEntityType());
        }
        if (accreditation.getUserNote() != null) {
            savedAccreditation.setUserNote(accreditation.getUserNote());
        }
        savedAccreditation.setDocuments(accreditation.getDocuments());
        logger.info(savedAccreditation.toString());
        return savedAccreditation;
    }

    /**
     * Used to sign approved application blockchain transaction. The transaction is used to save a hash for one
     * specific file. This retrieves the public/private key from Secrets Manager in AWS.
     *
     * @return Map of public/private key.
     * @throws JsonProcessingException if unable to map.
     */
    private Map<String, Object> getSavedSigningKey() throws JsonProcessingException {

        //2a) Get 'siging' account credentials from AWS Secrets Manager
        String sourceAccount;
        try {
            sourceAccount = Utils.getSecret();

        } catch (Exception e) {
            logger.info(e.getMessage());
            throw e;

        }
        //Map the address & mnemonic retrieved from AWS so that we can get the private key later
        Map<String, Object> response;
        try {
            response = new ObjectMapper().readValue(sourceAccount, HashMap.class);
        } catch (Exception e) {
            logger.info(e.getMessage());
            throw e;

        }
        return response;
    }
}
