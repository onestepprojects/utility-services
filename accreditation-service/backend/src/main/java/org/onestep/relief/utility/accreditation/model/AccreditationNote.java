

package org.onestep.relief.utility.accreditation.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Accreditation note.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@DynamoDBDocument
public class AccreditationNote implements Entity {

    @JsonProperty("comments")
    private String comments = null;

    @JsonProperty("date")
    private String date = null;

    @JsonProperty("user")
    private String user;    //Admin initials or name


}

