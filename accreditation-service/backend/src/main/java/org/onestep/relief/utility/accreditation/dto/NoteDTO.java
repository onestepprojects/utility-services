package org.onestep.relief.utility.accreditation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NoteDTO {

    private String comments = null;

    private String date = null;

    private String user;    //Admin initials or name

}
