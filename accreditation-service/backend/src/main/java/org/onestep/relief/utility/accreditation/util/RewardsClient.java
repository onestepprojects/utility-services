package org.onestep.relief.utility.accreditation.util;

import org.apache.http.HttpStatus;
import org.onestep.relief.utility.accreditation.dto.IssueRequest;
import org.onestep.relief.utility.accreditation.dto.IssueResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.onestep.relief.utility.accreditation.util.AccreditationConstants.ISSUE_PATH;
import static org.onestep.relief.utility.accreditation.util.AccreditationConstants.REWARDS_URL;

public class RewardsClient {

    private final static Logger LOGGER = LoggerFactory.getLogger(RewardsClient.class);

    // default URI and paths for org service as of 4/14/21. should be set via env in practice (see below)
    private static String rewardsURL = REWARDS_URL;
    private static String issuePath = ISSUE_PATH;


    // Build new jersey client
    private Client client = ClientBuilder.newClient();


    /**
     * Calls REST API from rewards service to issue an award.
     *
     * @param issueRequest details of rewards issue
     * @param tokenType type of token to award
     * @param serviceAuthToken auth token for service
     * @return IssueResponse details of successful reward
     * @throws Exception if there is a failure w/ API call
     */
    public  IssueResponse issueReward(IssueRequest issueRequest, String tokenType, String serviceAuthToken) throws Exception {
        String path = rewardsURL + issuePath;

        LOGGER.info("Attempting POST: " + path);
        LOGGER.info("issueRequest: " + issueRequest);
        LOGGER.info("svcAuthtoken: " + serviceAuthToken);

        WebTarget webTarget = client.target(path);
        Invocation invocation = webTarget.request(MediaType.APPLICATION_JSON)
                .header("Authorization", tokenType + " " + serviceAuthToken)
                .buildPost(Entity.entity(issueRequest, MediaType.APPLICATION_JSON));
        Response response = invocation.invoke();

        if (response != null && response.getStatus() == HttpStatus.SC_OK) {
            IssueResponse issueResponse = response.readEntity(IssueResponse.class);
            return issueResponse;
        } else {
            LOGGER.warn("Issue reward failed");
            LOGGER.warn(response.toString());
            throw new Exception(response.getStatus() + "Failed to issue reward");
        }
    }
}

