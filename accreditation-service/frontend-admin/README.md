<!-- Publish revision: 6 -->

# Accreditation Admin Module

## Getting Started

```sh
npm i
npm start
```

## NPM Publish

Currently the ci is not work correctly. Please publish manual each time you make changes.

1. Bump `package.json` version. e.g. `1.0.0-staging.0`
2. `NPM_TOKEN=*** npm publish --tag staging`

The `NPM_TOKEN` value is [gitlab personal access token](https://gitlab.com/-/profile/personal_access_tokens).
