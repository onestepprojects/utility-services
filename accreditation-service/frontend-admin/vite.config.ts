import { defineConfig } from '@onestepprojects/frontend-infrastructure/vite.config.js'

export default defineConfig({ base: '/accreditation/admin/', tailwindcss: true })
