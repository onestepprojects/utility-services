import { CheckCircleTwoTone, ClockCircleTwoTone, CloseCircleTwoTone } from '@ant-design/icons'
import { useMemoizedFn, useRequest } from 'ahooks'
import { Segmented, Table, Typography } from 'antd'
import { useMemo, useState, type FC } from 'react'
import { useSearchParams } from 'react-router-dom'
import { getAccreditationsByState, getOrganizations, getPersons } from '../services'
import { dayjs } from '../utils'
import { DetailModal, type DetailModalProps } from './DetailModal'

export const Home: FC = () => {
  const [searchParams, setSearchParams] = useSearchParams({ state: 'Pending' })
  const state = searchParams.get('state')

  const [accreditation, setAccreditation] = useState<DetailModalProps['accreditation']>()

  const {
    loading: accreditationsLoading,
    data: accreditations,
    refresh: refreshAccreditations,
  } = useRequest(() => getAccreditationsByState(state), { refreshDeps: [state] })

  const { loading: personsLoading, data: persons } = useRequest(getPersons)
  const { loading: organizationsLoading, data: organizations } = useRequest(getOrganizations)

  const loading = accreditationsLoading || personsLoading || organizationsLoading

  const accreditationEntities = useMemo<DetailModalProps['accreditation'][]>(() => {
    if (!accreditations?.length) return accreditations

    const idNameMap = new Map<string, string>()
    persons?.forEach((person) => idNameMap.set(person.uuid, person.name))
    organizations?.forEach((organization) => idNameMap.set(organization.id, organization.name))

    const accreditationEntities = accreditations.map((accreditation) => ({
      ...accreditation,
      entityName: idNameMap.get(accreditation.entityId),
    }))

    return accreditationEntities
  }, [accreditations, persons, organizations])

  const handleModalClose = useMemoizedFn((refresh?: boolean) => {
    setAccreditation(undefined)
    refresh && refreshAccreditations()
  })

  return (
    <div>
      <Table
        bordered
        loading={loading}
        title={() => (
          <div>
            <Typography.Title level={3}>Accreditation Admin Management</Typography.Title>
            <Segmented
              block
              size='large'
              options={[
                {
                  label: 'Pending',
                  value: 'Pending',
                  icon: <ClockCircleTwoTone />,
                },
                {
                  label: 'Approved',
                  value: 'Approved',
                  icon: <CheckCircleTwoTone twoToneColor='#52c41a' />,
                },
                {
                  label: 'Rejected',
                  value: 'Rejected',
                  icon: <CloseCircleTwoTone twoToneColor='#ff4d4f' />,
                },
              ]}
              value={state}
              onChange={(value: string) => setSearchParams({ state: value })}
            />
          </div>
        )}
        columns={[
          {
            title: 'Role',
            dataIndex: 'role',
            ellipsis: true,
            render: (value) =>
              loading ? (
                value
              ) : state === 'Pending' ? (
                <Typography.Link>{value}</Typography.Link>
              ) : state === 'Approved' ? (
                <Typography.Text type='success'>{value}</Typography.Text>
              ) : state === 'Rejected' ? (
                <Typography.Text type='danger' delete>
                  {value}
                </Typography.Text>
              ) : (
                value
              ),
          },
          {
            title: 'Type',
            dataIndex: 'entityType',
            render: (value) => ({ PER: 'Person', ORG: 'Organization' }[value]),
          },
          {
            title: 'Name',
            dataIndex: 'entityName',
          },
          {
            title: 'Date',
            dataIndex: 'date',
            responsive: ['md'],
            render: (value) => (value ? dayjs(value).format('llll') : ''),
          },
        ]}
        rowKey='accreditationId'
        onRow={(record) => ({
          onClick: () => setAccreditation(record),
        })}
        dataSource={accreditationEntities}
        pagination={{
          position: ['bottomCenter'],
          hideOnSinglePage: true,
        }}
      />
      <DetailModal
        accreditation={accreditation}
        onOk={() => handleModalClose(true)}
        onCancel={() => handleModalClose()}
      />
    </div>
  )
}
