import { CheckOutlined, CloseOutlined } from '@ant-design/icons'
import { useRequest } from 'ahooks'
import {
  Button,
  Descriptions,
  Form,
  Input,
  List,
  Modal,
  Space,
  message,
  type ModalProps,
} from 'antd'
import { type FC } from 'react'
import { approveAccreditation, rejectAccreditation } from '../services'
import { type Accreditation } from '../types'
import { dayjs } from '../utils'

export interface DetailModalProps extends ModalProps {
  accreditation: Accreditation & { entityName?: string }
}

export const DetailModal: FC<DetailModalProps> = (props) => {
  const { accreditation, ...modalProps } = props

  const [form] = Form.useForm()
  const comments = Form.useWatch('comments', form)
  const requestBody = {
    id: accreditation?.accreditationId,
    user: accreditation?.entityName,
    comments,
  }

  const { loading: approveAccreditationLoading, runAsync: approve } = useRequest(
    () => approveAccreditation(requestBody),
    { manual: true }
  )
  const { loading: rejectAccreditationLoading, runAsync: reject } = useRequest(
    () => rejectAccreditation(requestBody),
    { manual: true }
  )
  const loading = approveAccreditationLoading || rejectAccreditationLoading

  return (
    <Modal
      {...modalProps}
      visible={!!accreditation}
      title='Accreditation Detail'
      cancelButtonProps={{ hidden: true }}
      footer={
        ['Pending'].includes(accreditation?.accreditationState) ? (
          <Space size='large'>
            <Button
              type='primary'
              danger
              icon={<CloseOutlined />}
              loading={loading}
              onClick={async (e) => {
                try {
                  await reject()
                  modalProps.onOk(e)
                  message.success('Approved')
                } catch (err) {
                  console.error(err)
                }
              }}
            >
              Reject
            </Button>
            <Button
              type='primary'
              icon={<CheckOutlined />}
              loading={loading}
              onClick={async (e) => {
                try {
                  await approve()
                  modalProps.onOk(e)
                  message.warn('Rejected')
                } catch (err) {
                  console.error(err)
                }
              }}
            >
              Approve
            </Button>
          </Space>
        ) : undefined
      }
    >
      <Space style={{ width: '100%' }} direction='vertical' size='large'>
        <Descriptions bordered column={1}>
          <Descriptions.Item label='State'>{accreditation?.accreditationState}</Descriptions.Item>
          <Descriptions.Item label='Role'>{accreditation?.role}</Descriptions.Item>
          <Descriptions.Item label='Type'>
            {{ PER: 'Person', ORG: 'Organization' }[accreditation?.entityType]}
          </Descriptions.Item>
          <Descriptions.Item label='Name'>{accreditation?.entityName}</Descriptions.Item>
          <Descriptions.Item label='Date'>
            {accreditation?.date ? dayjs(accreditation.date).format('llll') : ''}
          </Descriptions.Item>
          {!!accreditation?.userNote?.comments && (
            <Descriptions.Item label='Admin Note'>
              {accreditation.userNote.comments}
            </Descriptions.Item>
          )}
          {['Approved', 'Rejected'].includes(accreditation?.accreditationState) && (
            <Descriptions.Item label='Admin Note'>
              {accreditation?.adminNote?.comments}
            </Descriptions.Item>
          )}
        </Descriptions>

        {!!accreditation?.documents?.length && (
          <List
            header='Documents'
            dataSource={accreditation.documents}
            renderItem={(item) => (
              <List.Item
                actions={[
                  <a key='preview' target='_blank' href={item.tempLink}>
                    Preview
                  </a>,
                ]}
              >
                <List.Item.Meta
                  title={item.fileName}
                  description={item.date && dayjs(item.date).fromNow()}
                />
              </List.Item>
            )}
          />
        )}

        {['Pending'].includes(accreditation?.accreditationState) && (
          <Form layout='vertical' form={form}>
            <Form.Item label='Admin Note' name='comments'>
              <Input.TextArea
                rows={6}
                placeholder='Add approve or reject comments'
                defaultValue={accreditation?.adminNote?.comments}
              />
            </Form.Item>
          </Form>
        )}
      </Space>
    </Modal>
  )
}
