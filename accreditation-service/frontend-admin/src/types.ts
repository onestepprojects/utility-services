export interface Accreditation {
  accreditationId: string
  accreditationState: 'Pending' | 'Approved' | string
  date: string
  documents: {
    docType: string
    fileName: string
    tempLink: string
    tempLinkThumb: string
    date: string
  }[]
  entityId: string
  entityType: 'ORG' | 'PER'
  role: 'Expert' | string
  userNote: {
    user: string
    comments: string
    date: string
  }
  adminNote: {
    user: string
    comments: string
    date: string
  }
}

export interface AccreditationRole {
  role: string
  documents: string[]
  comments: string
}

export interface Person {
  uuid: string
  name: string
  currentAddress: Address
  email: string
  phones: {
    phone: string
    mobile?: string
    sat_phone?: string
  }
  birthdate: string
  profilePicture: Picture
  lastActivity: string
  history: any[]
  homeAddress: Address
  ecUUID: string
  ecName: any
  ecPhones: {
    phone: string
  }
  ecAddress: Address
  paymentAccount: {
    payID: string
    blockchainAddress: string
    status: string
    assets: any
  }
  location: any
  gender: string
  socialMedia: SocialMedia
}

export interface Organization {
  id: string
  type: string
  supplier: boolean
  name: string
  logo: Picture
  email: string
  phones: {
    phone: string
  }
  website: string
  address: Address
  creatorUUID: string
  statusLabel: string
  statusLastActive: string
  tagline: string
  description?: {
    description: string
    serviceArea: { latitude: number; longitude: number }[]
    projectTypes: string[]
  }
  requesterRoles: string[]
  isActive: boolean
  socialMedia: SocialMedia
}

interface Address {
  address: string
  city: string
  country: string
  district: string
  postalCode: string
  state: string
}

interface Picture {
  name?: string // unique name as id
  size?: number
  format?: 'JPG' | 'PNG'
  body?: string
  caption?: string
  bucketName?: string
  creator?: string
  url?: string
}

interface SocialMedia {
  twitter: string
  facebook: string
  linkedin: string
  youtube: string
}
