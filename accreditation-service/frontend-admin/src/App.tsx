import { useEffect, type FC } from 'react'
import { BrowserRouter } from 'react-router-dom'
import './App.css'
import { Container, type ContainerProps } from './Container'
import { api } from './services'

interface AccreditationAdminModuleProps extends ContainerProps {
  /** Module router base name. */
  mountPath: string
}

const AccreditationAdminModule: FC<AccreditationAdminModuleProps> = (props) => {
  const { mountPath, auth } = props

  useEffect(() => {
    if (!auth) return
    api.defaults.headers['Authorization'] = `Bearer ${auth.token}`
  }, [auth])

  return (
    <div id='AccreditationAdminModule'>
      <BrowserRouter basename={mountPath}>
        <Container auth={auth} />
      </BrowserRouter>
    </div>
  )
}

export default AccreditationAdminModule
