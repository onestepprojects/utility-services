import axios from 'axios'
import type { Accreditation, Organization, Person } from './types'

export const api = axios.create({
  baseURL: process.env.REACT_APP_ACCREDITATION_MODULE_ACCREDITATION_API_URL,
})

export const getAccreditationsByState = (state: 'Pending' | 'Approved' | 'Rejected' | string) =>
  api.get<Accreditation[]>(`application?state=${state}`).then(({ data }) => data)

export const approveAccreditation = ({
  id,
  ...params
}: {
  id: string
  comments: string
  user: string
}) => api.post(`${id}/approve`, { ...params, date: new Date().toJSON() })

export const rejectAccreditation = ({
  id,
  ...params
}: {
  id: string
  comments: string
  user: string
}) => api.post(`${id}/reject`, { ...params, date: new Date().toJSON() })

export const getPersons = () =>
  api.get<Person[]>(`${process.env.REACT_APP_ORG_MODULE_API_URL}/persons`).then(({ data }) => data)

export const getOrganizations = () =>
  api
    .get<Organization[]>(`${process.env.REACT_APP_ORG_MODULE_API_URL}/organizations`)
    .then(({ data }) => data)
