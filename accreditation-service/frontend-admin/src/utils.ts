import _dayjs from 'dayjs'
import localizedFormat from 'dayjs/plugin/localizedFormat'
import relativeTime from 'dayjs/plugin/relativeTime'

_dayjs.extend(localizedFormat)
_dayjs.extend(relativeTime)

export const dayjs = _dayjs
