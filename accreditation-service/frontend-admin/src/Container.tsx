import { type FC } from 'react'
import { Route, Routes, useNavigate } from 'react-router-dom'
import type { Person } from './types'
import { Home } from './views'

export interface ContainerProps {
  auth: {
    token?: string
    person?: Person
    roles?: string[]
  }
}

export const Container: FC<ContainerProps> = ({ auth }) => {
  const navigate = useNavigate()

  return (
    <Routes>
      <Route path='/*' element={<Home personId={auth?.person?.uuid} />}></Route>
    </Routes>
  )
}
