import { useEffect, type FC } from 'react'
import { BrowserRouter } from 'react-router-dom'
import './App.css'
import { Container, type ContainerProps } from './Container'
import { api } from './services'

interface AccreditationModuleProps extends ContainerProps {
  /** Module router base name. */
  mountPath: string
}

const AccreditationModule: FC<AccreditationModuleProps> = ({ mountPath, auth }) => {
  useEffect(() => {
    if (!auth) return
    api.defaults.headers['Authorization'] = `Bearer ${auth.token}`
  }, [auth])

  return (
    <BrowserRouter basename={mountPath}>
      <Container auth={auth} />
    </BrowserRouter>
  )
}

export default AccreditationModule
