import { type FC } from 'react'
import { Route, Routes, useNavigate } from 'react-router-dom'
import type { Person } from './types'
import { EditModal, Home, NewModal, ViewModal } from './views'

export interface ContainerProps {
  auth: {
    token?: string
    person?: Person
    roles?: string[]
  }
}

export const Container: FC<ContainerProps> = ({ auth }) => {
  const navigate = useNavigate()

  return (
    <Routes>
      <Route path='/*' element={<Home personId={auth?.person?.uuid} />}>
        <Route
          path=':accreditationId'
          element={<ViewModal onOk={() => navigate('..')} onCancel={() => navigate('..')} />}
        />
        <Route
          path='new'
          element={
            <NewModal
              person={auth?.person}
              onOk={() => navigate('..')}
              onCancel={() => navigate('..')}
            />
          }
        />
        <Route
          path=':accreditationId/edit'
          element={
            <EditModal
              person={auth?.person}
              onOk={() => navigate('..')}
              onCancel={() => navigate('..')}
            />
          }
        />
      </Route>
    </Routes>
  )
}
