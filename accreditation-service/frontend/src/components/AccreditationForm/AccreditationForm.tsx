import { UploadOutlined } from '@ant-design/icons'
import {
  Button,
  Descriptions,
  Form,
  Input,
  Select,
  Upload,
  type FormInstance,
  type FormProps,
} from 'antd'
import type { RcFile } from 'antd/lib/upload'
import { useRef, type FC } from 'react'
import type { Accreditation } from '../../types'

interface AccreditationFormData {
  role: string
  documents: {
    docType: string
    file: RcFile
  }[]
  userNote: Accreditation['userNote']
}

interface RoleOption {
  label: string
  value: string
  documents: string[]
}

interface AccreditationFormProps extends FormProps<AccreditationFormData> {
  entityName?: string
  entityTypeName?: string
  roleLoading?: boolean
  roleOptions?: RoleOption[]
}

const getFileDataURL = (file: RcFile): Promise<string> =>
  new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = () => resolve(reader.result as string)
    reader.onerror = reject
  })

export const AccreditationForm: FC<AccreditationFormProps> = ({
  entityName,
  entityTypeName,
  roleLoading,
  roleOptions,
  ...formProps
}) => {
  const formRef = useRef<FormInstance>()

  return (
    <Form {...formProps} ref={formRef} layout='vertical'>
      <Descriptions column={1}>
        <Descriptions.Item label='Applicant or Organization Name'>{entityName}</Descriptions.Item>
        <Descriptions.Item label='Applying for'>{entityTypeName}</Descriptions.Item>
      </Descriptions>
      <Form.Item label='Role' name='role' rules={[{ required: true }]}>
        <Select
          loading={roleLoading}
          options={roleOptions}
          onChange={(_, { documents }: RoleOption) =>
            formRef.current?.setFieldsValue({
              documents: documents?.map((docType) => ({ docType })),
            })
          }
        />
      </Form.Item>
      <Form.List name='documents'>
        {(fields) =>
          fields.map((field) => (
            <div key={field.key}>
              <Form.Item
                label={formRef.current.getFieldValue(['documents', field.key, 'docType'])}
                name={[field.name, 'file']}
                getValueProps={(v) => v && { fileList: [v] }}
                getValueFromEvent={(e) => (Array.isArray(e) ? e[0] : e?.fileList[0])}
              >
                <Upload
                  listType='picture'
                  accept='.jpg,.jpeg,.png,.gif,.bmp,.tiff,.raw,.pdf'
                  maxCount={1}
                  beforeUpload={() => false}
                  previewFile={getFileDataURL}
                  onPreview={(file) =>
                    window.open(URL.createObjectURL(file.originFileObj), '_blank')
                  }
                >
                  <Button icon={<UploadOutlined />}>Click to upload</Button>
                </Upload>
              </Form.Item>
              <Form.Item hidden name={[field.name, 'docType']}>
                <Input />
              </Form.Item>
            </div>
          ))
        }
      </Form.List>
      <Form.Item
        label='Additional Notes'
        name={['userNote', 'comments']}
        extra='Explain if you can not supply some or all of required documents'
      >
        <Input.TextArea rows={6} />
      </Form.Item>
    </Form>
  )
}
