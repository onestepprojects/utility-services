import axios from 'axios'
import type { Accreditation, AccreditationRole, Organization, Person } from './types'

export const api = axios.create({
  baseURL: process.env.REACT_APP_ACCREDITATION_MODULE_ACCREDITATION_API_URL,
})

export const getAccreditations = (entityId: string) =>
  api.get<Accreditation[]>(`application/${entityId}/entity`).then(({ data }) => data)

export const getAccreditation = (accreditationId: string) =>
  api.get<Accreditation>(`application/${accreditationId}`).then(({ data }) => data)

export const getAccreditationRoles = (entityId: string) =>
  api
    .get<{ roles: AccreditationRole[] }>(`application/${entityId}/roles`)
    .then(({ data }) => data?.roles)
    .catch(() =>
      [
        'Community Admin',
        'Sponsor/Donor',
        'Community Member',
        'Supplier',
        'Expert',
        'Community Leader',
        'Relief Worker',
        'Health Worker',
      ].map((role) => ({ role, documents: [], comments: '' }))
    )

export const createAccreditation = (formData: FormData) =>
  api.post<Accreditation>(`application`, formData).then(({ data }) => data)

export const updateAccreditation = createAccreditation

export const deleteAccreditation = (accreditationId: string) =>
  api.delete(`application/${accreditationId}`).then(({ data }) => data)

export const getPerson = (personId: string) =>
  api
    .get<Person>(`${process.env.REACT_APP_ORG_MODULE_API_URL}/persons/${personId}`)
    .then(({ data }) => data)

export const getOrganization = (organizationId: string) =>
  api
    .get<Organization>(
      `${process.env.REACT_APP_ORG_MODULE_API_URL}/organizations/${organizationId}`
    )
    .then(({ data }) => data)
