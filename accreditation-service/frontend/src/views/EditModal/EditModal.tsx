import { useRequest } from 'ahooks'
import { Form, Modal, type ModalProps } from 'antd'
import { useState, type FC } from 'react'
import { useOutletContext, useParams } from 'react-router-dom'
import { AccreditationForm } from '../../components'
import {
  getAccreditation,
  getAccreditationRoles,
  getOrganization,
  updateAccreditation,
} from '../../services'
import type { Accreditation, Person } from '../../types'
import { dayjs } from '../../utils'

interface EditAccreditation extends Partial<Accreditation> {
  payId: string
  address: string
}

interface EditModalProps extends ModalProps {
  person?: Person
}

const entityTypeMap = {
  PER: 'Person',
  ORG: 'Organization',
}

export const EditModal: FC<EditModalProps> = ({ person, ...modalProps }) => {
  const { refreshAccreditations } = useOutletContext<{ refreshAccreditations: () => void }>()
  const { accreditationId } = useParams<{ accreditationId: string }>()

  const [form] = Form.useForm()
  const [formError, setFormError] = useState(false)
  const [formSubmitting, setFormSubmitting] = useState(false)

  const { loading: accreditationLoading, data: accreditation } = useRequest(
    () => getAccreditation(accreditationId),
    { ready: !!accreditationId, onSuccess: form.setFieldsValue }
  )

  const { loading: accreditationRolesLoading, data: accreditationRoles } = useRequest(
    () => getAccreditationRoles('*'), // accreditation.entityId
    { ready: !!accreditation?.entityId }
  )

  const { loading: organizationLoading, data: organization } = useRequest(
    () => getOrganization(accreditation.entityId),
    { ready: accreditation?.entityType === 'ORG' && !!accreditation?.entityId }
  )

  return (
    <Modal
      {...modalProps}
      visible
      centered
      maskClosable={false}
      title='Edit Application'
      okText='Submit'
      okButtonProps={{
        disabled: formError,
        loading: formSubmitting,
        onClick: () => form.submit(),
      }}
    >
      <AccreditationForm
        form={form}
        entityName={accreditation?.entityType === 'ORG' ? organization?.name : person?.name}
        entityTypeName={entityTypeMap[accreditation?.entityType]}
        roleLoading={accreditationRolesLoading}
        roleOptions={accreditationRoles?.map(({ role, documents }) => ({
          label: role,
          value: role,
          documents,
        }))}
        onFieldsChange={(_, allFields) =>
          setFormError(allFields.flatMap(({ errors }) => errors).length > 0)
        }
        onFinish={async (values) => {
          try {
            setFormSubmitting(true)
            console.log('submit accreditation form', values)

            const { role, userNote, documents } = values
            const { date, ...restAccreditation } = accreditation
            const newAccreditation: EditAccreditation = {
              ...restAccreditation,
              payId: person?.paymentAccount?.payID || 'Default Pay ID',
              address: person?.paymentAccount?.blockchainAddress || 'Default Blockchain Address',
              role,
              userNote: {
                ...userNote,
                date: dayjs().toJSON(),
              },
              documents: documents.map((doc) => ({
                docType: doc.docType,
                fileName: doc.file?.name,
              })),
            }

            console.log('update accreditation', newAccreditation)

            const formData = new FormData()
            documents
              .map(({ file }) => file)
              .filter(Boolean)
              .forEach((file) => formData.append('files', file))
            formData.append('accreditation', JSON.stringify(newAccreditation))

            await updateAccreditation(formData)
            refreshAccreditations()
            modalProps.onOk({} as any)
          } finally {
            setFormSubmitting(false)
          }
        }}
      />
    </Modal>
  )
}
