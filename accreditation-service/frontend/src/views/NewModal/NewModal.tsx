import { useRequest } from 'ahooks'
import { Form, Modal, type ModalProps } from 'antd'
import { useState, type FC } from 'react'
import { useOutletContext, useSearchParams } from 'react-router-dom'
import { AccreditationForm } from '../../components'
import { createAccreditation, getAccreditationRoles, getOrganization } from '../../services'
import type { Accreditation, Person } from '../../types'
import { dayjs } from '../../utils'

interface NewAccreditation extends Partial<Accreditation> {
  payId: string
  address: string
}

interface NewModalProps extends ModalProps {
  person?: Person
}

const entityTypeMap = {
  PER: 'Person',
  ORG: 'Organization',
}

export const NewModal: FC<NewModalProps> = ({ person, ...modalProps }) => {
  const [searchParams] = useSearchParams()
  const entityType = (searchParams.get('entityType') || 'PER') as Accreditation['entityType']
  const entityId = searchParams.get('entityId') || person?.uuid

  const { refreshAccreditations } = useOutletContext<{ refreshAccreditations: () => void }>()

  const [form] = Form.useForm()
  const [formError, setFormError] = useState(false)
  const [formSubmitting, setFormSubmitting] = useState(false)

  const { loading: organizationLoading, data: organization } = useRequest(
    () => getOrganization(entityId),
    { ready: entityType === 'ORG' && !!entityId }
  )

  const { loading: accreditationRolesLoading, data: accreditationRoles } = useRequest(
    () =>
      getAccreditationRoles(
        entityType === 'PER' ? person.uuid : entityType === 'ORG' ? entityId : ''
      ),
    { ready: (entityType === 'PER' && !!person?.uuid) || entityType === 'ORG' || !!entityId }
  )

  return (
    <Modal
      {...modalProps}
      visible
      centered
      maskClosable={false}
      title='Submit a New Application'
      okText='Submit'
      okButtonProps={{
        disabled: formError,
        loading: formSubmitting,
        onClick: () => form.submit(),
      }}
    >
      <AccreditationForm
        form={form}
        entityName={entityType === 'ORG' ? organization?.name : person?.name}
        entityTypeName={entityTypeMap[entityType]}
        roleLoading={accreditationRolesLoading}
        roleOptions={accreditationRoles?.map(({ role, documents }) => ({
          label: role,
          value: role,
          documents,
        }))}
        onFieldsChange={(_, allFields) =>
          setFormError(allFields.flatMap(({ errors }) => errors).length > 0)
        }
        onFinish={async (values) => {
          try {
            setFormSubmitting(true)
            console.log('submit accreditation form', values)

            const { role, userNote, documents } = values
            const accreditation: NewAccreditation = {
              accreditationState: 'Pending',
              entityId,
              entityType,
              payId: person?.paymentAccount?.payID || 'Default Pay ID',
              address: person?.paymentAccount?.blockchainAddress || 'Default Blockchain Address',
              role,
              userNote: {
                ...userNote,
                date: dayjs().toJSON(),
              },
              documents: documents.map((doc) => ({
                docType: doc.docType,
                fileName: doc.file?.name,
              })),
            }

            console.log('create accreditation', accreditation)

            const formData = new FormData()
            documents
              .map(({ file }) => file)
              .filter(Boolean)
              .forEach((file) => formData.append('files', file))
            formData.append('accreditation', JSON.stringify(accreditation))

            await createAccreditation(formData)

            refreshAccreditations()
            modalProps.onOk({} as any)
          } finally {
            setFormSubmitting(false)
          }
        }}
      />
    </Modal>
  )
}
