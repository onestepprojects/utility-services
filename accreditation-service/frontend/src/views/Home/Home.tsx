import { useRequest } from 'ahooks'
import { Button, Card, Popconfirm, Space, Spin, Table, Typography, message } from 'antd'
import { type FC } from 'react'
import { Link, Outlet } from 'react-router-dom'
import { deleteAccreditation, getAccreditations } from '../../services'
import { dayjs } from '../../utils'

interface HomeProps {
  personId?: string
}

export const Home: FC<HomeProps> = ({ personId }) => {
  const {
    loading: accreditationsLoading,
    data: accreditations,
    refresh: refreshAccreditations,
  } = useRequest(() => getAccreditations(personId))

  return (
    <div>
      <Card
        title={
          <div className='flex flex-wrap justify-between'>
            <div className='flex items-center'>
              <Typography.Title level={3}>Accreditation Management</Typography.Title>
              <Spin className='mx-4' spinning={accreditationsLoading} />
            </div>
            <Link to='new'>
              <Button type='primary'>Submit a New Application</Button>
            </Link>
          </div>
        }
      >
        <Table
          pagination={false}
          rowKey='accreditationId'
          columns={[
            { title: 'Role', dataIndex: 'role' },
            { title: 'Status', dataIndex: 'accreditationState' },
            {
              title: 'Submission Date',
              dataIndex: 'date',
              responsive: ['md'],
              render: (value) => dayjs(value).format('llll'),
            },
            {
              align: 'right',
              render: (_, record) => (
                <Space size='large' wrap>
                  <Link to={record.accreditationId}>
                    <Button size='small'>View</Button>
                  </Link>
                  <Link to={`${record.accreditationId}/edit`}>
                    <Button size='small'>Edit</Button>
                  </Link>
                  <Popconfirm
                    title={
                      <Typography.Text>
                        Are you sure remove <Typography.Text strong>{record.role}</Typography.Text>{' '}
                        accreditation?
                      </Typography.Text>
                    }
                    okText='Remove'
                    okButtonProps={{ danger: true }}
                    onConfirm={async () => {
                      await deleteAccreditation(record.accreditationId)
                      refreshAccreditations()
                      message.success(
                        <Typography.Text>
                          <Typography.Text strong>{record.role}</Typography.Text> accreditation
                          removed.
                        </Typography.Text>
                      )
                    }}
                  >
                    <Button size='small' danger>
                      Remove
                    </Button>
                  </Popconfirm>
                </Space>
              ),
            },
          ]}
          dataSource={accreditations}
        />
      </Card>

      <Outlet context={{ refreshAccreditations }} />
    </div>
  )
}
