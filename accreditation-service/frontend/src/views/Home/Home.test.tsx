import { render, screen, waitForElementToBeRemoved } from '@testing-library/react'
import { Home } from './Home'

jest.mock('./services', () => ({
  getAccreditations: jest.fn(() =>
    Promise.resolve({
      data: [
        {
          id: '1',
          role: 'Sponsor/Donor',
          accreditationState: 'Pending',
          date: 'Thu Jun 09 18:53:56 UTC 2022',
        },
      ],
    })
  ),
}))

describe('Home', () => {
  it('renders a table of accreditations', async () => {
    render(<Home personId='1' />)

    await waitForElementToBeRemoved(() => document.querySelector('.ant-table-placeholder'))

    expect(screen.getByRole('columnheader', { name: 'Role' })).toBeInTheDocument()
    expect(screen.getByRole('columnheader', { name: 'Status' })).toBeInTheDocument()
    expect(screen.getByRole('columnheader', { name: 'Submission Date' })).toBeInTheDocument()

    expect(screen.getByRole('cell', { name: 'Sponsor/Donor' })).toBeInTheDocument()
    expect(screen.getByRole('cell', { name: 'Pending' })).toBeInTheDocument()
  })
})
