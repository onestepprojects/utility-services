import { Comment } from '@ant-design/compatible'
import { useRequest } from 'ahooks'
import { Avatar, Descriptions, Modal, Spin, Tooltip, type ModalProps } from 'antd'
import { type FC } from 'react'
import { useParams } from 'react-router-dom'
import { getAccreditation } from '../../services'
import { dayjs } from '../../utils'

interface ViewModalProps extends ModalProps {}

export const ViewModal: FC<ViewModalProps> = ({ ...modalProps }) => {
  const { accreditationId } = useParams<{ accreditationId?: string }>()

  const { loading: accreditationLoading, data: accreditation } = useRequest(() =>
    getAccreditation(accreditationId)
  )

  return (
    <Modal {...modalProps} visible centered cancelButtonProps={{ hidden: true }}>
      <Descriptions
        title={
          <div>
            Accreditation <Spin className='mx-2' size='small' spinning={accreditationLoading} />
          </div>
        }
        bordered
        column={1}
      >
        <Descriptions.Item label='Role'>{accreditation?.role}</Descriptions.Item>
        <Descriptions.Item label='Status'>{accreditation?.accreditationState}</Descriptions.Item>
        <Descriptions.Item label='Submission Date'>
          {accreditation?.date && dayjs(accreditation.date).format('llll')}
        </Descriptions.Item>
      </Descriptions>
      {accreditation?.userNote && (
        <Comment
          className='mx-1'
          avatar={<Avatar>{accreditation.userNote.user || 'U'}</Avatar>}
          author={accreditation.userNote.user || 'User'}
          datetime={
            <Tooltip title={dayjs(accreditation.userNote.date).format('llll')}>
              <time>{dayjs(accreditation.userNote.date).fromNow()}</time>
            </Tooltip>
          }
          content={accreditation.userNote.comments}
        />
      )}
    </Modal>
  )
}
