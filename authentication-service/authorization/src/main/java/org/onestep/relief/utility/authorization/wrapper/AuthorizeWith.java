package org.onestep.relief.utility.authorization.wrapper;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface AuthorizeWith {
    
	String RoleName();

}