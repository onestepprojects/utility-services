package org.onestep.relief.utility.authorization.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.*;
import lombok.Data;

@Data
public class AWSConfiguration {
	
	@JsonProperty
    @NotEmpty
    private String region;
	
	@JsonProperty
    @NotEmpty
    private String cognitoUserPoolId;

	@JsonProperty
    @NotEmpty
    private String cognitoDomain;

}