package org.onestep.relief.utility.authorization.core;

import org.onestep.relief.utility.authorization.resources.IsAuthorizedResource;
import org.onestep.relief.utility.authorization.annotations.AuthorizeWith;

import java.io.IOException;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotAuthorizedException;
// import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.core.Response;

import javax.annotation.Priority;

import org.glassfish.jersey.server.model.AnnotatedMethod;


// import java.util.Collection;
// import java.util.Iterator;
// import org.apache.commons.io.IOUtils;
// import java.nio.charset.StandardCharsets;


public class AuthorizationWrapper implements DynamicFeature {

	@Override
	public void configure(final ResourceInfo resourceInfo, final FeatureContext configuration) {
		final AnnotatedMethod method = new AnnotatedMethod(resourceInfo.getResourceMethod());

		AuthorizeWith annotation = method.getAnnotation(AuthorizeWith.class);
		if (annotation != null) {
			configuration.register(new AuthorizeWithRequestFilter(annotation.RoleName(), annotation.ResourceParam(), annotation.ResourceParamLocation()));
			return;
		}

	}

	private static class AuthorizeWithRequestFilter implements ContainerRequestFilter {

		private final String RoleName;
		private final String ResourceParam;
		private final String ResourceParamLocation;
		private static IsAuthorizedResource auth = new IsAuthorizedResource();

		AuthorizeWithRequestFilter(final String RoleName, final String ResourceParam, final String ResourceParamLocation) {
			this.RoleName = RoleName;
			this.ResourceParam = ResourceParam;
			this.ResourceParamLocation = ResourceParamLocation;
		}

		@Override
		public void filter(final ContainerRequestContext requestContext) throws IOException {

			String token = requestContext.getHeaders().getFirst("Authorization");

			// System.out.println(token);

			System.out.println("Checking for role = "+this.RoleName);

			Response response = auth.isAuthorized(token, this.RoleName);
			Response.StatusType status = response.getStatusInfo();

			if (status == Response.Status.OK) {
				return;
			} else if (status == Response.Status.UNAUTHORIZED) {
				throw new NotAuthorizedException(response);
			} else if (status == Response.Status.FORBIDDEN) {
				throw new ForbiddenException(response);
			}

			// String body = IOUtils.toString(requestContext.getEntityStream(), StandardCharsets.UTF_8);

			// System.out.println(body);

			// requestContext.setEntityStream(IOUtils.toInputStream(body));

		}

	}
}