package org.onestep.relief.utility.authorization;

public class GlobalConfig {

	private static AuthorizationServiceConfiguration config = null;

	public static AuthorizationServiceConfiguration getConfig() {
		return config;
	}

	public static void setConfig(AuthorizationServiceConfiguration _config) {
		config = _config;
	}

}