package org.onestep.relief.utility.authorization.resources;

import org.onestep.relief.utility.authorization.core.impl.AuthorizationService;
import org.onestep.relief.utility.authorization.exceptions.AuthorizationServiceException;
import org.onestep.relief.utility.authorization.api.GenericResponse;
import org.onestep.relief.utility.authorization.api.CreateRoleRequest;
import org.onestep.relief.utility.authorization.annotations.AuthorizeWith;

import javax.naming.AuthenticationException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/role")
@Produces(MediaType.APPLICATION_JSON)
public class RoleResource {
	
	private AuthorizationService authzService;

	public RoleResource() {
		authzService = new AuthorizationService();
	}

	@POST
	@AuthorizeWith(RoleName = "admin")
	public Response createRole(CreateRoleRequest request) {
		
		GenericResponse responseBody;
		Response.Status status;

		try {
			authzService.createRole(request.getRoleId());
			status = Response.Status.OK;
			responseBody = new GenericResponse(String.format("Created role '%s'", request.getRoleId()));
		} catch (AuthorizationServiceException e) {
			status = Response.Status.BAD_REQUEST;
			responseBody = new GenericResponse(e.getMessage());
		}
		
		return Response.status(status).entity(responseBody).build();
	}
}