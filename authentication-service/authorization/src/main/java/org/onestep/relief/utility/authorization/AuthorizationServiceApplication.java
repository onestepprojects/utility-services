package org.onestep.relief.utility.authorization;

import org.onestep.relief.utility.authorization.resources.*;
import org.onestep.relief.utility.authorization.core.AuthorizationWrapper;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import org.eclipse.jetty.servlets.CrossOriginFilter;
import javax.servlet.*;
import java.util.EnumSet;
import javax.ws.rs.client.Client;
import org.glassfish.jersey.client.JerseyClientBuilder;

public class AuthorizationServiceApplication extends Application<AuthorizationServiceConfiguration> {

	public static void main(final String[] args) throws Exception {
		new AuthorizationServiceApplication().run(args);
	}

	@Override
	public String getName() {
		return "AuthorizationService";
	}

	@Override
	public void initialize(final Bootstrap<AuthorizationServiceConfiguration> bootstrap) {
		// TODO: application initialization
	}

	@Override
	public void run(final AuthorizationServiceConfiguration configuration, final Environment environment) {

		// enable CORS 
		final FilterRegistration.Dynamic cors = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
		cors.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
		cors.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "X-Requested-With,Content-Type,Accept,Origin,Authorization");
		cors.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD");
		cors.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");
		cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

		// make configuration available globally
		GlobalConfig.setConfig(configuration);
		
		// enable the authorization wrapper
		environment.jersey().register(AuthorizationWrapper.class);
		
		final Client client = new JerseyClientBuilder().build();

		environment.jersey().register(new ServiceLoginResource(client));
		environment.jersey().register(new IsAuthorizedResource());
		environment.jersey().register(new RoleResource());
		environment.jersey().register(new RoleBindingResource());
		environment.jersey().register(new UserResource());
	}

}
