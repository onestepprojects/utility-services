package org.onestep.relief.utility.authorization;

import org.onestep.relief.utility.authorization.core.AWSConfiguration;

import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.*;
import javax.validation.constraints.*;

public class AuthorizationServiceConfiguration extends Configuration {

	@NotNull
	private AWSConfiguration awsConfig = new AWSConfiguration();

	@JsonProperty("awsConfig")
	public AWSConfiguration getAwsConfig() {
		return this.awsConfig;
	}

	@JsonProperty("awsConfig")
	public void setAwsConfig(AWSConfiguration awsConfig) {
		this.awsConfig = awsConfig;
	}

}
