package org.onestep.relief.utility.authorization.resources;

import org.onestep.relief.utility.authorization.GlobalConfig;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.client.Entity;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.exceptions.JWTDecodeException;


@Path("/servicelogin")
@Produces(MediaType.APPLICATION_JSON)
public class ServiceLoginResource {
	
	private Client client;
	private String cognitoDomain;

	public ServiceLoginResource(Client client) {
		this.client = client;
		this.cognitoDomain = GlobalConfig.getConfig().getAwsConfig().getCognitoDomain();
	}	

	@POST
	public Response login(@HeaderParam("Authorization") String authHeader) {
		
		WebTarget webTarget = client.target(this.cognitoDomain + "/oauth2/token");
		Invocation.Builder invocationBuilder =  webTarget.request()
			.accept(MediaType.APPLICATION_JSON)
			.header("Authorization", authHeader);
		Response response = invocationBuilder.post(
			Entity.form(
				new Form("grant_type", "client_credentials")
			)
		);

		return response;
	}
}