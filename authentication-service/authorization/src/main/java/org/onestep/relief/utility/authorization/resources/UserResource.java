package org.onestep.relief.utility.authorization.resources;

import org.onestep.relief.utility.authorization.core.impl.AuthorizationService;
import org.onestep.relief.utility.authorization.exceptions.AuthorizationServiceException;
import org.onestep.relief.utility.authorization.annotations.AuthorizeWith;
import org.onestep.relief.utility.authorization.api.UserResponse;
import org.onestep.relief.utility.authorization.api.GenericResponse;
import org.onestep.relief.utility.authorization.utils.HttpUtils;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.exceptions.JWTDecodeException;


@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {
	
	private AuthorizationService authzService;

	public UserResource() {
		authzService = new AuthorizationService();
	}	

	@GET
	// @AuthorizeWith(RoleName = "this should be protected")
	public Response getUser(@HeaderParam("Authorization") String authHeader) {
		
		GenericResponse responseBody;
		Response.Status status;

		String token = HttpUtils.getTokenFromAuthHeader(authHeader);

		try {
			UserResponse user = authzService.getUserFromToken(token);
			status = Response.Status.OK;
			responseBody = new GenericResponse(String.format("Fetched user data from token."), user);
		} catch (AuthorizationServiceException e) {
			status = Response.Status.BAD_REQUEST;
			responseBody = new GenericResponse(e.getMessage());
		}
		
		return Response.status(status).entity(responseBody).build();
	}
}