package org.onestep.relief.utility.authorization.core;

import org.onestep.relief.utility.authorization.api.UserResponse;

import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface IAuthorizationService {

	Boolean isAuthenticated(String token);

	Boolean isAuthorized(String token, String Role);

	ArrayList<String> getUserRoles(String username);

	UserResponse getUserFromToken(String token);

	void createRole(String role);

	void createRoleBinding(String user, String role);

}