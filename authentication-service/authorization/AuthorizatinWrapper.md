# Authorization Helper

The authorization helper allows you to protect resource methods in your dropwizard project with specific roles in the authorization service.

The steps below show how to add it in your dropwizard project.

1. Add the two classes in the link below to your code base. Remember to change the `package` path depending on where you put this in your project.

[see this directory](https://gitlab.com/cscie599sec1spring21/team-2-project-area/-/tree/main/rewards-service/rewards-backend/src/main/java/org/onestep/relief/utility/rewards/auth)

2. Import `AuthorizationFilter` class and register it in the `run()` method of your dropwizard application.

```
import <path to AuthorizationFilter class in your project>.AuthorizationFilter;
...

    @Override
    public void run(final RewardsServiceConfiguration configuration,
                    final Environment environment) {
...
		// enable the authorization wrapper
		environment.jersey().register(AuthorizationFilter.class);
...

    }
```

3. Portect methods by adding the `@AuthorizeWith` annotation where necessary, for example:

```
import <path to AuthorizeWith annotation class in your project>.AuthorizeWith;
...

	@POST
	@Path("/create")
	@AuthorizeWith(RoleName = "admin")
	public Response create(@NotNull @Valid CreateRewardTypeRequest request) {
...
    }
```

4. Build your dropwizard app and make sure the following environment variable is set before running it.

```
ONESTEP_AUTHZ_URL=https://test.onesteprelief.org/onestep/authorization
```
