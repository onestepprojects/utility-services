# AuthorizationService

How to start the AuthorizationService application
---

1. Run `mvn clean install` to build your application
2. Update `config.yml` with your AWS region and Cognito user pool ID.
3. Set `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` environment variable from an AWS IAM user with admin rights.
4. Start application with `java -jar target/authorization-1.0-SNAPSHOT.jar server config.yml`
5. Test the `/is-authorized` endpoint by setting the `Authorization` header to an access token and navigating to`http://localhost:8080/is-authorized?permission=<a cognito group name>`

API
---

https://www.onestepprojects.tk/onestep/authorization/is-authorized?permission=?
