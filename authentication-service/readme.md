## **Authentication using AWS Cognito**

- [Pre-Requisite](#pre-requisite)
- [Configuration in React](#configuration-in-react)
- [Authorization](#authorization)
  - [Installation](#installation)

# Pre-Requisite

Request the authentication team to provide a `userPoolWebClientId` see below.

Include the following package in packages.json:

```json
"aws-amplify": "^3.3.20",
```

# Configuration in React

In your App.js (or a configuration file - recommended), add the following information:

```javascript
Amplify.configure(
    {
        userPoolId: 'us-west-2_B9oGErwyx',
        region: 'us-west-2',
        identityPoolRegion: 'us-west-2',
        userPoolWebClientId: 'client_id_provided_by_auth_team',
        oauth: {
            domain: 'identity-onesteprelief-dev-dev.auth.us-west-2.amazoncognito.com',
            scope: [
                'phone',
                'email',
                'openid',
                'profile',
                'aws.cognito.signin.user.admin'
            ],
            redirectSignIn: 'https://your_service_dns/',
            redirectSignOut: 'https://your_service_dns/',
            responseType: 'code'
        },
    });
```

Additionally, add the following snippet (this is the one in charge of redirecting a non-logged in user to the login page), also stores user information in the local cache:

```javascript
// Note: This code is temporal until the team figures out a way to abstract these lines
// into a single call to AuthHelper package
const [user, setUser] = useState(null);
  useEffect(async () => {

    try {
      var authenticateUser = await Auth.currentAuthenticatedUser();
      // This line sets the user in the cache (by invoking useState from above)
      setUser(authenticateUser);
    } catch (error) {
      console.log('Not authenticated, redirecting to authentication page.');
      // If .currentAuthenticatedUser throws, it means user is not logged in
      // proceed to call the federated login
      Auth.federatedSignIn();
    }

    // This is aws-amplify specific code, this listens for callbacks from aws-cognito
    Hub.listen('auth', async ({ payload: { event, data } }) => {
        switch (event) {
          case 'signIn':
          case 'cognitoHostedUI':
              try {
                  var authenticateUser = await Auth.currentAuthenticatedUser();
                  setUser(authenticateUser);
              } catch (error) {
                  // At this point user should be authenticated, getting an error
                  // means there could be another failure which we should log.
                  console.log(`Not signed in when it should, error: ${error}`);
              }
              break;
          case 'signOut':
              // Signing out event, clear user from cache, to logout a user call:
              // Auth.signOut()
              setUser(null);
              break;
          case 'signIn_failure':
          case 'cognitoHostedUI_failure':
              console.log('Sign in failure', data);
              break;
        }
    });
  }, []);
```

# Authorization

Plan is to release an httpHelper class that once imported it allows to make http calls (GET, PUT, POST, PATCH) and internally adds an authorization token from the logged in session.

## Installation

* Make sure to follow Gitlab's [Authenticate with a personal access token or deploy token](https://docs.gitlab.com/ee/user/packages/npm_registry/#authenticate-with-a-personal-access-token-or-deploy-token), this allows your local computer to authenticate against npm registry 
  * Recommendation, for local access you can use a _deploy token_ (there is a validation pending to make sure deploy tokens also work during CI/CD).
* After a sucessful authentication, run the following command:
  *  `npm i @cscie599sec1spring21/authentication-helper`
*  Then add the following import:
    ```javascript
    import { AuthHelper } from '@cscie599sec1spring21/authentication-helper'
    ```
* Now, to use `AuthHelper` do the following:
    ```javascript
    async function getCall() { 
        var authHelper = new AuthHelper();   
        var response = 
        await authHelper.Get('some_protected_uri');
        
        // Analyze the response
        if (response.status == 200) {
            // This is just an example how to append data into the current DOM
            const element = (
                <div>
                <h1>Hello, world! I am authorized to show you this info.</h1>
                </div>
            );
            ReactDOM.render(element, document.getElementById('response'));
        }
        else {
            // This is just an example how to append data into the current DOM
            const element = (
                <div>
                <h1>Hello, world! I am NOT authorized to show you this info.</h1>
                </div>
            );
            ReactDOM.render(element, document.getElementById('response'));
        }
    }
    ```