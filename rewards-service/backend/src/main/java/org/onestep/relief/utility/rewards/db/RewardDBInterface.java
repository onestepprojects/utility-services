package org.onestep.relief.utility.rewards.db;

import org.onestep.relief.utility.rewards.model.*;
import org.onestep.relief.utility.rewards.dao.*;

import java.util.List;

public interface RewardDBInterface {

	void addReward(RewardTransferDAO reward);

	List<RewardTransferDAO> queryRewards(ShowRewardRequestDAO request);

	void addRewardType(RewardTypeDAO rewardType);

	void addRewardLevel(RewardLevelDAO rewardLevel);

	RewardLevelDAO queryRewardLevels(String rewardType, Integer rewardAmount);

	List<RewardTypeDAO> getRewardTypes();

	RewardTypeDAO getRewardTypeByName(String name);

	void deleteRewardLevel(RewardLevelDAO rewardLevel);

	void deleteRewardType(RewardTypeDAO rewardType);
}