package org.onestep.relief.utility.rewards.dao;

import lombok.Data;

import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.*;

@Data
@DynamoDbBean
public class RewardTransferDAO {

	private String initiatorId;

	private String collectorId;

	private String reason;

	private String rewardType;

	private Integer amount;

	private Operation operation;

	private String transactionId;

	private Long transferDate;

	private ReceiverType receiverType;

	public enum ReceiverType {
		Person,
		Org;
	}

	public enum Operation {
		REWARD,
		REDEEM;
	}

	@DynamoDbPartitionKey
	public String getCollectorId() {
		return this.collectorId;
	}

	@DynamoDbSortKey
	public String getTransactionId() {
		return this.transactionId;
	}

}