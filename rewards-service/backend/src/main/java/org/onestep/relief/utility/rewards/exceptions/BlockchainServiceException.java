package org.onestep.relief.utility.rewards.exceptions;

public class BlockchainServiceException extends RuntimeException {

	public BlockchainServiceException(String message) {
		this(message, null);
	}

	public BlockchainServiceException(String message, Throwable cause) {
		super(message, cause);
	}

}