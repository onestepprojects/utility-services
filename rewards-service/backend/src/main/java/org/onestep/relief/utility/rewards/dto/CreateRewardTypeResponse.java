package org.onestep.relief.utility.rewards.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateRewardTypeResponse extends CreateRewardTypeRequest {

	@JsonProperty
	private String assetId;

}