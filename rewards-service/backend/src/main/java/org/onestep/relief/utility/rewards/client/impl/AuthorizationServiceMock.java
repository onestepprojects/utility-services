package org.onestep.relief.utility.rewards.client.impl;

import org.onestep.relief.utility.rewards.client.stubs.*;
import org.onestep.relief.utility.rewards.client.*;
import org.onestep.relief.utility.rewards.exceptions.*;

import java.util.ArrayList;

public class AuthorizationServiceMock implements AuthorizationServiceInterface {

	public UserDTO getUserFromToken(String token) {

		UserDTO user;

		if (token != null) {

			user = new UserDTO(
				"98hydffc-5f17-43e7-b909-b52dsa6se184",
				// String.valueOf(token.hashCode()),
				"OrganizationModule",
				// "name_" + String.valueOf(token.hashCode()),
				new ArrayList<String>()
			);
		} else {
			throw new AuthorizationServiceException("invalid token");
		}

		return user;
	}

}