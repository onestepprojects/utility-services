package org.onestep.relief.utility.rewards.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import javax.validation.constraints.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RewardLevelDTO {

	@JsonProperty
	@NotEmpty
	private String levelName;

	@JsonProperty
	@NotEmpty
	private String rewardType;

	@JsonProperty
	@NotNull
	@Positive
	private Integer minimumAmount;

	@JsonProperty
	@NotEmpty
	private String imageUrl;

}