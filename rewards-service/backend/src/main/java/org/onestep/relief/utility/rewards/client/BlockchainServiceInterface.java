package org.onestep.relief.utility.rewards.client;

import org.onestep.relief.utility.rewards.client.stubs.AssetTransferRequest;
import org.onestep.relief.utility.rewards.client.stubs.AssetTransferResponse;
import org.onestep.relief.utility.rewards.client.stubs.CreateAssetRequest;
import org.onestep.relief.utility.rewards.client.stubs.AssetDTO;
import org.onestep.relief.utility.rewards.dto.GenericResponse;

public interface BlockchainServiceInterface {

	public String transferAsset(AssetTransferRequest request);

	public AssetDTO createAsset(CreateAssetRequest request);

	public Integer getAssetBalance(String address, String assetId);
}