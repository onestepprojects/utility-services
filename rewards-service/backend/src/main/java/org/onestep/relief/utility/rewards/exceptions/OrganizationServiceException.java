package org.onestep.relief.utility.rewards.exceptions;

public class OrganizationServiceException extends RuntimeException {

	public OrganizationServiceException(String message) {
		this(message, null);
	}

	public OrganizationServiceException(String message, Throwable cause) {
		super(message, cause);
	}

}