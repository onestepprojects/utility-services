package org.onestep.relief.utility.rewards.client.impl;

import org.onestep.relief.utility.rewards.client.stubs.*;
import org.onestep.relief.utility.rewards.client.*;
import org.onestep.relief.utility.rewards.dto.GenericResponse;

import java.util.Random;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BlockchainServiceMock implements BlockchainServiceInterface {

	private static Logger logger = LoggerFactory.getLogger(BlockchainServiceMock.class);

	// private BlockchainService realBlockchain = new BlockchainService();


	public String transferAsset(AssetTransferRequest request) {

		logger.debug("Asset transfer request = {}", request);

		return UUID.randomUUID().toString();
	}

	public AssetDTO createAsset(CreateAssetRequest request) {

		// return this.realBlockchain.createAsset(request);

		return new AssetDTO(
			Long.valueOf(new Random().nextLong()),
			UUID.randomUUID().toString()
		);
	}

	public Integer getAssetBalance(String address, String assetId) {
		return null;
	}

}