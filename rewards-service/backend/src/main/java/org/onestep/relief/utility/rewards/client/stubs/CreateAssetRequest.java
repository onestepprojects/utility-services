package org.onestep.relief.utility.rewards.client.stubs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.dozer.Mapping;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties
public class CreateAssetRequest {

    @JsonProperty
    private String creator_addr;

    @JsonProperty
    private String creator_mnemonic;

    @JsonProperty
    @Mapping("total")
    private Integer total_amount;

    @JsonProperty
    @Mapping("frozen")
    private Boolean default_frozen;

    @JsonProperty
    @Mapping("unit")
    private String unit_name;

    @JsonProperty
    @Mapping("name")
    private String asset_name;

    @JsonProperty
    private String manager_addr;

    @JsonProperty
    private String reserve_addr;

    @JsonProperty
    private String freeze_addr;

    @JsonProperty
    private String clawback_addr;

    @JsonProperty
    private String url;

    @JsonProperty
    private Integer decimals;

}
