package org.onestep.relief.utility.rewards.client.impl;

import org.onestep.relief.utility.rewards.client.BlockchainServiceInterface;
import org.onestep.relief.utility.rewards.client.stubs.*;
import org.onestep.relief.utility.rewards.exceptions.BlockchainServiceException;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import org.glassfish.jersey.client.JerseyClientBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BlockchainService implements BlockchainServiceInterface {

	private static Logger logger = LoggerFactory.getLogger(BlockchainService.class);

	private String url;
	private Client client;

	public BlockchainService(String url) {
		this.url = url;
		this.client = new JerseyClientBuilder().build();
	}

	public String transferAsset(AssetTransferRequest request) {

		String txId;

		WebTarget webTarget = this.client.target(url + "/transaction/transferAsset");
		Invocation.Builder invocationBuilder =  webTarget.request().accept(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(
			Entity.entity(
				request,
				MediaType.APPLICATION_JSON
			)			
		);

		logger.debug("blockchain response = {}", response);

		AssetTransferResponse body = response.readEntity(AssetTransferResponse.class);
		
		if (response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode()) {
			txId = body.getData().getTxId();
			logger.info("txId from blockchain = {}", txId);
		} else {
			throw new BlockchainServiceException(body.getMessage());
		}

		return txId;
	}

	@Override
	public AssetDTO createAsset(CreateAssetRequest request) {

		AssetDTO asset;

		WebTarget webTarget = this.client.target(url + "/transaction/createAsset");
		Invocation.Builder invocationBuilder =  webTarget.request().accept(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(
			Entity.entity(
				request,
				MediaType.APPLICATION_JSON
			)			
		);

		logger.debug("blockchain response = {}", response);

		CreateAssetResponse body = response.readEntity(CreateAssetResponse.class);
		
		if (response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode()) {
			asset = body.getData();
			logger.info("asset from blockchain = {}", asset);
		} else {
			throw new BlockchainServiceException(body.getMessage());
		}

		return asset;
	}

	public Integer getAssetBalance(String address, String assetId) {

		Integer balance = null;

		WebTarget webTarget = this.client.target(url + "/account/getBalance/" + address);
		Invocation.Builder invocationBuilder =  webTarget.request().accept(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();

		logger.debug("blockchain response = {}", response);
		
		if (response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode()) {
			GetAssetBalanceResponse body = response.readEntity(GetAssetBalanceResponse.class);
			logger.debug("blockchain response body = {}", body);
			for (AssetBalanceDTO b : body.getAssets()) {
				if (b.getAssetId().equals(assetId)) {
					balance = b.getAmount();
					break;
				}
			}
		} else {
			throw new BlockchainServiceException(response.readEntity(String.class));
		}

		return balance;
	}
}