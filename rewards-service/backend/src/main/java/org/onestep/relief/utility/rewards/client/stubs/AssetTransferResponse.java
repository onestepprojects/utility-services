package org.onestep.relief.utility.rewards.client.stubs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
public class AssetTransferResponse {

	@JsonProperty
	private String message;
	
	@JsonProperty
	private TransactionIdDTO data;

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	@JsonIgnoreProperties(ignoreUnknown=true)
	public class TransactionIdDTO {

		@JsonProperty
		private String txId;

	}

}