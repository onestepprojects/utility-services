package org.onestep.relief.utility.rewards.exceptions;

public class RewardDBException extends RuntimeException {

	public RewardDBException(String message) {
		this(message, null);
	}

	public RewardDBException(String message, Throwable cause) {
		super(message, cause);
	}

}