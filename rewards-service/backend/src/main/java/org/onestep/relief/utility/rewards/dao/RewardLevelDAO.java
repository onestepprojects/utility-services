package org.onestep.relief.utility.rewards.dao;

import lombok.Data;

import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.*;

@Data
@DynamoDbBean
public class RewardLevelDAO {

	private String levelName;

	private String rewardType;

	private Integer minimumAmount;

	private String imageUrl;

	@DynamoDbPartitionKey
	public String getLevelName() {
		return this.levelName;
	}

	@DynamoDbSortKey
	public String getRewardType() {
		return this.rewardType;
	}

}