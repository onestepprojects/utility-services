package org.onestep.relief.utility.rewards;

import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.*;
import javax.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RewardsServiceConfiguration extends Configuration {

  @NotNull
  @JsonProperty("awsRegion")
  private String awsRegion;

  @JsonProperty("rewardDispenserAccount")
  private String rewardDispenserAccount;

  @JsonProperty("rewardDispenserKey")
  private String rewardDispenserKey;

  @JsonProperty("rewardExternalAccount")
  private String rewardExternalAccount;

  @JsonProperty("authzServiceURL")
  private String authzServiceURL;

  @JsonProperty("blockchainServiceURL")
  private String blockchainServiceURL;

  @JsonProperty("organizationServiceURL")
  private String organizationServiceURL;

  @JsonProperty("dbPrefix")
  private String dbPrefix;

}
