package org.onestep.relief.utility.rewards.service;

import org.dozer.MappingException;
import org.onestep.relief.utility.rewards.exceptions.*;
import org.onestep.relief.utility.rewards.client.*;
import org.onestep.relief.utility.rewards.client.stubs.*;
import org.onestep.relief.utility.rewards.model.*;
import org.onestep.relief.utility.rewards.dto.*;
import org.onestep.relief.utility.rewards.dao.*;
import org.onestep.relief.utility.rewards.db.*;
import org.onestep.relief.utility.rewards.converters.*;
import org.onestep.relief.utility.rewards.RewardsServiceConfiguration;

import java.net.URL;
import java.net.MalformedURLException;
import java.util.List;
import java.util.ArrayList;
import java.util.UUID;
import org.dozer.DozerBeanMapper;
import org.dozer.CustomConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RewardService implements RewardServiceInterface {
	
	private static Logger logger = LoggerFactory.getLogger(RewardService.class);
	
	private DozerBeanMapper mapper;
	private RewardDBInterface rewardDB;
	private OrganizationServiceInterface orgService;
	private BlockchainServiceInterface blockchainService;
	private AuthorizationServiceInterface authzService;
	private RewardsServiceConfiguration config;

	public RewardService(RewardDBInterface rewardDB,
						OrganizationServiceInterface orgService,
						BlockchainServiceInterface blockchainService,
						AuthorizationServiceInterface authzService,
						RewardsServiceConfiguration config) {
		this.rewardDB = rewardDB;
		this.orgService = orgService;
		this.blockchainService = blockchainService;
		this.authzService = authzService;
		this.config = config;

		this.mapper = new DozerBeanMapper();
		List<String> mappingFileList = new ArrayList<String>() {{
			add("dozerMapping.xml");
		}};
		this.mapper.setMappingFiles(mappingFileList);
	}

	public IssueRewardResponse issueReward(String token, IssueRewardRequest request) {

		IssueRewardResponse issueRewardResponse;
		PersonDTO issuerPerson, receiverPerson;
		OrgDTO receiverOrg;
		UserDTO issuerUser;
		String receiverAccountAddr, txId, undoTxId;
		RewardTransfer rewardTransfer;

		receiverAccountAddr = txId = undoTxId = null;

		// validate the issuer
		try {

			issuerPerson = this.orgService.getPerson(request.getIssuerId());
			logger.info("Got issuer person from issuerId: {}", issuerPerson);

		} catch (OrganizationServiceException eo) {

			logger.info("Could not find person from issuerId");
			logger.info("Organization service threw: {}", eo.getMessage());
			logger.info("We'll now try to get issuerId from the token.");

			try {
				issuerUser = this.authzService.getUserFromToken(token);
				logger.info("Got issuer user from token: {}", issuerUser);
				request.setIssuerId(issuerUser.getUsername());
			} catch (AuthorizationServiceException ea) {
				logger.error("Could not find user from the token.");
				logger.error("Authorization service threw: {}", ea.getMessage());
				throw new RewardServiceException("Could not determine reward issuer from the issuerId or the token.");
			}
		}

		try {

			if (request.getReceiverType() == IssueRewardRequest.ReceiverType.Org) {
				// call org service to get org from request.receiverId to find the person's blockchain account address
				receiverOrg = this.orgService.getOrg(request.getReceiverId());
				logger.info("Got org from receiverId: {}", receiverOrg);
				receiverAccountAddr = receiverOrg.getPaymentAccount().getBlockchainAddress();
			} else if (request.getReceiverType() == IssueRewardRequest.ReceiverType.Person) {
				// call org service to get person from request.receiverId to find the person's blockchain account address
				receiverPerson = this.orgService.getPerson(request.getReceiverId());
				logger.info("Got person from receiverId: {}", receiverPerson);
				receiverAccountAddr = receiverPerson.getPaymentAccount().getBlockchainAddress();
			} else {
				throw new RewardServiceException(String.format("Invalid reward receiver type: %s", request.getReceiverType()));
			}

			// send rewards on the blockchain and record txId
			txId = this.depositReward(receiverAccountAddr, request.getRewardType(), request.getAmount());
			rewardTransfer = this.mapper.map(request, RewardTransfer.class);
			rewardTransfer.setTransactionId(txId);

			// add reward to db
			RewardTransferDAO rewardTransferDAO = this.mapper.map(rewardTransfer, RewardTransferDAO.class);
			this.rewardDB.addReward(rewardTransferDAO);

			// build response DTO
			issueRewardResponse = this.mapper.map(rewardTransfer, IssueRewardResponse.class);

		} catch (OrganizationServiceException eo1) {

			logger.info("Could not find person from receiverId");
			logger.info("Organization service threw: {}", eo1.getMessage());
			throw new RewardServiceException("Could not determine reward receiver from the receiver Id and type.");

		} catch (BlockchainServiceException eb) {

			throw new RewardServiceException(String.format("Failed to transfer assets on the blockchain: %s", eb.getMessage()));

		} catch (RewardDBException e) {
			logger.error("Failed to add reward to DB. RewardDB service threw: {}", e.getMessage());
			throw new RewardServiceException(String.format("Failed to add reward to database, but blockchain transaction was made. TxId=%s,", txId));
		}
		
		return issueRewardResponse;
	}

	public List<IssueRewardResponse> showRewards(ShowRewardRequestDTO request) {
		
		List<IssueRewardResponse> result = new ArrayList<IssueRewardResponse>();

		try {
			ShowRewardRequestDAO showRewardRequestDAO = this.mapper.map(request, ShowRewardRequestDAO.class);

			List<RewardTransferDAO> rewardTransferDaoList = this.rewardDB.queryRewards(showRewardRequestDAO);

			for (RewardTransferDAO dao : rewardTransferDaoList) {
				result.add(
					this.mapper.map(
						this.mapper.map(dao, RewardTransfer.class),
						IssueRewardResponse.class
					)
				);
			}

		} catch (RewardDBException e) {
			throw new RewardServiceException(e.getMessage());
		}

		return result;
	}


	public CreateRewardTypeResponse createRewardType(CreateRewardTypeRequest request) {

		CreateRewardTypeResponse result;

		try {

			// create asset on the blockchain
			AssetDTO asset = this.blockchainService.createAsset(
				new CreateAssetRequest(
					this.config.getRewardDispenserAccount(),
					this.config.getRewardDispenserKey(),
					request.getTotalAmount(),
					false,
					request.getRewardUnit(),
					request.getRewardName(),
					this.config.getRewardExternalAccount(),
					this.config.getRewardExternalAccount(),
					this.config.getRewardExternalAccount(),
					this.config.getRewardDispenserAccount(),
					"onestep.relief.org",
					0
				)
			);

			// set assetId from response and add to db
			RewardType rewardType = this.mapper.map(request, RewardType.class);
			rewardType.setAssetId(asset.getAssetId().toString());
			RewardTypeDAO rewardTypeDao = this.mapper.map(rewardType, RewardTypeDAO.class);
			this.rewardDB.addRewardType(rewardTypeDao);

			result = this.mapper.map(rewardType, CreateRewardTypeResponse.class);

			// add a default beginner reward level
			this.createRewardLevel(
				new RewardLevelDTO(
					"Beginner",
					request.getRewardName(),
					0,
					"https://onestep.relief.org"
				)
			);

		} catch (RewardDBException e) {
			throw new RewardServiceException(String.format("Failed to add reward type to database: %s", e.getMessage()));
		} catch (BlockchainServiceException eb) {
			throw new RewardServiceException(String.format("Failed to create asset on the blockchain: %s", eb.getMessage()));
		} 

		return result;
	}


	public List<CalculateRewardLevelResponse> calculateRewardLevel(CalculateRewardLevelRequest request) {
		
		List<CalculateRewardLevelResponse> result = new ArrayList<CalculateRewardLevelResponse>();
			
		for (CreateRewardTypeResponse rewardType : this.getRewardTypes()) {
			result.add(this.calculateRewardLevelForRewardType(request.getRewardHolderId(), rewardType.getRewardName()));
		}

		return result;
	}


	public RewardLevelDTO createRewardLevel(RewardLevelDTO request) {

		RewardLevelDTO result = request;

		try {
			URL url = new URL(request.getImageUrl());
			RewardLevelDAO rewardLevel = this.mapper.map(request, RewardLevelDAO.class);
			this.rewardDB.addRewardLevel(rewardLevel);
		} catch (RewardDBException e) {
			throw new RewardServiceException(e.getMessage());
		} catch (MalformedURLException eu) {
			throw new RewardServiceException("invalid image URL");
		}

		return result;
	}

	@Override
	public void deleteRewardLevel(RewardLevelDTO request){

		try{
			RewardLevelDAO rewardLevel = this.mapper.map(request, RewardLevelDAO.class);
			this.rewardDB.deleteRewardLevel(rewardLevel);

		} catch (MappingException | RewardDBException e) {
			throw new RewardServiceException(e.getMessage());
		}
	}

	@Override
	public void deleteRewardType(RewardType request){

		try{
			RewardTypeDAO rewardType = this.mapper.map(request, RewardTypeDAO.class);
			this.rewardDB.deleteRewardType(rewardType);

		} catch (MappingException | RewardDBException e) {
			throw new RewardServiceException(e.getMessage());
		}
	}

	public List<CreateRewardTypeResponse> getRewardTypes() {

		List<CreateRewardTypeResponse> result = new ArrayList<CreateRewardTypeResponse>();

		try {
			List<RewardTypeDAO> rewardTypeDAOList = this.rewardDB.getRewardTypes();

			for (RewardTypeDAO dao : rewardTypeDAOList) {
				result.add(this.mapper.map(dao, CreateRewardTypeResponse.class));
			}

		} catch (RewardDBException e) {
			throw new RewardServiceException(e.getMessage());
		}

		return result;
	}


	private String depositReward(String toAddress, String rewardName, Integer amount) {

		String assetId = getAssetIdForRewardName(rewardName);

		AssetTransferRequest req = new AssetTransferRequest(
			new BlockchainAccountDTO(this.config.getRewardDispenserAccount(), this.config.getRewardDispenserKey()),
			new BlockchainAccountDTO(toAddress, null),
			amount,
			"rewards module asset transfer",
			assetId
		);
		String txId = this.blockchainService.transferAsset(req);

		return txId;
	}

	private String getAssetIdForRewardName(String rewardTypeName) {

		String assetId;

		try {
			RewardTypeDAO rewardTypeDAO = this.rewardDB.getRewardTypeByName(rewardTypeName);
			assetId = rewardTypeDAO.getAssetId();
		} catch (RewardDBException e) {
			throw new RewardServiceException(e.getMessage());
		}

		return assetId;
	}

	private CalculateRewardLevelResponse calculateRewardLevelForRewardType(String rewardHolderId, String rewardType) {
		
		List<IssueRewardResponse> rewardList;
		Integer rewardTotal = 0;
		RewardLevelDAO rewardLevelDao;
		RewardLevelDTO rewardLevelDto;
		CalculateRewardLevelResponse result;
		ShowRewardRequestDTO req = new ShowRewardRequestDTO();

		req.setReceiverId(rewardHolderId);
		req.setRewardType(rewardType);

		try {
			rewardList = this.showRewards(req);

			for (IssueRewardResponse r : rewardList) {
				rewardTotal += r.getAmount();
			}

			rewardLevelDao = this.rewardDB.queryRewardLevels(rewardType, rewardTotal);

			rewardLevelDto = this.mapper.map(rewardLevelDao, RewardLevelDTO.class);

			String assetId = getAssetIdForRewardName(rewardType);

			Integer balance = this.blockchainService.getAssetBalance(
				this.orgService.getPerson(rewardHolderId).getPaymentAccount().getBlockchainAddress(),
				assetId
			);

			if (balance == null) {
				balance = 0;
			}

			result = new CalculateRewardLevelResponse(rewardLevelDto, balance);

		} catch (RewardDBException e) {
			throw new RewardServiceException(e.getMessage());
		} catch (BlockchainServiceException eb) {
			throw new RewardServiceException(String.format("Failed to get blockchain account balance: %s", eb.getMessage()));
		} catch (OrganizationServiceException eo) {
			throw new RewardServiceException(String.format("Failed to get person: %s", eo.getMessage()));
		}

		return result;
	}
}