package org.onestep.relief.utility.rewards.client.impl;

import org.onestep.relief.utility.rewards.exceptions.OrganizationServiceException;
import org.onestep.relief.utility.rewards.client.OrganizationServiceInterface;
import org.onestep.relief.utility.rewards.client.stubs.PersonDTO;
import org.onestep.relief.utility.rewards.client.stubs.OrgDTO;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import org.glassfish.jersey.client.JerseyClientBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OrganizationService implements OrganizationServiceInterface {

	private static Logger logger = LoggerFactory.getLogger(OrganizationService.class);

	private String url;
	private Client client;

	public OrganizationService(String url) {
		this.url = url;
		this.client = new JerseyClientBuilder().build();
	}

	public PersonDTO getPerson(String id) {

		PersonDTO person = null;

		WebTarget webTarget = this.client.target(url + "/persons/" + id);
		Invocation.Builder invocationBuilder =  webTarget.request()
			.accept(MediaType.APPLICATION_JSON)
			.accept(MediaType.TEXT_PLAIN);
		Response response = invocationBuilder.get();

		logger.debug("organization response = {}", response);

		int status = response.getStatusInfo().getStatusCode();
		
		if (status == Response.Status.OK.getStatusCode()) {
			person = response.readEntity(PersonDTO.class);
		} else {
			throw new OrganizationServiceException(response.readEntity(String.class));
		}

		return person;
	}

	public OrgDTO getOrg(String id) {

		OrgDTO org = null;

		WebTarget webTarget = this.client.target(url + "/organizations/" + id);
		Invocation.Builder invocationBuilder =  webTarget.request()
			.accept(MediaType.APPLICATION_JSON)
			.accept(MediaType.TEXT_PLAIN);
		Response response = invocationBuilder.get();

		logger.debug("organization response = {}", response);

		int status = response.getStatusInfo().getStatusCode();
		
		if (status == Response.Status.OK.getStatusCode()) {
			org = response.readEntity(OrgDTO.class);
		} else {
			throw new OrganizationServiceException(response.readEntity(String.class));
		}

		return org;
	}

}