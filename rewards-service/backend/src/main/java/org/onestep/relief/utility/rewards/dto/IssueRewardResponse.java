package org.onestep.relief.utility.rewards.dto;

import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;
import lombok.NonNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IssueRewardResponse {

	@JsonProperty
	private String reason;

	@JsonProperty
	private String rewardType;

	@JsonProperty
	private Integer amount;

	@JsonProperty
	private String issuerId;

	@JsonProperty
	private String receiverId;

	@JsonProperty
	private ReceiverType receiverType;

	@JsonProperty
	private String transactionId;

	@JsonProperty
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime date;

	public enum ReceiverType {
		Person,
		Org;
	}

}