package org.onestep.relief.utility.rewards.client.stubs;

import java.util.ArrayList;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetUserFromTokenResponse {

	@JsonProperty
	private String message;

	@JsonProperty
	private UserDTO data;

}