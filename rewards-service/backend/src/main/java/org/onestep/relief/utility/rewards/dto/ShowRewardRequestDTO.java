package org.onestep.relief.utility.rewards.dto;

import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.*;
import lombok.Data;

@Data
public class ShowRewardRequestDTO {

	@JsonProperty
	private String issuerId;

	@JsonProperty
	@NotEmpty
	private String receiverId;

	@JsonProperty
	private String rewardType;

	@JsonProperty
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime fromDate;

	@JsonProperty
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime toDate;

}