package org.onestep.relief.utility.rewards.client.stubs;

import java.util.ArrayList;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AssetBalanceDTO {

	@JsonProperty
	private Integer amount;

	@JsonProperty
	private String creator;

	@JsonProperty
	private String assetId;

	@JsonIgnore
	private String name;

}