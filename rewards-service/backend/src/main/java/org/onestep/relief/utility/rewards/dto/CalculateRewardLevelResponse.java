package org.onestep.relief.utility.rewards.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CalculateRewardLevelResponse {

	@JsonProperty
	private RewardLevelDTO rewardLevel;

	@JsonProperty
	private Integer rewardTotal;

}