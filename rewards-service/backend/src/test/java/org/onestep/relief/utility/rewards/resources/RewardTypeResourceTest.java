package org.onestep.relief.utility.rewards.resources;

import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import io.dropwizard.testing.junit5.ResourceExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.onestep.relief.utility.rewards.dto.CreateRewardTypeRequest;
import org.onestep.relief.utility.rewards.dto.CreateRewardTypeResponse;
import org.onestep.relief.utility.rewards.dto.GenericResponse;
import org.onestep.relief.utility.rewards.exceptions.RewardServiceException;
import org.onestep.relief.utility.rewards.service.RewardServiceInterface;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(DropwizardExtensionsSupport.class)
public class RewardTypeResourceTest {

    private static final RewardServiceInterface service = mock(RewardServiceInterface.class);

    private static final ResourceExtension extension = ResourceExtension.builder()
            .addResource(new RewardTypeResource(service))
            .build();

    @Test
    public void createRewardTypeTest(){

        CreateRewardTypeRequest createRewardTypeRequest = new CreateRewardTypeRequest();
        CreateRewardTypeResponse createRewardTypeResponse = new CreateRewardTypeResponse();

        when(service.createRewardType(any(CreateRewardTypeRequest.class)))
                .thenReturn(createRewardTypeResponse);

        Entity<?> entity = Entity.entity(createRewardTypeRequest, MediaType.APPLICATION_JSON_TYPE);

        Response response = extension
                .target("/rewardtype/create")
                .request()
                .post(entity);

        GenericResponse genericResponse = response.readEntity(GenericResponse.class);

        Assertions.assertEquals(genericResponse.getMessage(), "Created reward type successfully.");
        verify(service).createRewardType(any(CreateRewardTypeRequest.class));

        when(service.createRewardType(any(CreateRewardTypeRequest.class)))
                .thenThrow(new RewardServiceException("Failed to create reward type"));

        response = extension
                .target("/rewardtype/create")
                .request()
                .post(entity);

        genericResponse = response.readEntity(GenericResponse.class);

        Assertions.assertEquals(genericResponse.getMessage(), "Failed to create reward type");
        Assertions.assertEquals(response.getStatus(), 400);
    }

    @Test
    public void getRewardTypeTest(){

        List<CreateRewardTypeResponse> rewardTypes = Arrays.asList(new CreateRewardTypeResponse());

        when(service.getRewardTypes()).thenReturn(rewardTypes);

        GenericResponse genericResponse = extension
                .target("/rewardtype/getRewardType").request().get(GenericResponse.class);

        Assertions.assertEquals(genericResponse.getMessage(), "Fetched reward types successfully.");
        verify(service).getRewardTypes();
    }
}
