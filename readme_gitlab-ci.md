# Automate docker image builds, with Gitlab CI CD pipleline

Given a directory with project files and docker files set up, and successfully tested locally to build a runnable docker image, the process can be automated in gitlab. In our case, the "blockchain-service" directory has all the project files and docker files set up to run.

To separate docker builds from different projects in the same repository, a blank .gitlab-ci.yml file is created, and an include statement were used to point to another yml file that has the instructions to build the blockchain-service CI CD pipeline.

## .gitlab-ci.yml file:

```
include:
  - /.gitlab-ci-blockchain-service.yml
  - /.gitlab-ci-another-service.yml
```

The gitlab-ci-blockchain-service.yml file has instructions for gitlab to build the docker images, and upload them to the Gitlab Container Registry, and the AWS Elastic Container Registry (ECR).

## .gitlab-ci-blockchain-service.yml file

```
# Blockchain
# Build docker image
# Release it to Gitlab container registry
#
release:blockchain-service-docker:
  rules:
    ## start the pipeline manually from the gitlab website ci/cd section
    - if: $CI_PIPELINE_IID == $CI_PIPELINE_IID
      when: manual
  variables:
    GITLAB_REGISTRY: registry.gitlab.com
    GITLAB_IMAGE: registry.gitlab.com/cscie599sec1spring21/team-2-project-area/blockchain-service:blockchain-service-$CI_COMMIT_BRANCH-$CI_PIPELINE_IID
  image: docker:19.03.12
  services:
    - docker:19.03.12-dind
  stage: release
  before_script:
    - echo branch is $CI_COMMIT_BRANCH
    - cat .gitlab-ci-blockchain-service.yml
    - docker --version
  script:
    ## build and tag image
    ## all files in the repository are accessible
    - docker build -t $GITLAB_IMAGE -f ./blockchain-service/blockchain-service-dockerfile ./blockchain-service
    ## run test
    - docker run --entrypoint="" $GITLAB_IMAGE sh -c "set -e; mvn test --file /blockchain-service/pom.xml"
    ## run test
    - docker run --entrypoint="" $GITLAB_IMAGE sh -c "set -e; java -jar /blockchain-service/target/blockchain-1.0-SNAPSHOT.jar check"
    ## login to gitlab container registry
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $GITLAB_REGISTRY
    ## push the image to the registry
    - docker push $GITLAB_IMAGE
#
# Deploy container to kubernetes cluster
#
deploy:blockchain-service-docker:
  rules:
    - if: $CI_PIPELINE_IID == $CI_PIPELINE_IID
      when: on_success
  variables:
    GITLAB_REGISTRY: registry.gitlab.com
    GITLAB_IMAGE: registry.gitlab.com/cscie599sec1spring21/team-2-project-area/blockchain-service:blockchain-service-$CI_COMMIT_BRANCH-$CI_PIPELINE_IID
    KUBE_YML: ./blockchain-service/kube-blockchain-service.yml
  image:
    name: ubuntu:latest
  stage: deploy
  needs: ["release:blockchain-service-docker"]
  script:
    - sh .gitlab-ci-deploy-kube.sh
    - kubectl apply -f ${KUBE_YML}
#
```

## Project specific variables:

`GITLAB_REGISTRY` is where gitlab will store the image
`GITLAB_TAG` is the specific location to store the image. The path in this tag should match the path of the repository. The text after the last `/` is the name of the image, in our case `blockchain-service`. The remainging text describe the image tag, which is made of the branch name that the image came from, plus the specific pipeline iteration number the image came from. For example, `blockchain-service:main-249` is the image for blockchain-service, built from the main branch on pipeline #249.

All the following variables, and `image`, `services`, `stage`, `before_script` can be kept constant for other projects. The items under `script` will need to be modified. The docker build command should be similar to the command used to run docker locally, with the addition of the tags. All the subsequent commands should be unchanged for other projects. The variables `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` will need to be specified in gitlab settings, like shown in this video [https://www.youtube.com/watch?v=jg9sUceyGaQ .](https://youtu.be/jg9sUceyGaQ?t=988)

## Multiple jobs

The `.gitlab-ci-blockchain-service.yml` file has multiple jobs, each under its own header. `build:blockchain-service-docker` , `test:blockchain-service-docker` , and `test:blockchain:`. Each job ran independently, but may have required the outputs from each other. The `test-blockchain-service-docker` is dependent on the image created by `build-blockchain-service-docker`.

With these `.yml` files pushed to gitlab, the pipeline should start running, and new images should appear in the container registry, and in AWS ECR. They can be deployed to a EKS cluster using kubectl commands.
