package org.onestep.relief.utility.blockchain.service.impl;

import com.algorand.algosdk.v2.client.common.IndexerClient;

import static org.onestep.relief.utility.blockchain.util.BlockchainConstants.*;

/**
 * Implements a singleton for the IndexerClient instance. Less expensive than
 * creating a new client
 * each time we need it. Just need to update parameters when used.
 */
public class AlgoIndexerClient extends IndexerClient {

    // Singleton object to return. Only 1 client allowed
    private static IndexerClient instance;

    private AlgoIndexerClient(String host) {
        super(host, 443);
    }

    /**
     * Added for unit testing to ensure clients are setup for correct API
     */
    public String getHost() {
        return super.getHost();
    }

    public int getPort() {
        return super.getPort();
    }

    /**
     * Public method used to create singleton
     *
     * @return single instance of IndexerClient
     */
    // Method to create Singleton
    public static IndexerClient getInstance() {
        if (instance == null) {
            // if instance is null, initialize
            instance = new IndexerClient(ALGO_EXPLORER_API_INDEXER, 443);
        }
        return instance;
    }
}
