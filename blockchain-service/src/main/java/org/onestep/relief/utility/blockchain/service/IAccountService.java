package org.onestep.relief.utility.blockchain.service;

import org.onestep.relief.utility.blockchain.dto.BalancesResponse;
import org.onestep.relief.utility.blockchain.model.AccountBalance;
import org.onestep.relief.utility.blockchain.model.Asset;
import org.onestep.relief.utility.blockchain.model.TransactionContribution;
import org.onestep.relief.utility.blockchain.model.AccountHistory;

import java.io.IOException;
import java.util.List;

public interface IAccountService {

    com.algorand.algosdk.account.Account createAccount();

    AccountHistory getAccountHistory(String address) throws IOException;

    AccountBalance getAccountBalance(String address) throws Exception;

    Asset[] getAccountAssets(String address) throws Exception;

    List<TransactionContribution> getAccountTransactionContributions(String address, String startDate, String endDate) throws Exception;

    BalancesResponse getBalanceHistory(String accountAddress, int days) throws Exception;

}
