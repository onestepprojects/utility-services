package org.onestep.relief.utility.blockchain.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class AccountBalance implements Entity {

    private long algoBalance;

    private List<Asset> assets;

    public AccountBalance() {
        this.assets = new ArrayList<>();
    }

    public AccountBalance(long algoBalance) {
        this.algoBalance = algoBalance;
        this.assets = new ArrayList<>();
    }
}
