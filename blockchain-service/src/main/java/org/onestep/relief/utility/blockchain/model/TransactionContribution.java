package org.onestep.relief.utility.blockchain.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Transaction contribution statistics associated with existing account.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionContribution {
    public TransactionContribution(long assetId, String assetName) {
        this.assetId = assetId;
        this.assetName = assetName;
    }

    public TransactionContribution(long assetId, String assetName, int decimals) {
        this(assetId, assetName);
        this.decimals = decimals;
    }

    @JsonProperty
    private long assetId = 0;

    @JsonProperty
    private String assetName = "ALGO";

    @JsonProperty
    private int decimals = 6;

    /** Number of contributions. */
    @JsonProperty
    private long transactionCount = 0;

    /** Total amount of tokens contributed. */
    @JsonProperty
    private long transactionTotalAmount = 0;

    /**
     * Adjusted display amount of tokens contributed which divided by asset
     * `decimals`.
     */
    @JsonProperty
    private long getTransactionDisplayAmount() {
        return this.transactionTotalAmount / (long) Math.pow(10, this.decimals);
    };

    public void add(long amount) {
        this.transactionTotalAmount += amount;
        this.transactionCount++;
    }
}
