package org.onestep.relief.utility.blockchain.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class TransactionHistoryCfgAsset extends AbstractTransactionHistory{

    @JsonProperty
    private Long assetId;
    @JsonProperty
    private String name;
    @JsonProperty
    private Long total;

    public TransactionHistoryCfgAsset(Long date, String name, String type,
                                      Long assetId, Long total) {

        super(date,null,null,null,null,type);
        this.assetId = assetId;
        this.name = name;
        this.total = total;
    }
}
