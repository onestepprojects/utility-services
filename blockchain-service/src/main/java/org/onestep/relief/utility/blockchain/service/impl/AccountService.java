package org.onestep.relief.utility.blockchain.service.impl;

import com.algorand.algosdk.crypto.Address;
import com.algorand.algosdk.v2.client.common.Response;
import com.algorand.algosdk.v2.client.indexer.LookupAccountTransactions;
import com.algorand.algosdk.v2.client.model.AssetHolding;
import com.algorand.algosdk.v2.client.model.Enums.TxType;
import com.algorand.algosdk.v2.client.model.Transaction;
import com.algorand.algosdk.v2.client.model.TransactionsResponse;
import org.onestep.relief.utility.blockchain.dto.BalanceHistoryResponse;
import org.onestep.relief.utility.blockchain.dto.BalancesResponse;
import org.onestep.relief.utility.blockchain.model.*;
import org.onestep.relief.utility.blockchain.service.IAccountService;
import org.onestep.relief.utility.blockchain.util.HTTPRequests;
import org.onestep.relief.utility.blockchain.util.TransactionHistoryFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.GenericType;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.onestep.relief.utility.blockchain.util.BlockchainConstants.*;

public class AccountService implements IAccountService {

    private Logger logger = LoggerFactory.getLogger(AccountService.class);

    private CommonService service;

    public AccountService(CommonService service) {

        this.service = service;
    }

    @Override
    public com.algorand.algosdk.account.Account createAccount() {

        com.algorand.algosdk.account.Account account = null;

        try {

            account = new com.algorand.algosdk.account.Account();

        } catch (GeneralSecurityException e) {
            logger.error("Unable to create account: " + e.getMessage());
        }

        return account;
    }

    @Override
    public AccountBalance getAccountBalance(String accountAddress) throws Exception {

        Address account;
        Response<com.algorand.algosdk.v2.client.model.Account> response;
        AccountBalance balance = new AccountBalance();

        try {
            account = new Address(accountAddress);

            response = AlgoClient.getInstance().AccountInformation(account)
                    .execute(new String[]{API_KEY_HEADER}, new String[]{PURESTAKE_API_KEY});

        } catch (GeneralSecurityException e) {
            logger.error("Unable to create account: " + e.getMessage());
            throw e;
        }

        if (Objects.nonNull(response)) {

            List<AssetHolding> assets = response.body().assets;

            for (AssetHolding asset : assets) {
                String name = null;
                if (asset.assetId.equals(ASSETID_USDC)) {
                    name = "USDC";
                } else if (asset.assetId.equals(ASSETID_STEPTOKEN)) {
                    name = "Step Token";
                }
                balance.getAssets().add(new Asset(asset.amount.longValue(), asset.creator,
                        asset.assetId, name));
            }

            balance.setAlgoBalance(response.body().amount);
        }

        return balance;

    }

    @Override
    public Asset[] getAccountAssets(String accountAddress) throws Exception {
        javax.ws.rs.core.Response response;

        response = HTTPRequests.GetRequest(PAGO_API_ADDR + "account/balances?address=" + accountAddress);

        Map<String, String>[] maps = response.readEntity(new GenericType<>() {
        });

        Asset[] assets = new Asset[maps.length];
        for (int i = 0; i < maps.length; i++) {
            Map<String, String> map = maps[i];
            String name = map.get("unitName");
            long assetId = Long.parseLong(map.get("assetId"));
            long amount = Long.parseLong(map.get("amount"));
            Asset asset = new Asset(name, assetId, amount);
            asset.setCreator(map.get("creatorAccount"));
            assets[i] = asset;
        }

        return assets;
    }

    @Override
    public List<TransactionContribution> getAccountTransactionContributions(String accountAddress, String startDate, String endDate) throws Exception {
        Address accountId = new Address(accountAddress);

        // filter by startDate/endDate
        LookupAccountTransactions config = AlgoIndexerClient.getInstance().lookupAccountTransactions(accountId);
        if (startDate != null) config.afterTime(new SimpleDateFormat("yyyy-MM-dd").parse(startDate));
        if (endDate != null) config.beforeTime(new SimpleDateFormat("yyyy-MM-dd").parse(endDate));
        Response<TransactionsResponse> response = config.execute();

        if (response.body() == null) {
            return List.of();
        }

        // TransactionContribution algoTransactionContribution = new TransactionContribution(0, "ALGO");
        TransactionContribution usdcTransactionContribution = new TransactionContribution(ASSETID_USDC, "USDC");

        List<TransactionContribution> transactionContributions = List.of(
                // algoTransactionContribution,
                usdcTransactionContribution);
        response.body().transactions.forEach(transaction -> {
            if (transaction.txType == TxType.AXFER && transaction.assetTransferTransaction != null
                    && transaction.assetTransferTransaction.receiver.equals(accountAddress)) {
                long assetId = transaction.assetTransferTransaction.assetId;
                long amount = transaction.assetTransferTransaction.amount.longValue();

                // if (algoTransactionContribution.getAssetId() == assetId) {
                // algoTransactionContribution.add(amount);
                // }
                if (usdcTransactionContribution.getAssetId() == assetId) {
                    usdcTransactionContribution.add(amount);
                }
            }
        });

        return transactionContributions;
    }

    @Override
    public AccountHistory getAccountHistory(String accountAddress) {

        Address account;
        Response<TransactionsResponse> response = null;
        AccountHistory history = new AccountHistory();

        try {
            account = new Address(accountAddress);

            response = IndexClient.getInstance().lookupAccountTransactions(account)
                    .execute(new String[]{API_KEY_HEADER}, new String[]{PURESTAKE_API_KEY});

        } catch (GeneralSecurityException e) {
            logger.error("Unable to create account" + e.getMessage());
        } catch (Exception e) {
            logger.error("Unable to get account information" + e.getMessage());
        }

        if (Objects.nonNull(response)) {

            List<Transaction> transactions = response.body().transactions;

            for (com.algorand.algosdk.v2.client.model.Transaction txn : transactions) {

                AbstractTransactionHistory transaction = TransactionHistoryFactory.createTransaction(txn);

                history.getTransactions().add(transaction);
            }
        }

        return history;
    }

    @Override
    public BalancesResponse getBalanceHistory(String accountAddress, int days) {

        javax.ws.rs.core.Response response;
        String path = ALGO_EXPLORER_API;
        path = path.replace("{address}", accountAddress);
        path = path.replace("{days}", String.valueOf(days));

        try {
            response = HTTPRequests.GetRequest(path);
        } catch (Exception e) {
            throw e;
        }
        ArrayList<BalanceHistoryResponse> balances = (ArrayList<BalanceHistoryResponse>) response
                .readEntity(List.class);
        logger.info(String.valueOf(balances));

        BalancesResponse balancesResponse = new BalancesResponse();
        balancesResponse.setBalanceHistory(balances);
        return balancesResponse;

    }
}
