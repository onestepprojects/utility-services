package org.onestep.relief.utility.blockchain.util;

import com.algorand.algosdk.v2.client.common.AlgodClient;
import com.algorand.algosdk.v2.client.model.PendingTransactionResponse;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.servlets.assets.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.InvalidParameterException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import static org.onestep.relief.utility.blockchain.util.AWSConstants.*;


public class BlockchainUtils {

    private static Logger logger = LoggerFactory.getLogger(BlockchainUtils.class);

    public static String getSecret()  {

        logger.info(BLOCKCHAIN_SECRETSMANAGER_ID);
        logger.info(BLOCKCHAIN_SECRETSMANAGER_SECRET);


        // Create a Secrets Manager client
        AWSSecretsManager client = AWSSecretsManagerClientBuilder.standard()
                .withRegion(AWS_SECRETSMANAGER_REGION)
                .withCredentials(new AWSStaticCredentialsProvider(new
                        BasicAWSCredentials(BLOCKCHAIN_SECRETSMANAGER_ID, BLOCKCHAIN_SECRETSMANAGER_SECRET)))
                .build();

        // In this sample we only handle the specific exceptions for the 'GetSecretValue' API.
        // See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
        // We rethrow the exception by default.

        String secret = null;
        String decodedBinarySecret = null;
        GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest()
                .withSecretId(SECRETS_MANAGER_KEY);
        GetSecretValueResult getSecretValueResult;

        try {
            getSecretValueResult = client.getSecretValue(getSecretValueRequest);
        } catch (DecryptionFailureException e) {
            // Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (InternalServiceErrorException e) {
            // An error occurred on the server side.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (InvalidParameterException e) {
            // You provided an invalid value for a parameter.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (InvalidRequestException e) {
            // You provided a parameter value that is not valid for the current state of the resource.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (ResourceNotFoundException e) {
            // We can't find the resource that you asked for.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        }

        // Decrypts secret using the associated KMS CMK.
        // Depending on whether the secret is a string or binary, one of these fields will be populated.
        if (getSecretValueResult.getSecretString() != null) {
            secret = getSecretValueResult.getSecretString();
        } else {
            decodedBinarySecret = new String(Base64.getDecoder().decode(getSecretValueResult.getSecretBinary()).array());
        }

        return secret;
    }


    // Function from Algorand Inc. - Utility function to wait on a transaction to be confirmed

    /**
     convenience method to check that transaction is on the blockchain
     */
    public static PendingTransactionResponse waitForConfirmation(AlgodClient client, String txId, String[] headers, String[] values) throws Exception {
        Long lastRound = client.GetStatus().execute(headers, values).body().lastRound;


        while (true) {
            try {
                // Check the pending tranactions
                PendingTransactionResponse pendingInfo = client.PendingTransactionInformation(txId).execute(headers, values).body();
                if (pendingInfo.confirmedRound != null && pendingInfo.confirmedRound > 0) {
                   logger.info("Transaction confirmed in round " + pendingInfo.confirmedRound);
                   logger.info(pendingInfo.toString());
                    return pendingInfo;
                }
                lastRound++;
                client.WaitForBlock(lastRound).execute(headers, values);
            } catch (Exception e) {
                throw (e);
            }
        }
    }

    public static String parseJson(String jsonString, String key) throws JsonProcessingException {
        Map<String, Object> response = new ObjectMapper().readValue(jsonString, HashMap.class);
        return response.get(key).toString();
    }

    public static Map<String, Object> getMnemonic() {

        //2a) Get 'faucet' account credentials from AWS Secrets Manager
        String sourceAccount = null;
        try {
            sourceAccount = BlockchainUtils.getSecret();

        } catch (Exception e) {
            logger.info(e.getMessage());

        }
        Map<String, Object> response = null;
        try {
            response = new ObjectMapper().readValue(sourceAccount, HashMap.class);
        } catch (Exception e) {
            logger.info(e.getMessage());
        }
        return response;
    }
}
