package org.onestep.relief.utility.blockchain.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class Config {
  private static final Logger logger = LoggerFactory.getLogger(Config.class);
  static Properties configFile;
  static Properties pagoConfigFile;

  static {

    configFile = new java.util.Properties();
    pagoConfigFile = new java.util.Properties();

    try {
      configFile.load(Config.class.getClassLoader().getResourceAsStream("blockchainConfig.cfg"));
      pagoConfigFile.load(Config.class.getClassLoader().getResourceAsStream("pago.cfg"));
    } catch (Exception eta) {
      logger.info(eta.getMessage());
    }

  }

  public static String getProperty(String key) {
    String propertyValue = System.getenv(key);
    if (propertyValue == null) {
      return configFile.getProperty(key);
    }

    return propertyValue;
  }

  public static String getPagoProperty(String key) {
    String propertyValue = System.getenv(key);

    if (propertyValue == null)
      return pagoConfigFile.getProperty(key);

    return propertyValue;
  }

  // helper function for getting environment vars or setting defaults
  public static String getEnv(String key) {
    if (System.getenv(key) == null) {
      logger.info("Env var " + key + " not set. Using default");
      return "";
    }
    return System.getenv(key);
  }

}
