package org.onestep.relief.utility.blockchain.resources;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.onestep.relief.utility.blockchain.facade.AccountFacade;
import org.onestep.relief.utility.blockchain.model.ErrorResponse;
import org.onestep.relief.utility.blockchain.model.TransactionContribution;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;

import io.dropwizard.jersey.caching.CacheControl;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Path("onestep/blockchain/account")
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource {

    /**
     * Facade class that interfaces with Algorand SDK.
     * Hides the implementation details from Api clients
     */
    private AccountFacade facade;

    public AccountResource(AccountFacade facade) {
        this.facade = facade;
    }

    /**
     * Creates a new account generating key pairs
     *
     * @return Account
     */
    @GET
    @Path("/createAccount")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createAccount() {
        return Response.status(201).entity(facade.createAccount()).build();
    }

    /**
     * Gets the balance and assets associated with existing accounts.
     * Takes in an address
     *
     * @param address Blockchain address
     * @return AccountBalance
     */
    @GET
    @Path("/getBalance/{address}")
    public Response getAccountBalance(@PathParam("address") String address) {

        try {
            return Response.status(200)
                    .entity(facade.getAccountBalance(address)).build();
        } catch (Exception e) {
            return Response.status(500)
                    .entity(new ErrorResponse("Unable to get account balance: " + e.getMessage())).build();
        }
    }

    /**
     * Gets the USDC balance of account
     * Takes in an address
     *
     * @param address Blockchain address
     * @return JSON object with USDC balance
     */
    @GET
    @Path("/getBalance/usdc/{address}")
    public Response getAccountBalanceUSDC(@PathParam("address") String address) {

        try {
            return Response.status(200)
                    .entity(facade.getAccountBalanceUSDC(address)).build();
        } catch (Exception e) {
            return Response.status(500)
                    .entity(new ErrorResponse("Unable to get account balance: " + e.getMessage())).build();
        }
    }

    /**
     * Gets the assets associated with existing account.
     * Takes in an address
     *
     * @param address Blockchain address
     * @return Asset[]
     */
    @GET
    @Path("/{address}/assets")
    public Response getAccountAssets(@PathParam("address") String address) {
        try {
            return Response.status(200)
                    .entity(facade.getAccountAssets(address)).build();
        } catch (Exception e) {
            return Response.status(500)
                    .entity(new ErrorResponse("Unable to get account assets: " + e.getMessage())).build();
        }
    }

    @Data
    @AllArgsConstructor
    private static class CacheKey {
        String address;
        String startDate;
        String endDate;
    }

    private LoadingCache<CacheKey, List<TransactionContribution>> accountTransactionContributionsLoadingCache = Caffeine
            .newBuilder()
            .maximumSize(100)
            .expireAfterWrite(60, TimeUnit.SECONDS)
            .build(key -> facade.getAccountTransactionContributions(key.address, key.startDate, key.endDate));

    /**
     * Gets the transactions contribution associated with existing account.
     * Takes in an address
     *
     * @param address Blockchain address
     * @return List<TransactionContribution>
     */
    @GET
    @Path("/{address}/transaction-contributions")
    @CacheControl(maxAge = 60, maxAgeUnit = TimeUnit.SECONDS)
    public Response getAccountTransactionContributions(@PathParam("address") String address, @QueryParam("startDate") String startDate,
                                                       @QueryParam("endDate") String endDate) {
        try {
            return Response.status(200)
                    .entity(accountTransactionContributionsLoadingCache.get(new CacheKey(address, startDate, endDate))).build();
        } catch (Exception e) {
            return Response.status(500)
                    .entity(new ErrorResponse("Unable to get account transaction contributions: " + e.getMessage()))
                    .build();
        }
    }

    /**
     * Returns a history of transactions associated with an account address
     * The transactions returned can be payment or asset transactions
     *
     * @param address Blockchain address
     * @return AccountHistory
     */
    @GET
    @Path("/accountHistory/{address}")
    public Response getAccountHistory(@PathParam("address") String address) {

        try {
            return Response.status(200)
                    .entity(facade.getAccountHistory(address)).build();
        } catch (IOException e) {
            return Response.status(500)
                    .entity(new ErrorResponse("Unable to get account history: " + e.getMessage())).build();
        }
    }

    /**
     * Returns a history of transactions associated with an account address
     * The transactions returned can be payment or asset transactions
     *
     * @param address Blockchain address
     * @return AccountHistory
     */
    @GET
    @Path("/{address}/balanceHistory/{days}")
    public Response getBalanceHistory(@PathParam("address") String address, @PathParam("days") int days) {

        try {
            return Response.status(200)
                    .entity(facade.getBalanceHistory(address, days)).build();
        } catch (Exception e) {
            return Response.status(500)
                    .entity(new ErrorResponse("Unable to get balance history: " + e.getMessage())).build();
        }
    }
}
