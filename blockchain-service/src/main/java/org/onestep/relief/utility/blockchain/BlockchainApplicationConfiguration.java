package org.onestep.relief.utility.blockchain;

import io.dropwizard.Configuration;

/**
 * Creating A Configuration Class
 * https://www.dropwizard.io/en/latest/getting-started.html#creating-a-configuration-class
 */
public class BlockchainApplicationConfiguration extends Configuration {

}
