package org.onestep.relief.utility.blockchain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProvisionPayResponse {

    private String blockchainAddress;
    private List<Asset> assets = new ArrayList<>();

    public void addAsset(Asset toAdd) {
        assets.add(toAdd);
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Asset {
        private String assetId;
        private String txId;
    }


}
