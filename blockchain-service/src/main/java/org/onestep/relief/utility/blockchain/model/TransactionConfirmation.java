package org.onestep.relief.utility.blockchain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionConfirmation implements Entity{

    private Long applicationIndex;

    private Long confirmedRound;

    private Long closingAmount;

    private Long closeRewards;
}
