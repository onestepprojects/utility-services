package org.onestep.relief.utility.blockchain.service.impl;

import com.algorand.algosdk.transaction.SignedTransaction;
import com.algorand.algosdk.transaction.Transaction;
import com.algorand.algosdk.util.Encoder;
import com.algorand.algosdk.v2.client.common.AlgodClient;
import com.algorand.algosdk.v2.client.common.IndexerClient;
import com.algorand.algosdk.v2.client.common.Response;
import com.algorand.algosdk.v2.client.model.*;
import org.apache.http.HttpStatus;
import org.onestep.relief.utility.blockchain.model.Asset;
import org.onestep.relief.utility.blockchain.model.*;
import org.onestep.relief.utility.blockchain.service.IPagoService;
import org.onestep.relief.utility.blockchain.service.ITransactionService;
import org.onestep.relief.utility.blockchain.util.TransactionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.GenericType;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import static org.onestep.relief.utility.blockchain.util.BlockchainConstants.*;

public class TransactionService implements ITransactionService {

    AlgodClient algoClient = AlgoClient.getInstance();
    IndexerClient indexClient = IndexClient.getInstance();
    private final Logger logger = LoggerFactory.getLogger(TransactionService.class);
    private final CommonService service;
    private final IPagoService pagoService;
    public TransactionService(CommonService service, IPagoService pagoService) {

        this.service = service;
        this.pagoService = pagoService;
    }

    @Override
    public TransactionParametersResponse getTransactionParameters() {

        TransactionParametersResponse params = null;

        try {
            params = algoClient.TransactionParams()
                    .execute(new String[]{API_KEY_HEADER}, new String[]{PURESTAKE_API_KEY}).body();

        } catch (Exception e) {
            logger.error("Unable to get transaction parameters " + e.getMessage());
            //todo throw e;
        }

        return params;
    }

    @Override
    public Transaction createTransaction(NewTransaction newTransaction) {

        Transaction transaction;
        transaction = TransactionFactory.createTransaction(newTransaction);
        return transaction;
    }


    @Override
    public SignedTransaction signTransaction(com.algorand.algosdk.account.Account account, Transaction transaction) {

        SignedTransaction signedTransaction = null;

        try {

            signedTransaction = account.signTransaction(transaction);

            logger.info("Signed txn with id " + signedTransaction.transactionID);

        } catch (NoSuchAlgorithmException e) {
            logger.error("Unable to sign transaction" + e.getMessage());
            //todo throw e;
        }

        return signedTransaction;
    }

    @Override
    public String postTransaction(SignedTransaction signedTransaction) throws Exception {

        String[] headers = {API_KEY_HEADER, CONTENT_TYPE};
        String[] values = {PURESTAKE_API_KEY, X_BINARY_CONTENT_TYPE};

        String transactionId;
        logger.info("signed transaction: ");
        logger.info(signedTransaction.toString());

        try {

            byte[] bytes = Encoder.encodeToMsgPack(signedTransaction);

            PostTransactionsResponse response = algoClient.RawTransaction()
                    .rawtxn(bytes).execute(headers, values).body();

            transactionId = response.txId;

            logger.info("Posted txn with id " + transactionId);

        } catch (Exception e) {
            logger.error("Unable to post transaction: " + e.getMessage());
            throw e;
        }

        return transactionId;
    }

    @Override
    public PendingTransactionResponse getTransactionStatus(String trnsactionId) throws Exception {

        PendingTransactionResponse response;

        try {

            response = algoClient.PendingTransactionInformation(trnsactionId)
                    .execute(new String[]{API_KEY_HEADER}, new String[]{PURESTAKE_API_KEY}).body();

            if (response.confirmedRound != null && response.confirmedRound > 0) {
                logger.info("Committed transaction " + response.txn.transactionID);
                return response;
            }

        } catch (Exception e) {
            logger.error("Unable to retrieve transaction status" + e.getMessage());
            throw e;
        }

        return response;
    }

    /**
     * Given an asset ID, returns information about asset, namely the asset name
     *
     * @param assetId long number
     * @return asset object
     * @throws Exception if unable to retrieve or if ID is invalid
     */
    @Override
    public Asset getAsset(long assetId) throws Exception {

        Asset asset = new Asset();
        Response<AssetResponse> response;

        try {
            response = indexClient.lookupAssetByID(assetId)
                    .execute(new String[]{API_KEY_HEADER}, new String[]{PURESTAKE_API_KEY});
        } catch (Exception e) {
            logger.error("Unable to get asset information" + e.getMessage());
            throw e;
        }

        AssetParams params = response.body().asset.params;
        asset.setName(params.name);
        asset.setAssetId(assetId);
        asset.setCreator(params.creator);

        return asset;
    }

    /**
     * Given an transaction ID, returns information about transaction from indexer. Used for older
     * transactions as they are not perpetually available on the blockchain.
     * <p>
     * For immediate transactions, use getTransactionStatus
     *
     * @param txId String
     * @return appropriate implementation of AbstractTransactionHistory
     * @throws Exception if unable to retrieve or if ID is invalid
     * @see TransactionService#getTransactionStatus(String)
     */
    @Override
    public AbstractTransactionHistory getTransaction(String txId) throws Exception {

        Response<TransactionsResponse> response;

        try {
            response = indexClient
                    .searchForTransactions()
                    .txid(txId)
                    .execute(new String[]{API_KEY_HEADER}, new String[]{PURESTAKE_API_KEY});
        } catch (Exception e) {
            logger.error("Unable to get asset information " + e.getMessage());
            throw e;
        }

        String txType = response.body().transactions.get(0).txType.toString();


        AbstractTransactionHistory transaction = new TransactionHistoryPayment();

        com.algorand.algosdk.v2.client.model.Transaction params =
                response.body().transactions.get(0);


        transaction.setSender((new AccountModel(params.sender)));
        if (params.note != null) {
            transaction.setNote(new String(params.note, StandardCharsets.UTF_8));
        }

        transaction.setDate(params.roundTime);
        transaction.setType(txType);

        if (txType.equals("pay")) {
            transaction.setReceiver((new AccountModel(params.paymentTransaction.receiver)));
            transaction.setAmount(params.paymentTransaction.amount);

        } else if (txType.equals("axfer")) {
            transaction.setReceiver((new AccountModel(params.assetTransferTransaction.receiver)));
            transaction.setAmount(params.assetTransferTransaction.amount.longValue());
        }

        return transaction;
    }

    /**
     * Confirm that input is a valid Pago account by retrieving Pago ID and returning. Caller can test
     * if returned value is null; if it's not, then Pago Id can be used to retrieve public address
     * and this can be used to get balance from blockchain.
     *
     * @param toConfirm For now, this can either be the PagoId OR an public blockchain address registered with Pago
     * @return String value of the account ID if it exists, otherwise Null
     * @throws Exception if either the Pago ID or public address is not valid Pago account
     */
    @Override
    public String confirmPagoAccount(String toConfirm) throws Exception {
        String routePagoId = "account/registration-payid?payid=";
        String routePublicAddress = "account/registration-address?address=";
        javax.ws.rs.core.Response response;

        //First try and see if it's a valid PagoId
        response = pagoService.pagoGetRequest(routePagoId + toConfirm);

        Map<String, String> map = response.readEntity(new GenericType<>() {
        });


        if (map != null && response.getStatus() == HttpStatus.SC_OK) {
            return map.get("id");
        }

        // Otherwise try with a public blockchain address (different route)
        response = pagoService.pagoGetRequest(routePublicAddress + toConfirm);

        map = response.readEntity(new GenericType<>() {
        });

        if (map != null && response.getStatus() == HttpStatus.SC_OK) {
            return map.get("id");
        }
        throw new Exception("Invalid Pago Id ");
    }

    /** Used to retrieve public blockchain address, given the Pago account Id (not a PagoId)
     * @param pagoAccountId UUID of the Pago account
     * @return String blockchain public address
     * @throws Exception if unable to retrive address. Should not throw as the ID has previously been
     *          retrieved from Pago
     */
    @Override
    public String getAddressFromPago(String pagoAccountId) throws Exception {

        String routeRegistration = "account/registration?id=";
        javax.ws.rs.core.Response response;

        response = pagoService.pagoGetRequest(routeRegistration + pagoAccountId);
        Map<String, String> map = response.readEntity(new GenericType<>() {
        });

        if (map != null && response.getStatus() == HttpStatus.SC_OK) {
            return map.get("accountAddress");
        }
        throw new Exception("Unable to retrieve blockchain address from Pago ");

    }
}
