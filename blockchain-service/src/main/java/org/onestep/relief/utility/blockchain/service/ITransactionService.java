package org.onestep.relief.utility.blockchain.service;

import com.algorand.algosdk.algod.client.ApiException;
import com.algorand.algosdk.transaction.SignedTransaction;
import com.algorand.algosdk.transaction.Transaction;
import com.algorand.algosdk.v2.client.model.PendingTransactionResponse;
import com.algorand.algosdk.v2.client.model.TransactionParametersResponse;
import org.onestep.relief.utility.blockchain.model.AbstractTransactionHistory;
import org.onestep.relief.utility.blockchain.model.Asset;
import org.onestep.relief.utility.blockchain.model.NewTransaction;


public interface ITransactionService {

    TransactionParametersResponse getTransactionParameters();


    Transaction createTransaction(NewTransaction newTransaction);


    SignedTransaction signTransaction(com.algorand.algosdk.account.Account account, Transaction transaction) throws ApiException;

    String postTransaction(SignedTransaction signedTransaction) throws Exception;

    PendingTransactionResponse getTransactionStatus(String trnsactionId) throws Exception;

    Asset getAsset(long assetId) throws Exception;

    AbstractTransactionHistory getTransaction(String transactionId) throws Exception;

    String confirmPagoAccount(String pagoId) throws Exception;

    String getAddressFromPago(String pagoAccountId) throws Exception;
}
