package org.onestep.relief.utility.blockchain.service.impl;

import com.algorand.algosdk.v2.client.common.IndexerClient;

import static org.onestep.relief.utility.blockchain.util.BlockchainConstants.*;

/**
 * Implements a singleton for the AlgodClient instance. Less expensive than creating a new client
 * each time we need it. Just need to update parameters when used.
 */
public class IndexClient extends IndexerClient {

    // Singleton object to return. Only 1 client allowed
    private static IndexerClient instance;

    private IndexClient(String host, int port, String token) {

        super(host, port, token);
    }

    /**
     * Added for unit testing to ensure clients are setup for correct API
     */
    public String getHost() { return super.getHost(); }

    public int getPort() { return super.getPort(); }


    /**
     * Public method used to create singleton
     * @return single instance of IndexerClient
     */
    //Method to create Singleton
    public static IndexerClient getInstance() {
        if (instance == null) {
            // if instance is null, initialize
            instance =  new IndexerClient(INDEXER_API_ADDR, INDEXER_API_PORT, ALGOD_API_TOKEN);
        }
        return instance;
    }
}
