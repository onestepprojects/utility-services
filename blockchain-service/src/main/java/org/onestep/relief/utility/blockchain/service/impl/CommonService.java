package org.onestep.relief.utility.blockchain.service.impl;

import com.algorand.algosdk.crypto.Address;
import com.algorand.algosdk.v2.client.common.AlgodClient;
import com.algorand.algosdk.v2.client.model.*;

import org.onestep.relief.utility.blockchain.service.ICommonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;


import static org.onestep.relief.utility.blockchain.util.BlockchainConstants.*;

public class CommonService implements ICommonService {

    private Logger logger = LoggerFactory.getLogger(CommonService.class);
    AlgodClient algoClient = AlgoClient.getInstance();

    @Override
    public NodeStatusResponse checkNodeStatus() {

        NodeStatusResponse status = null;

        try {
            status = algoClient.GetStatus()
                    .execute(new String [] {API_KEY_HEADER}, new String [] {PURESTAKE_API_KEY}).body();
        } catch (Exception e) {
            logger.error("Unable to get node status:" + e.getMessage());
        }

        return status;
    }

    @Override
    public BlockResponse getBlockInfo() {

        BlockResponse block = null;

        if(Objects.nonNull(checkNodeStatus())){

            Long lastRound = checkNodeStatus().lastRound;

            try {
                block = algoClient.GetBlock(lastRound)
                        .execute(new String [] {API_KEY_HEADER}, new String [] {PURESTAKE_API_KEY}).body();
            } catch (Exception e) {
                logger.error("Unable to get block info:" + e.getMessage());
            }

        }
        return block;
    }

    @Override
    public com.algorand.algosdk.v2.client.model.Account getAccountInfo(String address) throws Exception {

        com.algorand.algosdk.v2.client.model.Account account;

        try {
            account = algoClient.AccountInformation(new Address(address))
                    .execute(new String [] {API_KEY_HEADER}, new String [] {PURESTAKE_API_KEY}).body();

        } catch (NoSuchAlgorithmException e) {
            logger.error("Unable to get account information:" + e.getMessage());
            throw e;
        } catch (Exception e) {
            logger.error("Unable to get account information" +  e.getMessage());
            throw e;
        }

        return account;
    }

}
