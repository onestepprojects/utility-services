package org.onestep.relief.utility.blockchain.util;

import com.algorand.algosdk.builder.transaction.AssetCreateTransactionBuilder;
import com.algorand.algosdk.transaction.Transaction;
import com.algorand.algosdk.v2.client.model.TransactionParametersResponse;
import org.onestep.relief.utility.blockchain.model.NewTransaction;
import org.onestep.relief.utility.blockchain.service.impl.AlgoClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.util.Objects;

import static org.onestep.relief.utility.blockchain.util.BlockchainConstants.*;


/**
 * Factory class which contains factory method to implement creation of various transaction types.
 */
public class TransactionFactory {

    private static final Logger logger = LoggerFactory.getLogger(TransactionFactory.class);

    /**
     * Factory method used to create the 3 types of blockchain transactions the module currently supports
     *
     * @param newTransaction Object containing necessary transaction information, including the transaction type.
     * @return Algorand Transaction object
     */
    public static Transaction createTransaction(NewTransaction newTransaction) {

        Objects.requireNonNull(newTransaction.getSender(), "sender is required");

        Transaction transaction = null;
        String type = newTransaction.getTransactionType();


        switch (type) {

            case TRANSACTION_TYPE_PAY -> transaction = Transaction.PaymentTransactionBuilder()
                    .sender(newTransaction.getSender().getAddress())
                    .receiver((newTransaction.getReceiver().getAddress()))
                    .amount(newTransaction.getAmount())
                    .noteUTF8(newTransaction.getNote())
                    .suggestedParams(getTransactionParameters())
                    .build();

            case TRANSACTION_TYPE_AXFER -> transaction = Transaction.AssetTransferTransactionBuilder()
                    .sender(newTransaction.getSender().getAddress())
                    .assetReceiver(newTransaction.getReceiver().getAddress())
                    .assetAmount(newTransaction.getAmount())
                    .assetIndex(newTransaction.getAssetId())
                    .noteUTF8(newTransaction.getNote())
                    .suggestedParams(getTransactionParameters())
                    .build();

            case TRANSACTION_TYPE_ACFG -> {
                //Unit name is capped at 8 characters
                String units = (newTransaction.getUnit().length() > 8)
                        ? newTransaction.getUnit().substring(0, 8) : newTransaction.getUnit();

                transaction = AssetCreateTransactionBuilder.Builder().suggestedParams(getTransactionParameters())
                        .assetName(newTransaction.getName())
                        .assetUnitName(units)
                        .assetTotal(BigInteger.valueOf(newTransaction.getTotal()))
                        .assetDecimals(newTransaction.getDecimals())
                        .sender(newTransaction.getSender().getAddress())
                        .clawback(newTransaction.getClawback_addr())
                        .manager(newTransaction.getManager_addr())
                        .reserve(newTransaction.getReserve_addr())
                        .freeze(newTransaction.getFreeze_addr())
                        .build();
            }

            case "destroy" -> transaction = Transaction.AssetDestroyTransactionBuilder()
                    .sender(newTransaction.getSender().getAddress())
                    .assetIndex(newTransaction.getAssetId())
                    .suggestedParams(getTransactionParameters())
                    .build();
        }

        return transaction;
    }


    //TODO this will eventually replace same method in Blockchain service
    private static TransactionParametersResponse getTransactionParameters() {

        TransactionParametersResponse params = null;

        try {
            params = AlgoClient.getInstance().TransactionParams()
                    .execute(new String[]{API_KEY_HEADER}, new String[]{PURESTAKE_API_KEY}).body();

        } catch (Exception e) {
            logger.error("Unable to get transaction parameters " + e.getMessage());
        }
        return params;
    }

}
