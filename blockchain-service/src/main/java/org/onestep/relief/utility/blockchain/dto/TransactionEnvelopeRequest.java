package org.onestep.relief.utility.blockchain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import static org.onestep.relief.utility.blockchain.util.PagoEnvelopeConstants.*;

@Data
@AllArgsConstructor
public class TransactionEnvelopeRequest {

    public TransactionEnvelopeRequest() {
        this.name = NAME;
        this.description = DESCRIPTION;
        this.applicationId = APPLICATION_ID;
        this.state = STATE;

    }

    private String name;

    private String originator;

    private String applicationId;

    private String description;

    private String state;

    private TransactionPagoRequest transaction;

    @Override
    public String toString() {
        return "{"
                + "\"name\":\"" + name + "\""
                + ",\"originator\":\"" + originator + "\""
                + ",\"applicationId\":\"" + applicationId + "\""
                + ",\"description\":\"" + description + "\""
                + ",\"transaction\":" + transaction
                + "}";
    }
}
