package org.onestep.relief.utility.blockchain.util;

import org.onestep.relief.utility.blockchain.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.onestep.relief.utility.blockchain.util.BlockchainConstants.*;

/**
 * Creates appropriate AbstractTransactionHistory object depending on transaction type from the response. Different
 * transaction types have different properties (asset ID for example) and the returned structure
 * is different for each.
 */
public class TransactionHistoryFactory {

    private static Logger logger = LoggerFactory.getLogger(TransactionHistoryFactory.class);

    /**
     * @param transaction Transactions from an account are passed to this function; they are mapped to the
     *                    appropriate type and returned.The data format is different for each transaction type.
     * @return  Appropriate transaction type instance which extends AbstractTransactionHistory
     */
    public static AbstractTransactionHistory createTransaction(com.algorand.algosdk.v2.client.model.Transaction transaction) {

        Long date = transaction.roundTime;
        String sender = transaction.sender;
        String receiver;
        Long amount;
        String note = transaction.note == null ? "": new String(transaction.note); //decode note, account for no note
        String type = transaction.txType.toString();

        AbstractTransactionHistory response = null;


        switch (type) {
            case TRANSACTION_TYPE_PAY -> {
                receiver = transaction.paymentTransaction.receiver;
                amount = transaction.paymentTransaction.amount;
                response = new TransactionHistoryPayment(date, new AccountModel(sender),
                        new AccountModel(receiver), amount, note, type);
            }
            case TRANSACTION_TYPE_AXFER -> {

                amount = transaction.assetTransferTransaction.amount.longValue();
                Long assetID = transaction.assetTransferTransaction.assetId;
                receiver = transaction.assetTransferTransaction.receiver;

                String name = null;
                if (assetID.equals(ASSETID_USDC)) {
                    name = "USDC";
                } else if (assetID.equals(ASSETID_STEPTOKEN)) {
                    name = "Step Token";
                }

                response = new TransactionHistoryAsset(date, new AccountModel(sender),
                        new AccountModel(receiver), amount, note, type, assetID, name);

            }
            case TRANSACTION_TYPE_ACFG -> {

                amount = transaction.assetConfigTransaction.params.total.longValue();
                Long assetId = transaction.assetConfigTransaction.assetId;
                String name = transaction.assetConfigTransaction.params.name;

                response = new TransactionHistoryCfgAsset(date, name, type, assetId, amount);
            }
        }

        return response;

    }
}
