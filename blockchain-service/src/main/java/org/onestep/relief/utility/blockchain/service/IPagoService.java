package org.onestep.relief.utility.blockchain.service;


import org.onestep.relief.utility.blockchain.dto.SendTransactionRequest;
import org.onestep.relief.utility.blockchain.dto.TransactionEnvelopeRequest;
import org.onestep.relief.utility.blockchain.dto.TransactionPagoRequest;

import javax.ws.rs.core.Response;

public interface IPagoService {

    TransactionEnvelopeRequest createEnvelope(TransactionPagoRequest request);

    TransactionPagoRequest mapTransaction(SendTransactionRequest request, String blockchainAddress);

    String submitTransactionEnvelope(TransactionEnvelopeRequest transactionEnvelopeRequest) throws Exception;

    Response pagoGetRequest(String path);
}
