package org.onestep.relief.utility.blockchain;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;

import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.onestep.relief.utility.blockchain.resources.AccountResource;
import org.onestep.relief.utility.blockchain.resources.ProvisionAccountResource;
import org.onestep.relief.utility.blockchain.resources.TransactionResource;
import org.onestep.relief.utility.blockchain.facade.AccountFacade;
import org.onestep.relief.utility.blockchain.facade.TransactionFacade;
import org.onestep.relief.utility.blockchain.health.AppHealthCheck;
import org.onestep.relief.utility.blockchain.service.impl.*;
import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.ws.rs.client.Client;

public class BlockchainApplication extends Application<BlockchainApplicationConfiguration> {

    @Override
    public void run(BlockchainApplicationConfiguration configuration, Environment environment) {

        // Enable CORS headers
        final FilterRegistration.Dynamic cors = environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        cors.setInitParameter("allowedOrigins", "*");
        cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin,Authorization");
        cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");
        cors.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");

        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        cors.setInitParameter(CrossOriginFilter.CHAIN_PREFLIGHT_PARAM, Boolean.FALSE.toString());

        // Now we added REST Client Resource named RESTClientController
        final Client client = new JerseyClientBuilder().build();

        environment.jersey()
                .register(new AccountResource(new AccountFacade(
                        new AccountService(new CommonService()))));

        environment.jersey()
                .register(new TransactionResource(new TransactionFacade(new TransactionService(new CommonService(),
                        new PagoService(client)), new CommonService(), new PagoService(client))));
        environment.jersey()
                .register(new ProvisionAccountResource(new TransactionFacade(new TransactionService(new CommonService(),
                        new PagoService(client)), new CommonService(), new PagoService(client)),
                        new AccountFacade(new AccountService(new CommonService()))));
        environment.jersey()
                .register(new TransactionService(new CommonService(), new PagoService(client)));

        environment.healthChecks().register("AppHealthCheck", new AppHealthCheck());

    }

    public static void main(String... args) throws Exception {
        new BlockchainApplication().run(args);
    }
}
