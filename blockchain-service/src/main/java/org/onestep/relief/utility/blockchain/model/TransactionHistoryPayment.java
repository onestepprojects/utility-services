package org.onestep.relief.utility.blockchain.model;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class TransactionHistoryPayment extends AbstractTransactionHistory{


    public TransactionHistoryPayment(Long date, AccountModel sender,
                                     AccountModel receiver, Long amount,
                                     String note, String type) {

        super(date,sender,receiver,amount,note,type);
    }
}
