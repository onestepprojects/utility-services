package org.onestep.relief.utility.blockchain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class BalancesResponse {

    @JsonProperty
    private List<BalanceHistoryResponse> balanceHistory;

    public List<BalanceHistoryResponse> getBalanceHistory() {
        return balanceHistory;
    }

    public void setBalanceHistory(List<BalanceHistoryResponse> balanceHistory) {
        this.balanceHistory = balanceHistory;
    }

}

