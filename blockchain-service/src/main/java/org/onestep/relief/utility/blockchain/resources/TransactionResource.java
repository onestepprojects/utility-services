package org.onestep.relief.utility.blockchain.resources;

import org.onestep.relief.utility.blockchain.dto.CreateAssetRequest;
import org.onestep.relief.utility.blockchain.facade.TransactionFacade;
import org.onestep.relief.utility.blockchain.dto.AuthDocumentTransactionRequest;
import org.onestep.relief.utility.blockchain.model.AccountModel;
import org.onestep.relief.utility.blockchain.model.ErrorResponse;
import org.onestep.relief.utility.blockchain.model.MnemonicModel;
import org.onestep.relief.utility.blockchain.dto.SendTransactionRequest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("onestep/blockchain/transaction")
@Produces(MediaType.APPLICATION_JSON)
public class TransactionResource {

    /**
     * Facade class that interfaces with Algorand SDK.
     * Hides the implementation details from Api clients
     */
    private TransactionFacade facade;

    public TransactionResource(TransactionFacade facade) {
        this.facade = facade;
    }

    /**
     * Sends usdc and returns an object containing transactionId
     * Takes in a request object composed of the sender, receiver, note, amount
     * and transactionType
     * @param mnemonic Private pass phrase. Should eventually be deprecated.
     * @return TransactionDetails
     */
    @POST
    @Path("/optInUsdc")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response optInUsdc(MnemonicModel mnemonic){

        //TODO - This method should take a SendTransactionRequest but for now to not break the API
        // keep it as MNemonic. However map it to new object for consistency with downstream code
        SendTransactionRequest request = new SendTransactionRequest();
        AccountModel senderAccount = new AccountModel();
        senderAccount.setMnemonic(mnemonic.getMnemonic());
        request.setSender(senderAccount);

        try {
            return Response.status(200)
                    .entity(facade.optInUsdc(request)).build();
        } catch (Exception e) {
            return Response.status(500)
                    .entity(new ErrorResponse("Unable to opt in for usdc "+ e.getMessage())).build();
        }
    }
    @POST
    @Path("/optIn")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response optIn(SendTransactionRequest request){

        try {
            return Response.status(200)
                    .entity(facade.optIn(request)).build();
        } catch (Exception e) {
            return Response.status(500)
                    .entity(new ErrorResponse("Unable to opt in for asset "+ e.getMessage())).build();
        }
    }

    /**
     * Sends usdc and returns an object containing transactionId
     * Takes in a request object composed of the sender, receiver, note, amount
     * and transactionType
     * @param request SendTransactionRequest object with transaction information.
     * @return TransactionDetails
     */
    @POST
    @Path("/sendUsdc")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response sendUsdc(SendTransactionRequest request){

        try {
            return Response.status(200)
                    .entity(facade.sendUsdc(request)).build();
        } catch (Exception e) {
            return Response.status(500)
                    .entity(new ErrorResponse("Unable to send usdc "+ e.getMessage())).build();
        }
    }

    /**
     * Sends algo and returns an object containing transactionId
     * Takes in a request object composed of the sender, receiver, note, amount
     * and transactionType
     * @param request SendTransactionRequest object with transaction information.
     * @return TransactionDetails
     */
    @POST
    @Path("/sendAlgo")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response sendAlgo(SendTransactionRequest request) {

        try {
            return Response.status(200)
                    .entity(facade.sendAlgo(request)).build();
        } catch (Exception e) {
            return Response.status(500)
                    .entity(new ErrorResponse("Unable to send Algos: "+ e.getMessage())).build();
        }
    }

    /**
     * Creates a new Asset on the blockchain. Client is required to pass in all
     * details of the asset.
     * @param request - CreateAssetRequest object with all of the transaction requirements
     * @return TransactionDetails
     */
    @POST
    @Path("/createAsset")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createAsset(CreateAssetRequest request) {

        try {
            return Response.status(200)
                    .entity(facade.createAsset(request)).build();
        } catch (Exception e) {
            return Response.status(500)
                    .entity(new ErrorResponse("Unable to create asset: "+ e.getMessage())).build();
        }
    }

    /**
     * Transfers an on the blockchain. Client is required to pass in all
     * details of the asset.
     * @param request - SendTransactionRequest object with all of the transaction requirements
     * @return TransactionDetails
     */
    @POST
    @Path("/transferAsset")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response transferAsset(SendTransactionRequest request) {

        try {
            return Response.status(200)
                    .entity(facade.sendXferAsset(request)).build();
        } catch (Exception e) {
            return Response.status(500)
                    .entity(new ErrorResponse("Unable to transfer asset: "+ e.getMessage())).build();
        }
    }


    /**
     * Returns the transaction status by taking in the transactionId.
     * Useful to check the status of pending transactions after it is committed
     * Returns a TransactionConfirmation object containing the confirmedRound and the closingAmount
     * Returns pendingTransaction is transaction is not committed yet
     * @param transactionId Blockchain transaction ID
     * @return TransactionConfirmation
     */
    @GET
    @Path("/txnStatus/{txnId}")
    public Response getTransactionStatus(@PathParam("txnId") String transactionId) {

        try {
            return Response.status(200)
                    .entity(facade.getTransactionStatus(transactionId)).build();
        } catch (Exception e) {
            return Response.status(500)
                    .entity(new ErrorResponse("Unable to get txn status: "+ e.getMessage())).build();
        }
    }

    /**
     * Signs documents by creating a transaction
     * Returns an object containing transactionId
     * @param request AuthDocumentTransactionRequest with transaction information.
     * @return TransactionDetails
     */
    @POST
    @Path("/signAuth")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response signAuthenticate(AuthDocumentTransactionRequest request) {

        try {
            return Response.status(200)
                    .entity(facade.createAuthDoc(request)).build();
        } catch (Exception e) {
            return Response.status(500)
                    .entity(new ErrorResponse("Unable to sign documents: "+ e.getMessage())).build();
        }
    }

    @GET
    @Path("/getAsset/{assetId}")
    public Response getAsset(@PathParam("assetId") long assetId) {

        try {
            return Response.status(200)
                    .entity(facade.getAsset(assetId)).build();
        } catch (Exception e) {
            return Response.status(500)
                    .entity(new ErrorResponse("Unable to retrieve asset information: "+ e.getMessage())).build();
        }
    }

    /**
     * Returns the transaction indicated by txId
     * Used for transactions that aren't new, queries the Indexer API
     *
     * @param transactionId Blockchain transaction ID.
     * @return Transaction ID in JSON format.
     */
    @GET
    @Path("/transaction")
    public Response getTransactionFromIndexer(@QueryParam("txId") String transactionId) {

        try {
            return Response.status(200)
                    .entity(facade.getTransactionFromIndexer(transactionId)).build();
        } catch (Exception e) {
            return Response.status(500)
                    .entity(new ErrorResponse("Unable to get txn "+ e.getMessage())).build();
        }
    }


    @POST
    @Path("/{assetId}/destroyAsset")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response destroyAsset(@PathParam("assetId") Long assetId) {

        try {
            return Response.status(200)
                    .entity(facade.destroyAsset(assetId)).build();
        } catch (Exception e) {
            return Response.status(500)
                    .entity(new ErrorResponse("Unable to destroy asset: "+ e.getMessage())).build();
        }
    }

}
