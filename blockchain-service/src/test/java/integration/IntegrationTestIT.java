package integration;

import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.testing.DropwizardTestSupport;
import io.dropwizard.testing.ResourceHelpers;
import org.eclipse.jetty.http.HttpStatus;
import org.glassfish.jersey.client.ClientProperties;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.onestep.relief.utility.blockchain.BlockchainApplication;
import org.onestep.relief.utility.blockchain.BlockchainApplicationConfiguration;
import org.onestep.relief.utility.blockchain.facade.TransactionFacade;
import org.onestep.relief.utility.blockchain.model.AccountBalance;
import org.onestep.relief.utility.blockchain.model.AccountModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class IntegrationTestIT {
    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("test-config.yml");
    private static final String ACCOUNT1 = "7JBKQM3PXALN372QKGINDPGTKPLIBGUSUAMUKSVL2UFGTE5TOKMTHCUIBE";

    public static final DropwizardTestSupport<BlockchainApplicationConfiguration> SUPPORT =
            new DropwizardTestSupport<>(BlockchainApplication.class,
                    CONFIG_PATH);

    private static Logger logger = LoggerFactory.getLogger(IntegrationTestIT.class);

    static Client client = null;
    @BeforeAll
    static public void setUp() {
        try {
            SUPPORT.before();

            client = new JerseyClientBuilder(SUPPORT.getEnvironment()).build("test client");
            client.property(ClientProperties.CONNECT_TIMEOUT, 4000);
            client.property(ClientProperties.READ_TIMEOUT, 4000 );

        } catch (Exception e) {
            logger.info(e.getMessage());

        }
    }

    @AfterAll
    static public void tearDown() {
        SUPPORT.after();
    }

    @org.junit.jupiter.api.Test
    public void testCreateAccount() {

        Response response = client.target(
                String.format("http://localhost:%d/onestep/blockchain/account/createAccount", SUPPORT.getLocalPort()))
                .request().get();
        assertEquals(HttpStatus.CREATED_201, response.getStatus());

        AccountModel m = response.readEntity(AccountModel.class);
        assertNotEquals(m.getAddress(), "");
        assertNotEquals(m.getMnemonic(), "");
    }

    @Test
    public void testGetBalance() {

        Response r1 = client.target(
                String.format("http://localhost:%d/onestep/blockchain/account/createAccount", SUPPORT.getLocalPort()))
                .request().get();
        assertEquals(HttpStatus.CREATED_201, r1.getStatus());
        AccountModel m = r1.readEntity(AccountModel.class);

        Response r2 = client.target(
                String.format("http://localhost:%d/onestep/blockchain/account/getBalance/%s", SUPPORT.getLocalPort(), m.getAddress()))
                .request().get();
        AccountBalance b = r2.readEntity(AccountBalance.class);
        assertEquals(0L, b.getAlgoBalance());

    }

}
