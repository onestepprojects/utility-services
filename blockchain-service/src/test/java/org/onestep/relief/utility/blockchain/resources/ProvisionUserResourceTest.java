package org.onestep.relief.utility.blockchain.resources;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.onestep.relief.utility.blockchain.dto.SendTransactionRequest;
import org.onestep.relief.utility.blockchain.facade.AccountFacade;
import org.onestep.relief.utility.blockchain.facade.TransactionFacade;
import org.onestep.relief.utility.blockchain.service.impl.*;
import org.onestep.relief.utility.blockchain.util.HTTPRequests;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(DropwizardExtensionsSupport.class)
public class ProvisionUserResourceTest {

    final Client client = new JerseyClientBuilder().build();

    private final ProvisionAccountResource resource =
        new ProvisionAccountResource(new TransactionFacade(new TransactionService(new CommonService(),
                new PagoService(client)), new CommonService(),new PagoService(client)),
                new AccountFacade(new AccountService(new CommonService())));

    @BeforeEach
    public void beforeEach() {
    }


    @Test
    public void provisionUserTest()  {

        resource.provisionFundAccount();
    }

    @Test
    public void provisionNewUserTest() throws JsonProcessingException {

        Logger logger = LoggerFactory.getLogger(ProvisionAccountResource.class);


        final ObjectMapper mapper = new ObjectMapper();


        SendTransactionRequest request = mapper.readValue(fixture("provisionNewUser.json"), SendTransactionRequest.class);


        logger.info(resource.provisionUserAccount(request).toString());

    }

}
