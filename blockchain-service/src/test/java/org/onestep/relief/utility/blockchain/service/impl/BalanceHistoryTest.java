package org.onestep.relief.utility.blockchain.service.impl;

import org.glassfish.jersey.client.JerseyClientBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.onestep.relief.utility.blockchain.util.HTTPRequests;

import javax.ws.rs.client.Client;

public class BalanceHistoryTest {

    final Client client = new JerseyClientBuilder().build();

    AccountService service = new AccountService(new CommonService());

    @Test
    public void balanceHistoryTest() throws Exception {

        String address = "B2GCA7S5A5QFUFXD3BJCFPSSQNWTTSUOA57PC42PSD2ZOMH7UCUEOQLY3Q";
        int days = 30;

        Assertions.assertNotNull(service.getBalanceHistory(address,days));
        Assertions.assertEquals(service.getBalanceHistory(address,days).getBalanceHistory().size(),30);

    }
}
