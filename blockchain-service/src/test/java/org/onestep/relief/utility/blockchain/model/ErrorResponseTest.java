package org.onestep.relief.utility.blockchain.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class ErrorResponseTest {

    private static final ObjectMapper mapper = new ObjectMapper();

    ErrorResponse errorResponse;

    @BeforeEach
    public void beforeEach() {

        errorResponse = new ErrorResponse("Unable to create txn");
    }
    @Test
    public void serializeErrorResponse() throws JsonProcessingException {

        String response = mapper.writeValueAsString(mapper
                .readValue(fixture("error.json"), ErrorResponse.class));

        assertThat(mapper.writeValueAsString(errorResponse)).isEqualTo(response);
    }

    @Test
    public void deserializeErrorResponse() throws JsonProcessingException {

        assertThat(mapper.readValue(fixture("error.json"), ErrorResponse.class))
                .isEqualTo(errorResponse);
    }
}
