package org.onestep.relief.utility.blockchain.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.onestep.relief.utility.blockchain.util.BlockchainUtils;

import java.util.HashMap;
import java.util.Map;

public class GetSecretsTest {


    @Test
    public void ResponseNotNullTest()  {

        //2a) Get 'faucet' account credentials from AWS Secrets Manager
        Assertions.assertNotNull(BlockchainUtils.getSecret());

    }

    @Test
    public void AddressandMnemonicNotNullTest() throws JsonProcessingException {

        //2a) Get 'faucet' account credentials from AWS Secrets Manager

        String sourceAccount = BlockchainUtils.getSecret();


        //Map the address & mnemonic retrieved from AWS so that we can get the private key later
        Map<String, Object> response  = new ObjectMapper().readValue(sourceAccount, HashMap.class);

        Assertions.assertNotNull(response.get("mnemonic").toString());
        Assertions.assertNotNull(response.get("address").toString());
    }
}
