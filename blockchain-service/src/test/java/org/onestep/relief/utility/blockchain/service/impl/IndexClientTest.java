package org.onestep.relief.utility.blockchain.service.impl;

import static org.onestep.relief.utility.blockchain.util.BlockchainConstants.*;
import com.algorand.algosdk.v2.client.common.IndexerClient;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(DropwizardExtensionsSupport.class)
class IndexClientTest {

    private IndexerClient client;


    @BeforeEach
    public void beforeEach() {
        client = IndexClient.getInstance();
    }


    /**
     * Tests that singleton is created and is a singleton.
     */
    @Test
    public void testNotNull() {
        Assertions.assertNotNull(client);
    }

    @Test
    public void testSingleInstance() {

        IndexerClient client2 = IndexClient.getInstance();

        Assertions.assertSame(client, client2);
    }

    /**
     * Ensures that the client is setup for the correct URL and Port, per the constants.
     * Reason - Code was changing and the Indexer client was changed to use the Algod URL
     * which was causing code to fail.
     */
    @Test
    public void TestURLandPort() {

        Assertions.assertTrue(INDEXER_API_ADDR.equals(client.getHost()));
        Assertions.assertEquals(INDEXER_API_PORT, client.getPort());

        //Ensure that our constants are not set the same.
        Assertions.assertFalse(INDEXER_API_ADDR.equals(ALGOD_API_ADDRESS));

    }

}