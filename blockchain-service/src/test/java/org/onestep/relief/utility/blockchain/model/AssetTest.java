package org.onestep.relief.utility.blockchain.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AssetTest {

    private static final ObjectMapper mapper = new ObjectMapper();

    Asset asset;

    @BeforeEach
    public void beforeEach() {

        asset = new Asset(1000L, "John Doe", 7635L,"USDC");

    }
    @Test
    public void serializeAsset() throws JsonProcessingException {

        String response = mapper.writeValueAsString(mapper
                .readValue(fixture("asset.json"), Asset.class));

        assertThat(mapper.writeValueAsString(asset)).isEqualTo(response);
    }

    @Test
    public void deserializeAsset() throws JsonProcessingException {

        assertThat(mapper.readValue(fixture("asset.json"), Asset.class))
                .isEqualTo(asset);
    }
}
