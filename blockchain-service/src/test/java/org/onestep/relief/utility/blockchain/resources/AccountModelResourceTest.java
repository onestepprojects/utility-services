package org.onestep.relief.utility.blockchain.resources;

import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import io.dropwizard.testing.junit5.ResourceExtension;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.onestep.relief.utility.blockchain.facade.AccountFacade;
import org.onestep.relief.utility.blockchain.model.AccountBalance;
import org.onestep.relief.utility.blockchain.model.AccountHistory;
import org.onestep.relief.utility.blockchain.model.AccountModel;
import org.onestep.relief.utility.blockchain.model.TransactionHistoryPayment;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(DropwizardExtensionsSupport.class)
public class AccountModelResourceTest {


    private static final AccountFacade facade = mock(AccountFacade.class);

    private static final ResourceExtension extension = ResourceExtension.builder()
            .addResource(new AccountResource(facade))
            .build();

    @Test
    public void createAccountTest() {

        AccountModel account = new AccountModel("ZKSHLK");

        when(facade.createAccount()).thenReturn(account);

        AccountModel response = extension
                .target("onestep/blockchain/account/createAccount").request().get(AccountModel.class);

        assertThat(response.getAddress()).isEqualTo("ZKSHLK");
        verify(facade).createAccount();
    }

    @Test
    public void getAccountBalanceTest() throws Exception {

        AccountBalance balance = new AccountBalance(1000L);

        when(facade.getAccountBalance("ZKSHLK")).thenReturn(balance);

        AccountBalance response = extension
                .target("onestep/blockchain/account/getBalance/ZKSHLK").request().get(AccountBalance.class);

        assertThat(response.getAlgoBalance()).isEqualTo(1000L);
        verify(facade).getAccountBalance("ZKSHLK");
    }

    @Disabled
    public void getAccountHistoryTest() throws IOException {

        AccountHistory history = new AccountHistory();

        history.getTransactions().add(new TransactionHistoryPayment(100000000L,
                new AccountModel("ADDR5476"), new AccountModel("ADDR9865"),
                1000L, "Note", "Type"));

        when(facade.getAccountHistory("ADDR5476")).thenReturn(history);

        AccountHistory response = extension
                .target("onestep/blockchain/account/accountHistory/ADDR5476")
                .request()
                .get(AccountHistory.class);

        assertThat(response.getTransactions()).isNotNull();
        assertThat(response.getTransactions().get(0).getSender()).isNotNull();
        assertThat(response.getTransactions().get(0).getSender().getAddress()).isEqualTo("ADDR5476");
    }
}
