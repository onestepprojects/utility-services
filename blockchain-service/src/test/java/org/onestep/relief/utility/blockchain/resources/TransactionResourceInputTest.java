package org.onestep.relief.utility.blockchain.resources;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.onestep.relief.utility.blockchain.dto.AuthDocumentTransactionRequest;
import org.onestep.relief.utility.blockchain.dto.SendTransactionRequest;
import org.onestep.relief.utility.blockchain.facade.TransactionFacade;
import org.onestep.relief.utility.blockchain.model.*;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Asserts that the input we think we are calling the method with is what we expect.
 * Ensures that if we change the model or input type it is caught in this regression test.
 */
@ExtendWith(MockitoExtension.class)
//@RunWith(JUnitPlatform.class)
public class TransactionResourceInputTest {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Mock
    private TransactionFacade transactionFacadeMock;
    @InjectMocks
    private TransactionResource underTest;

    @BeforeEach
    public void setUp()  {

        MockitoAnnotations.initMocks(this);

    }

    @org.junit.jupiter.api.Test
    public void getAssetParametersTest() throws Exception {
        long assetId = 12345;

        /*
         * This is the method call whose arguments we are verifying
         */
        underTest.getAsset(assetId);

        ArgumentCaptor<Long> captur = ArgumentCaptor.forClass(Long.class);
        Mockito.verify(transactionFacadeMock).getAsset(captur.capture());
        assertThat(captur.getValue().intValue()).isEqualTo(assetId);
    }


    @org.junit.jupiter.api.Test
    public void authDocumentParametersTest() throws Exception {



        AuthDocumentTransactionRequest request = mapper.readValue(fixture("signDoc.json"), AuthDocumentTransactionRequest.class);

        String expectedSender = request.getSender().getAddress();
        AuthDocumentTransactionRequest.Note expectedNote = request.new Note(request.getNote().getNote(),
                request.getNote().getDocId(), request.getNote().getHash());


        /*
         * This is the method call whose arguments we are verifying
         */
        underTest.signAuthenticate(request);

        // results
        ArgumentCaptor<AuthDocumentTransactionRequest> captur = ArgumentCaptor.forClass(AuthDocumentTransactionRequest.class);
        Mockito.verify(transactionFacadeMock).createAuthDoc(captur.capture());

        assertThat(captur.getValue().getSender().getAddress()).isEqualTo(expectedSender);
        assertThat(captur.getValue().getNote()).isEqualTo(expectedNote);
    }

    @org.junit.jupiter.api.Test
    public void getTransactionStatusParametersTest() throws Exception {

        String txnId = "WGQTPBMOPFOYMTFYJV34OWVUSWFCHE4I3XHPNOSMQT4E2OFC4CDQ";

        /*
         * This is the method call whose arguments we are verifying
         */
        underTest.getTransactionStatus(txnId);

        ArgumentCaptor<String> captur = ArgumentCaptor.forClass(String.class);
        Mockito.verify(transactionFacadeMock).getTransactionStatus(captur.capture());
        assertThat(captur.getValue()).isEqualTo("WGQTPBMOPFOYMTFYJV34OWVUSWFCHE4I3XHPNOSMQT4E2OFC4CDQ");
    }

    @org.junit.jupiter.api.Test
    public void sendAlgoParametersTest() throws  Exception {

        SendTransactionRequest request = mapper.readValue(fixture("sendAlgoUsdc.json"), SendTransactionRequest.class);
        String expectedSender = request.getSender().getAddress();
        String expectedReceiver = request.getReceiver().getAddress();
        String expectedNote = request.getNote();
        Long expectedAmount = request.getAmount();


        /*
         * This is the method call whose arguments we are verifying
         */
        underTest.sendAlgo(request);

        ArgumentCaptor<SendTransactionRequest> captur = ArgumentCaptor.forClass(SendTransactionRequest.class);
        Mockito.verify(transactionFacadeMock).sendAlgo(captur.capture());

        assertThat(captur.getValue().getSender().getAddress()).isEqualTo(expectedSender);
        assertThat(captur.getValue().getReceiver().getAddress()).isEqualTo(expectedReceiver);
        assertThat(captur.getValue().getNote()).isEqualTo(expectedNote);
        assertThat(captur.getValue().getAmount()).isEqualTo(expectedAmount);
    }


    @org.junit.jupiter.api.Test
    public void sendUsdcParametersTest() throws Exception {

        SendTransactionRequest request = mapper.readValue(fixture("sendAlgoUsdc.json"), SendTransactionRequest.class);
        String expectedSender = request.getSender().getAddress();
        String expectedReceiver = request.getReceiver().getAddress();
        String expectedNote = request.getNote();
        Long expectedAmount = request.getAmount();


        /*
         * This is the method call whose arguments we are verifying
         */
        underTest.sendUsdc(request);

        ArgumentCaptor<SendTransactionRequest> captur = ArgumentCaptor.forClass(SendTransactionRequest.class);
        Mockito.verify(transactionFacadeMock).sendUsdc(captur.capture());

        assertThat(captur.getValue().getSender().getAddress()).isEqualTo(expectedSender);
        assertThat(captur.getValue().getReceiver().getAddress()).isEqualTo(expectedReceiver);
        assertThat(captur.getValue().getNote()).isEqualTo(expectedNote);
        assertThat(captur.getValue().getAmount()).isEqualTo(expectedAmount);

    }

    @org.junit.jupiter.api.Test
    public void optInUsdcParametersTest() throws Exception {

        MnemonicModel request = mapper.readValue(fixture("mnemonic.json"), MnemonicModel.class);
        String expectedMnemonic = request.getMnemonic();

        /*
         * This is the method call whose arguments we are verifying
         */
        underTest.optInUsdc(request);

        ArgumentCaptor<SendTransactionRequest> captur = ArgumentCaptor.forClass(SendTransactionRequest.class);
        Mockito.verify(transactionFacadeMock).optInUsdc(captur.capture());
        assertThat(captur.getValue().getSender().getMnemonic()).isEqualTo(expectedMnemonic);

    }


}
