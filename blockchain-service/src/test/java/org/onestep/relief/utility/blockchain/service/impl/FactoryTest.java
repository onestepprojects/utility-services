package org.onestep.relief.utility.blockchain.service.impl;

import com.algorand.algosdk.transaction.Transaction;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dozer.DozerBeanMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.onestep.relief.utility.blockchain.dto.CreateAssetRequest;
import org.onestep.relief.utility.blockchain.dto.SendTransactionRequest;
import org.onestep.relief.utility.blockchain.model.AccountModel;
import org.onestep.relief.utility.blockchain.model.NewTransaction;
import org.onestep.relief.utility.blockchain.util.TransactionFactory;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.onestep.relief.utility.blockchain.util.BlockchainConstants.GENESIS_HASH;

public class FactoryTest {


    private static final ObjectMapper mapper = new ObjectMapper();
    private DozerBeanMapper dozerMapper;



    @BeforeEach
    public void beforeEach() {

        this.dozerMapper = new DozerBeanMapper();

    }

    @org.junit.jupiter.api.Test
    public void sendAlgoTest() throws JsonProcessingException {

        // First map 'input' to its DTO
        SendTransactionRequest request = mapper.readValue
                (fixture("sendAlgoTransaction.json"), SendTransactionRequest.class);

        // Then map the DTO the the model object
        NewTransaction newTransaction = dozerMapper.map(request, NewTransaction.class);

        // We set transaction type so that client doesn't mess it up
        newTransaction.setTransactionType("pay");

        Transaction transaction = TransactionFactory.createTransaction(newTransaction);

        Assertions.assertTrue(transaction.type.toString().equals("Payment"));
        Assertions.assertEquals(transaction.sender.toString(),request.getSender().getAddress());
        Assertions.assertEquals(transaction.receiver.toString(),request.getReceiver().getAddress());
        Assertions.assertEquals(transaction.amount.longValue(),request.getAmount());
        //Nominal test to ensure that we are retrieving suggested parameters
        Assertions.assertNotNull(transaction.genesisHash);

    }


    @org.junit.jupiter.api.Test
    public void xferAssetTransactionTest() throws JsonProcessingException {

        // First map 'input' to its DTO
        SendTransactionRequest request = mapper.readValue
                (fixture("xferAssetTransaction.json"), SendTransactionRequest.class);

        // Then map the DTO the the model object
        NewTransaction newTransaction = dozerMapper.map(request, NewTransaction.class);

        // We set transaction type so that client doesn't mess it up
        newTransaction.setTransactionType("axfer");

        Transaction transaction = TransactionFactory.createTransaction(newTransaction);
        Assertions.assertTrue(transaction.type.toString().equals("AssetTransfer"));
        Assertions.assertEquals(transaction.sender.toString(),request.getSender().getAddress());
        Assertions.assertEquals(transaction.assetReceiver.toString(),request.getReceiver().getAddress());
        Assertions.assertEquals(transaction.assetAmount.longValue(),request.getAmount());
        Assertions.assertEquals(transaction.xferAsset.intValue(),request.getAssetId());
        //Nominal test to ensure that we are retrieving suggested parameters
        Assertions.assertNotNull(transaction.genesisHash);
    }

    @org.junit.jupiter.api.Test
    public void createAssetTransactionTest() throws JsonProcessingException {

        // First map 'input' to its DTO
        CreateAssetRequest request = mapper.readValue
                (fixture("createAssetTransaction.json"), CreateAssetRequest.class);

        // Then map the DTO the the model object
        NewTransaction newTransaction = dozerMapper.map(request, NewTransaction.class);
        newTransaction.setSender(new AccountModel(request.getCreator_addr()));
        newTransaction.setTransactionType("acfg");

        // We set transaction type so that client doesn't mess it up
        newTransaction.setTransactionType("acfg");

        Transaction transaction = TransactionFactory.createTransaction(newTransaction);

        Assertions.assertTrue(transaction.type.toString().equals("AssetConfig"));

        Assertions.assertEquals(transaction.sender.toString(), request.getCreator_addr());
        Assertions.assertEquals(transaction.assetParams.assetName, request.getAsset_name());
        Assertions.assertEquals(transaction.assetParams.assetTotal.intValue(), request.getTotal_amount());
        Assertions.assertEquals(transaction.assetParams.assetDecimals, request.getDecimals());
        Assertions.assertEquals(transaction.assetParams.assetManager.toString(), request.getManager_addr());
        Assertions.assertEquals(transaction.assetParams.assetReserve.toString(), request.getReserve_addr());
        Assertions.assertEquals(transaction.assetParams.assetFreeze.toString(), request.getFreeze_addr());
        Assertions.assertEquals(transaction.assetParams.assetClawback.toString(), request.getClawback_addr());






        //Nominal test to ensure that we are retrieving suggested parameters
        Assertions.assertNotNull(transaction.genesisHash);
    }
}
