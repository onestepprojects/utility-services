package org.onestep.relief.utility.blockchain.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


public class AccountModelBalanceTest {

    private static final ObjectMapper mapper = new ObjectMapper();

    AccountBalance balance;

    @BeforeEach
    public void beforeEach() {

        balance = new AccountBalance(1000);
        Asset asset = new Asset();
        balance.setAssets(Arrays.asList(asset));

        asset.setAmount(1000L);
        asset.setAssetId(11);
        asset.setCreator("John");

    }
    @Test
    public void serializeAccountBalance() throws JsonProcessingException {

        String response = mapper.writeValueAsString(mapper
                .readValue(fixture("accountBalance.json"), AccountBalance.class));

        assertThat(mapper.writeValueAsString(balance)).isEqualTo(response);
    }

    @Test
    public void deserializeAccountBalance() throws JsonProcessingException {

        assertThat(mapper.readValue(fixture("accountBalance.json"), AccountBalance.class))
                .isEqualTo(balance);
    }
}
