package org.onestep.relief.utility.blockchain.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AccountModelTest {

    private static final ObjectMapper mapper = new ObjectMapper();

    AccountModel account;

    @BeforeEach
    public void beforeEach() {

        account = new AccountModel("ADDR5467", "toy joy wide green test");

    }
    @Test
    public void serializeAccount() throws JsonProcessingException {

        String response = mapper.writeValueAsString(mapper
                .readValue(fixture("account.json"), AccountModel.class));

        assertThat(mapper.writeValueAsString(account)).isEqualTo(response);
    }

    @Test
    public void deserializeAccount() throws JsonProcessingException {

        assertThat(mapper.readValue(fixture("account.json"), AccountModel.class))
                .isEqualTo(account);
    }
}
