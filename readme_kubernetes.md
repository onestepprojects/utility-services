# Pushing the docker image to AWS Kubernetes

With a docker image created and ready to be deployed, we can push it to a kubernetes cluster.

We set up the kubernetes cluster with kOps for AWS https://kops.sigs.k8s.io/ and followed their set up guide.  Once the cluster was set up, we can deploy the images.  Our cluster consists of 2 t3a.small ec2 instances, one as master and other one as node.  We found kOps to be the most cost efficient way to set up kubernetes on AWS.  The 2 t3a.small cluster will cost about $60 per month if always left on.

## 1) Deploying the Image

We used these config files for blockchain-service, which should be similar for simlar dropwizard apps. There are 3 specific sections in the config `deployment service ingress` each separated by `---`

```
#blockchain
apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: onestep-test
  name: deployment-blockchain-service ## should be unique
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: app-blockchain-service ## match with service
  replicas: 1 ## use minimum 2 to allow for rolling (uninterrupted) updates
  template:
    metadata:
      labels:
        app.kubernetes.io/name: app-blockchain-service ## match with service
    spec:
      imagePullSecrets:
        - name: gitlab-registry
      containers:
        - name: c-blockchain-service ## should be unique
          image: <GITLAB_IMAGE> ## replace with image
          ports:
            - containerPort: 8080 ## match localhost port
              name: blockchain
            - containerPort: 8081 ## match localhost port
              name: healthcheck
---
apiVersion: v1
kind: Service
metadata:
  namespace: onestep-test
  name: service-blockchain-service ## should be unique
spec:
  selector:
    app.kubernetes.io/name: app-blockchain-service ## match with service
  type: NodePort
  ports:
    - port: 8080 ## external (match with ingress)
      targetPort: 8080 ## internal (match with deployment)
      protocol: TCP
      name: blockchain-blockchain
    - port: 8081
      targetPort: 8081
      protocol: TCP
      name: healthcheck-blockchain
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  namespace: onestep-test
  name: ingress-nginx-blockchain
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/rewrite-target: /onestep/blockchain/$2
    # https://kubernetes.github.io/ingress-nginx/examples/rewrite/
spec:
  rules:
    - host: "test.onestepprojects.tk"
      http:
        paths:
          - path: /onestep/blockchain(/|$)(.*)
            pathType: Prefix
            backend:
                service:
                  name: service-blockchain-service
                  port:
                    number: 8080

```

We modified the paths to our api's using nginx's rewrite url functions, as explained here https://kubernetes.github.io/ingress-nginx/examples/rewrite/

`kubectl` was used to execute the config file and deploy the image to the cluster, with the filename being project specific

```
kubectl apply -f kube-blockchain-service.yml
```

We checked the status with these commands

```
# all resource status
kubectl get pod,deployment,service,ingress --namespace onestep
```

This command should also provide the http address of the server. The http address should also be on the aws console, ends with `...aws.com.` The site should now be accessible from the internet.

To use custom dns addresses, `freenom.com` provides free domain names for 1 year, and they can be linked to the aws http addresses, by editing the nameservers to use the AWS Route 53 hosted zone feature. AWS can also provide certificates for https, which can be connected to the load balancer.

## Updates and changes

To make changes, the **kubectl apply -f** command can be used to reload with the updated config file. If no changes were made, we can reload using this command.

```
kubectl rollout restart deployment/blockchain-service-deployment --namespace onestep
```

## 2) Automating the process with gitlab-ci

Like with creating images, gitlab-ci can also automate deploying images. Below is an example of a gitlab-ci.yml for automated deployments.

```
#
# Deploy container to kubernetes cluster
# https://sanderknape.com/2019/02/automated-deployments-kubernetes-gitlab/
#
deploy:blockchain-service-docker:
  rules:
    - if: $CI_PIPELINE_IID==$CI_PIPELINE_IID
      when: manual  ## go to CI CD pipeline on gitlab site to run, only when needed
  variables:
    GITLAB_REGISTRY: registry.gitlab.com
    GITLAB_IMAGE: registry.gitlab.com/cscie599sec1spring21/team-2-project-area/blockchain-service:blockchain-service-$CI_COMMIT_BRANCH-$CI_PIPELINE_IID
    KUBE_YML: ./blockchain-service/kube-blockchain-service.yml  ### the location of kube config file
  image:
    name: ubuntu:latest
  stage: deploy
  needs: ["test:blockchain-service-docker"]
  before-script:
    # install kubectl
    - apt-get update && apt-get install -y apt-transport-https ca-certificates curl wget
    - curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
    - echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | tee /etc/apt/sources.list.d/kubernetes.list
    - apt-get update && apt-get install -y kubectl
    - kubectl version --client
  script:
    # connect to the cluster
    - kubectl config set-cluster ${KUBE_CLUSTER} --server="${KUBE_SERVER}"
    - kubectl config set-credentials ${KUBE_USER} --token="${KUBE_USER_TOKEN}"
    - kubectl config set clusters.${KUBE_CLUSTER}.certificate-authority-data ${KUBE_CERTIFICATE_AUTHORITY_DATA}
    - kubectl config set-context ${KUBE_CLUSTER} --cluster=${KUBE_CLUSTER} --user=${KUBE_USER} && kubectl config use-context ${KUBE_CLUSTER}
    - kubectl config set-context --current --namespace=$(echo $(grep "namespace" ${KUBE_YML}) | awk '{print $2}')
    # enable kubectl login to gitlab
    - kubectl delete secret gitlab-registry --ignore-not-found
    - kubectl create secret docker-registry gitlab-registry --docker-server=$GITLAB_REGISTRY --docker-username=gitlab-ci-token --docker-password=$CI_BUILD_TOKEN
    # apply the deployment
    - kubectl apply -f ${KUBE_YML}
#
#

```

Most of the script can be reused for similar projects. Special variables like `${KUBE_CLUSTER} ${KUBE_SERVER} ${KUBE_USER_TOKEN}` are specific to each cluster and can be found with `kubectl`, and need to be specified as variables under gitlab settings -> CI/CD
