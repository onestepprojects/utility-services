﻿/*
 * OneStep Relief Platform
 */

namespace OneStepCli.Service.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public static class GuardHelper
    {
        public static void EnsureNotNull<T>(T value, string argName) where T : class
        {
            if (value == null)
            {
                throw new ArgumentNullException(
                    paramName: argName,
                    message: $"Argument '{argName}' cannot be null.");
            }
        }

        public static void EnsureNotNullOrEmpty(string value, string argName)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException(
                    paramName: argName,
                    message: $"Argument '{argName}' cannot be null, empty or whitespace.");
            }
        }

        public static void EnsureGreaterThan(int value, int greaterThan, string argName)
        {
            if (value < greaterThan)
            {
                throw new ArgumentException(
                    paramName: argName,
                    message: $"Argument '{argName}' with value '{value}' is less than expected value '{greaterThan}'.");
            }
        }
    }
}
