﻿/*
 * OneStep Relief Platform
 */

namespace OneStepCli.Service.Helper
{
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Threading.Tasks;

    public class ExecuteWithLogger
    {
        ILogger Logger { get; }

        public ExecuteWithLogger(ILogger logger)
        {
            Logger = logger;
        }

        public async Task ExponentialBackoffAsync(Func<Task> func, int maxNumberOfRetries = 5)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            Logger.LogInformation($"Start time: '{DateTime.Now}'");

            try
            {
                // Fail fast
                GuardHelper.EnsureGreaterThan(value: maxNumberOfRetries, greaterThan: 0, nameof(maxNumberOfRetries));
                GuardHelper.EnsureNotNull(value: func, argName: nameof(func));

                var exceptions = new List<Exception>();

                while (maxNumberOfRetries > 0)
                {
                    try
                    {
                        await func();
                        return;
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex, message: $"An error has occurred while processing {nameof(ExponentialBackoffAsync)}");
                        maxNumberOfRetries--;

                        // Add to exceptions list
                        exceptions.Add(ex);

                        // If we reached the max number of retries, add this exception to the top of the list.
                        // Note: this action causes an array to probably be resized and moves all elements to
                        // its next position, this is O(n) time complexity (copy all elements).
                        if (maxNumberOfRetries == 0)
                        {
                            exceptions.Insert(0, new Exception($"Reached all max number of retries: '{maxNumberOfRetries}'"));
                        }
                        else
                        {
                            // Haven't reached max retries yet, add deterministic wait time
                            await Task.Delay(TimeSpan.FromSeconds(5));
                        }
                    }
                }

                // If we reach here, we exhausted all retries
                throw new AggregateException(exceptions);
            }
            catch
            {
                throw;
            }
            finally
            {
                stopWatch.Stop();
                Logger.LogInformation($"Elapsed time: '{stopWatch.Elapsed.TotalSeconds}' seconds");
            }
        }

        public async Task<TResult> ExponentialBackoffAsync<TResult>(Func<Task<TResult>> func, int maxNumberOfRetries = 5)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            Logger.LogInformation($"Start time: '{DateTime.Now}'");

            try
            {
                // Fail fast
                GuardHelper.EnsureGreaterThan(value: maxNumberOfRetries, greaterThan: 0, nameof(maxNumberOfRetries));
                GuardHelper.EnsureNotNull(value: func, argName: nameof(func));

                var exceptions = new List<Exception>();

                while (maxNumberOfRetries > 0)
                {
                    try
                    {
                        return await func();
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex, message: $"An error has occurred while processing {nameof(ExponentialBackoffAsync)}");
                        maxNumberOfRetries--;

                        // Add to exceptions list
                        exceptions.Add(ex);

                        // If we reached the max number of retries, add this exception to the top of the list.
                        // Note: this action causes an array to probably be resized and moves all elements to
                        // its next position, this is O(n) time complexity (copy all elements).
                        if (maxNumberOfRetries == 0)
                        {
                            exceptions.Insert(0, new Exception($"Reached all max number of retries: '{maxNumberOfRetries}'"));
                        }
                        else
                        {
                            // Haven't reached max retries yet, add deterministic wait time
                            await Task.Delay(TimeSpan.FromSeconds(5));
                        }
                    }
                }

                // If we reach here, we exhausted all retries
                throw new AggregateException(exceptions);
            }
            catch
            {
                throw;
            }
            finally
            {
                stopWatch.Stop();
                Logger.LogInformation($"Elapsed time: '{stopWatch.Elapsed.TotalSeconds}' seconds");
            }
        }
    }
}
