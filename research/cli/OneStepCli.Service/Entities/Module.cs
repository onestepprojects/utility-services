﻿/*
 * OneStep Relief Platform
 */

namespace OneStepCli.Service.Entities
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class Module
    {
        /// <summary>
        /// The module name.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; }

        /// <summary>
        /// The list of available commands.
        /// </summary>
        [JsonProperty("commands")]
        public IList<Command> Commands { get; }

        public Module(string name, IList<Command> commands)
        {
            this.Name = name;
            this.Commands = commands;
        }
    }
}
