﻿/*
 * OneStep Relief Platform
 */

namespace OneStepCli.Service.Entities
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public class Command
    {
        /// <summary>
        /// The command name, must be registered in manifest otherwise command is ignored.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; }

        /// <summary>
        /// The request http method
        /// </summary>
        [JsonProperty("httpMethod")]
        public string HttpMethod { get; }

        /// <summary>
        /// The Body of the action to execute.
        /// </summary>
        [JsonProperty("body")]
        public JToken Body { get; }

        /// <summary>
        /// The request uri.
        /// </summary>
        [JsonProperty("uri")]
        public string Uri { get; }

        /// <summary>
        /// The request type.
        /// </summary>
        [JsonProperty("requestType")]
        public RequestType RequestType { get; }

        public Command(string name, string uri, string httpMethod, JToken body, RequestType requestType)
        {
            this.Name = name;
            this.Uri = uri;
            this.Body = body;
            this.HttpMethod = httpMethod;
            this.RequestType = requestType;
        }
    }
}
