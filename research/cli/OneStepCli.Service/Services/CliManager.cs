﻿/*
 * OneStep Relief Platform
 */

namespace OneStepCli.Service.Services
{
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json;
    using OneStepCli.Service.Entities;
    using OneStepCli.Service.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;
    using OneStepCli.Service.Exceptions;
    using OneStepCli.Service.Helper;
    using System.Security;
    using System.Net.Http;
    using Amazon.CognitoIdentityProvider.Model;
    using Amazon.CognitoIdentityProvider;
    using System.Linq;
    using System.Collections.Immutable;

    public class CliManager : ICliManager
    {
        /// <summary>
        /// The private cli manager instance.
        /// </summary>
        static ICliManager instance;

        /// <summary>
        /// The public cli manager instance, singleton.
        /// </summary>
        public static ICliManager Instance
        {
            get
            {
                if (CliManager.instance == null)
                {
                    throw new ArgumentNullException($"{nameof(CliManager)} hasn't been initialized. Make sure to call {nameof(CliManager)}.{nameof(InitializeAsync)} method.");
                }
                return CliManager.instance;
            }
            set
            {
                CliManager.instance = value;
            }
        }

        /// <summary>
        /// The user's access token.
        /// </summary>
        string AccessToken { get; set; }

        /// <summary>
        /// The manifest.
        /// </summary>
        static ImmutableDictionary<string, ImmutableDictionary<string, Command>> Manifest;

        /// <summary>
        /// The manifest path.
        /// </summary>
        static string ManifestPath { get; set; }

        /// <summary>
        /// The AWS cognito client.
        /// </summary>
        readonly AmazonCognitoIdentityProviderClient client =
            new AmazonCognitoIdentityProviderClient(
                awsAccessKeyId: Environment.GetEnvironmentVariable("onestep_cli_aws_accesskeyid"),
                awsSecretAccessKey: Environment.GetEnvironmentVariable("onestep_cli_aws_accesskeysecret"),
                region: Amazon.RegionEndpoint.USEast1);

        /// <summary>
        /// The private constructor. Ensures this class is accessed as a singleton only.
        /// </summary>
        private CliManager() { }

        /// <summary>
        /// The initialize method. This method initializes manifest and instance. Initializing a manifest is a time
        /// consuming process, therefore ensuring this class is instantiated once and manifest is created only once
        /// improves performance.
        /// </summary>
        /// <param name="manifestPath">The manifest path</param>
        /// <returns></returns>
        public static async Task InitializeAsync(string manifestPath)
        {
            CliManager.ManifestPath = manifestPath;
            CliManager.Instance = new CliManager();
            CliManager.Manifest = await Instance.ReadManifestAsync(ManifestPath);
        }

        public async Task<string> AuthenticateAsync(string username, string password)
        {
            var authReq = new AdminInitiateAuthRequest
            {
                UserPoolId = Environment.GetEnvironmentVariable("onestep_cli_userpoolid"),
                ClientId = Environment.GetEnvironmentVariable("onestep_cli_clientid"),
                AuthFlow = AuthFlowType.ADMIN_USER_PASSWORD_AUTH
            };
            authReq.AuthParameters.Add("USERNAME", username);
            authReq.AuthParameters.Add("PASSWORD", password);

            AdminInitiateAuthResponse authResp = await client.AdminInitiateAuthAsync(authReq);

            return authResp.AuthenticationResult.AccessToken;
        }

        public async Task ExecuteCommandAsync(string module, Command command)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(AccessToken))
                {
                    // Perfom login only once
                    AccessToken =
                        await AuthenticateAsync(
                            username: Environment.GetEnvironmentVariable("onestep_cli_username"),
                            password: Environment.GetEnvironmentVariable("onestep_cli_password"));
                }

                GuardHelper.EnsureNotNull(CliManager.Manifest, nameof(CliManager.Manifest));

                // Ensure module is registered in the manifest
                if (!CliManager.Manifest.TryGetValue(module, out var commands))
                {
                    throw new ArgumentException($"Module: '{module}' is not registered in the manifest.");
                }

                // Ensure command is registered in the module
                if (!commands.TryGetValue(command.Name, out var commandFromManifest))
                {
                    throw new ArgumentException($"Command: '{command.Name}' is not registered in the manifest for module '{module}'.");
                }

                // Set body value
                command = new Command(
                    name: commandFromManifest.Name,
                    uri: commandFromManifest.Uri,
                    httpMethod: commandFromManifest.HttpMethod,
                    body: command.Body,
                    requestType: commandFromManifest.RequestType);

                var response = await CallEndpoint(command.Uri, command.HttpMethod, command.Body, command.RequestType);

                if (!response.IsSuccessStatusCode)
                {
                    throw new ApplicationException(
                        $"There was an exception calling the endpoint: '{command.Uri}', response code: '{response.ReasonPhrase}', response message: '{await response.Content.ReadAsStringAsync()}'");
                }
            }
            catch
            {
                // Any other possible exception, just throw
                throw;
            }
        }

        public async Task<Module> ParseCommandFileAsync(string commandFileFullPath)
        {
            try
            {
                GuardHelper.EnsureNotNull(commandFileFullPath, nameof(commandFileFullPath));

                var commandContents = await ReadFileContent(commandFileFullPath);
                GuardHelper.EnsureNotNullOrEmpty(commandContents, nameof(commandContents));
                var module = JsonConvert.DeserializeObject<Module>(commandContents);

                // Ensure module is registered in the manifest
                if (!CliManager.Manifest.ContainsKey(module.Name))
                {
                    throw new ArgumentException($"Module: '{module.Name}' is not registered in the manifest.");
                }

                return module;
            }
            catch (JsonException jEx)
            {
                throw new UserMadeException("Error parsing the manifest file.", jEx, ErrorCode.INVALID_JSON);
            }
        }

        public async Task<ImmutableDictionary<string, ImmutableDictionary<string, Command>>> ReadManifestAsync(string manifestFullPath)
        {
            try
            {
                if (CliManager.Manifest != null)
                {
                    // Return fast
                    return CliManager.Manifest;
                }

                GuardHelper.EnsureNotNull(manifestFullPath, nameof(manifestFullPath));

                var manifestContents = await ReadFileContent(manifestFullPath);
                GuardHelper.EnsureNotNullOrEmpty(manifestContents, nameof(manifestContents));

                // Parse manifest
                var manifest = JsonConvert.DeserializeObject<Manifest>(manifestContents);

                // Convert to immutable dictionary, this is to improve searching a module
                // to a constant time O(1), at the same time searching for commands takes
                // constant time O(1) because commands is a dictionary.
                // We use immutable dictionary to ensure the object does not get modified.
                return manifest.Modules.ToImmutableDictionary(
                    keySelector: module => module.Name,
                    elementSelector: module => module.Commands.ToImmutableDictionary(
                        keySelector: command => command.Name,
                        elementSelector: command => command));
            }
            catch (JsonException jEx)
            {
                throw new UserMadeException("Error parsing the manifest file.", jEx, ErrorCode.INVALID_JSON);
            }
        }

        private async Task<string> ReadFileContent(string filePath)
        {
            try
            {
                GuardHelper.EnsureNotNull(filePath, nameof(filePath));

                return await File.ReadAllTextAsync(filePath);
            }
            catch (Exception ex) when
                (ex is PathTooLongException ||
                 ex is DirectoryNotFoundException ||
                 ex is IOException ||
                 ex is UnauthorizedAccessException ||
                 ex is FileNotFoundException ||
                 ex is NotSupportedException ||
                 ex is SecurityException)
            {
                throw new UserMadeException("There was an error reading the file.", ex, ErrorCode.FILE_ACCESS_ERROR);
            }
        }

        private async Task<HttpResponseMessage> CallEndpoint(string uri, string httpMethod, JToken body, RequestType requestType)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Add("Authorization", $"bearer {AccessToken}");

            switch (httpMethod.ToLower())
            {
                case "get":
                    uri = requestType == RequestType.QUERY && body != null ? string.Concat(uri, body.ToString()) : uri;
                    return await client.GetAsync(uri);
                case "post":
                    var postData = body != null ? new StringContent(body.ToString(), Encoding.UTF8, "application/json") : null;
                    return await client.PostAsync(uri, postData);
                case "put":
                    var putData = body != null ? new StringContent(body.ToString(), Encoding.UTF8, "application/json") : null;
                    return await client.PutAsync(uri, putData);
                case "delete":
                    return await client.DeleteAsync(uri);
                default:
                    throw new ArgumentException($"'{httpMethod}' is not a valid http method");
            }
        }
    }
}
