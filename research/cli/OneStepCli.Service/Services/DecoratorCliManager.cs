﻿/*
 * OneStep Relief Platform
 */

namespace OneStepCli.Service.Services
{
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json.Linq;
    using OneStepCli.Service.Entities;
    using OneStepCli.Service.Helper;
    using OneStepCli.Service.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading.Tasks;

    public class DecoratorCliManager : ICliManager
    {
        ICliManager CliManager { get; }

        ILogger Logger { get; }

        public DecoratorCliManager(ICliManager cliManager, ILogger logger)
        {
            GuardHelper.EnsureNotNull(cliManager, nameof(cliManager));
            GuardHelper.EnsureNotNull(logger, nameof(logger));

            CliManager = cliManager;
            Logger = logger;
        }

        public async Task<ImmutableDictionary<string, ImmutableDictionary<string, Command>>> ReadManifestAsync(string manifestFullPath)
            => await ExecuteWithDiagnostics(async () => await CliManager.ReadManifestAsync(manifestFullPath));

        public async Task ExecuteCommandAsync(string module, Command command)
        {
            await ExecuteWithDiagnostics(
                    action: async () =>
                    {
                        ExecuteWithLogger executeWithLogger = new ExecuteWithLogger(Logger);
                        await executeWithLogger.ExponentialBackoffAsync(
                            func: async () => await CliManager.ExecuteCommandAsync(module, command),
                            maxNumberOfRetries: 3);
                    });
        }

        public async Task<Module> ParseCommandFileAsync(string commandFileFullPath)
            => await ExecuteWithDiagnostics(async () => await CliManager.ParseCommandFileAsync(commandFileFullPath));

        public async Task<string> AuthenticateAsync(string username, string password)
            => await ExecuteWithDiagnostics(async () => await CliManager.AuthenticateAsync(username, password));

        private async Task ExecuteWithDiagnostics(Func<Task> action)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            Logger.LogInformation($"Start time: '{DateTime.Now}'");

            try
            {
                await action();
            }
            catch (Exception ex)
            {
                Logger.LogError("There was an error executing the action.", ex);
                throw ex;
            }
            finally
            {
                stopWatch.Start();
                Logger.LogInformation($"Elapsed time: '{stopWatch.Elapsed.TotalSeconds}' seconds.");
            }
        }

        private async Task<TResult> ExecuteWithDiagnostics<TResult>(Func<Task<TResult>> func)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            Logger.LogInformation($"Start time: '{DateTime.Now}'");

            try
            {
                return await func();
            }
            catch (Exception ex)
            {
                Logger.LogError("There was an error executing the function.", ex);
                throw ex;
            }
            finally
            {
                stopWatch.Start();
                Logger.LogInformation($"Elapsed time: '{stopWatch.Elapsed.TotalSeconds}' seconds.");
            }
        }
    }
}
