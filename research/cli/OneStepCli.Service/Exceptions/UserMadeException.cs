﻿/*
 * OneStep Relief Platform
 */

namespace OneStepCli.Service.Exceptions
{
    using System;

    public class UserMadeException : Exception
    {
        public ErrorCode ErrorCode { get; }
        public UserMadeException(string message, Exception innerException, ErrorCode errorCode) :
            base(message, innerException) 
        {
            ErrorCode = errorCode;
        }
    }
}
