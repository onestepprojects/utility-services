﻿/*
 * OneStep Relief Platform
 */

namespace OneStepCli.Service.Exceptions
{
    public enum ErrorCode
    {
        INVALID_JSON,
        FILE_ACCESS_ERROR
    }
}
