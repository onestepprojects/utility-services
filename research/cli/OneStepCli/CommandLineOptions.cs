﻿using CommandLine;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneStepCli
{
    public class CommandLineOptions
    {
        [Value(index: 0, Required = true, HelpText = "The manifest absolute path.")]
        public string ManifestPath { get; set; }

        [Option(shortName: 'm', longName : "module", Required = false, HelpText = "The module name, required when --file is not passed")]
        public string Module { get; set; }

        [Option(shortName: 'c', longName: "command", Required = false, HelpText = "The command to execute, required when --file is not passed")]
        public string Command { get; set; }

        [Option(shortName: 'b', longName: "body", Required = false, HelpText = "The command body (could be a JSON object or just a string value), required when --file is not passed")]
        public string Body { get; set; }

        [Option(shortName: 't', longName: "request-type", Required = false, HelpText = "The command body request type (could be a JSON or QUERY), required when --file is not passed")]
        public string RequestType { get; set; }

        [Option(shortName: 'f', longName: "file", Required = false, HelpText = "The command file, required when --module --command --body are not passed")]
        public string CommandFile { get; set; }
    }
}
