import { useState, useEffect } from 'react';
import Amplify, { Auth, Hub } from 'aws-amplify';

export function useIsAuthenticated() {
    const [user, setUser] = useState(null);
    useEffect(async () => {
        Hub.listen('auth', async ({ payload: { event, data } }) => {
            console.log(event);
            switch (event) {
            case 'signIn':
            case 'cognitoHostedUI':
                try {
                    var authenticateUser = await Auth.currentAuthenticatedUser();
                    setUser(authenticateUser);
                } catch (error) {
                    // Note(jcotillo): At this point user should be authenticated, getting an error
                    // means there could be another failure which we should log.
                    console.log(`Not signed in when it should, error: ${error}`);
                }
                break;
            case 'signOut':
                setUser(null);
                break;
            case 'signIn_failure':
            case 'cognitoHostedUI_failure':
                console.log('Sign in failure', data);
                break;
            }
        });
    });

    return user;
}