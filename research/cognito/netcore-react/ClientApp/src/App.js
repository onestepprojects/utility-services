import React, { useEffect, useState } from 'react';
import Amplify, { Auth, Hub } from 'aws-amplify';

Amplify.configure(
    {
        userPoolId: 'us-east-1_kCbtqfmSS',
        region: 'us-east-1',
        identityPoolRegion: 'us-east-1',
        userPoolWebClientId: 'v36bchpb2in1k7cb7l57lis98',
        oauth: {
            domain: 'onestep-user-pool-dev01.auth.us-east-1.amazoncognito.com',
            scope: [
                'phone',
                'email',
                'openid',
                'profile',
                'aws.cognito.signin.user.admin'
            ],
            redirectSignIn: 'http://localhost:3000/',
            redirectSignOut: 'http://localhost:3000/',
            responseType: 'code'
        },
    });

function App() {
    const [user, setUser] = useState(null);

    useEffect(() => {
        Hub.listen('auth', ({ payload: { event, data } }) => {
            console.log(`event is: ${event}`);
            switch (event) {
                case 'signIn':
                case 'cognitoHostedUI':
                    getUser().then(userData => setUser(userData));
                    break;
                case 'signOut':
                    setUser(null);
                    break;
                case 'signIn_failure':
                case 'cognitoHostedUI_failure':
                    console.log('Sign in failure', data);
                    break;
                default:
                    break;
            }
        });

        getUser().then(userData => setUser(userData));
    }, []);

    function getUser() {
        return Auth.currentAuthenticatedUser()
            .then(userData => {
                console.log(userData);
                return userData;
            })
            .catch(() => console.log('Not signed in'));
    }

    return (
        <div>
            <p>User: {user ? JSON.stringify(user.attributes) : 'None'}</p>
            {user ?
                (
                    <>
                        <p>Access Token:</p>
                        <p>{user ? JSON.stringify(user.signInUserSession.accessToken) : 'None'}</p>
                        <button onClick={() => Auth.signOut()}>Sign Out</button>
                        <div>
                            <a href='https://identity-onesteprelief-dev-dev.auth.us-west-2.amazoncognito.com/login?client_id=7281iajjv45hv39ti9u643s8hb&response_type=code&scope=aws.cognito.signin.user.admin+email+openid+phone+profile&redirect_uri=http://localhost:3000/'>Login Service</a>
                        </div>
                    </>
                ) :
                (
                    <button onClick={() => Auth.federatedSignIn()}>Federated Sign In</button>
                )
            }
        </div>
    );
}

export default App;