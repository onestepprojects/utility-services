#!/usr/bin/env python3
# pip install py-algorand-sdk
from algosdk.v2client import algod
from algosdk import mnemonic
from algosdk import transaction

import json
import time
import base64
import os

print("\n")
print("READ unsigned.txn")

# read from file
txns = transaction.retrieve_from_file("./unsigned.txn")
txn = txns[0]
print("\ntxid", ":", txn.get_txid())

print("\n")
for key in txn.__dict__:
    print(key, ":", txn.__dict__[key])
# print("\nNOTE", ":", txn.note.decode())
print("\n")
