#!/usr/bin/env python3
# https://developer.algorand.org/docs/features/transactions/offline_transactions/#unsigned-transaction-file-operations
# pip install py-algorand-sdk
from os import error
from algosdk.v2client import algod
from algosdk import transaction

print("\n")
print("SEND TRANSACTION signed.txn")

# utility for waiting on a transaction confirmation


def wait_for_confirmation(client, transaction_id, timeout):
    """
    Wait until the transaction is confirmed or rejected, or until 'timeout'
    number of rounds have passed.
    Args:
        transaction_id (str): the transaction to wait for
        timeout (int): maximum number of rounds to wait
    Returns:
        dict: pending transaction information, or throws an error if the transaction
            is not confirmed or rejected in the next timeout rounds
    """
    start_round = client.status()["last-round"] + 1
    current_round = start_round

    while current_round < start_round + timeout:
        try:
            pending_txn = client.pending_transaction_info(transaction_id)
        except Exception:
            return
        if pending_txn.get("confirmed-round", 0) > 0:
            return pending_txn
        elif pending_txn["pool-error"]:
            raise Exception(
                'pool error: {}'.format(pending_txn["pool-error"]))
        client.status_after_block(current_round)
        current_round += 1
    raise Exception(
        'pending tx not found in timeout rounds, timeout value = : {}'.format(timeout))


algod_token = ''
endpoint_address = 'https://testnet-algorand.api.purestake.io/ps2'
purestake_header = {'X-API-Key': 'NcAk1rREad5CXCTHl48XW8mVXZ8ZJBjSaNPhUaLD'}
algod_client = algod.AlgodClient(algod_token, endpoint_address, headers=purestake_header)
params = algod_client.suggested_params()

# read signed transaction from file
txns = transaction.retrieve_from_file("./signed.txn")
signed_txn = txns[0]

txid = signed_txn.transaction.get_txid()
print("\ntxid", ":", txid, "\n")

# send transaction to network
try:
    algod_client.send_transaction(signed_txn)
except Exception as e:
    print(e)

# wait for confirmation
try:
    confirmed_txn = wait_for_confirmation(algod_client, txid, 10)
    print(confirmed_txn)
except Exception as err:
    print(err)
