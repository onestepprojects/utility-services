# Manually Sign Transactions

https://www.purestake.com/blog/how-to-use-multisig-and-offline-keys-with-algorand/

Use algokey to sign transaction offline

to sign transaction:

```
./algokey sign --keyfile key.env --txfile unsigned.txn --outfile signed.txn
```

### Input 

```key.env``` : private key file

```unsigned.txn``` : unsigned transaction file

To create ```key.env``` , make a file ```mnemonic.env``` and paste in 25 word mnemonic, then run ```mnkey.sh```

### Output

```signed.txn``` : signed transaction file

<br>

# Manually create and send transactions

Use python3 to make and send transaction, requires algosdk ```pip install py-algorand-sdk ```

```./txmake.py``` : make unsigned transaction

```./txsend.py``` : send signed transaction
