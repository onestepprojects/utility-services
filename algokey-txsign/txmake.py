#!/usr/bin/env python3
# https://www.purestake.com/blog/algorand-python-sdk/
# https://developer.algorand.org/docs/features/transactions/offline_transactions/#unsigned-transaction-file-operations
#

# pip install py-algorand-sdk
from algosdk.v2client import algod
from algosdk import transaction
import os

print("\n")
print("MAKE TRANSACTION unsigned.txn")

# Setup client with PureStake key
algod_token = ''
endpoint_address = 'https://testnet-algorand.api.purestake.io/ps2'
purestake_header = {'X-API-Key': 'NcAk1rREad5CXCTHl48XW8mVXZ8ZJBjSaNPhUaLD'}
algod_client = algod.AlgodClient(algod_token, endpoint_address, headers=purestake_header)
params = algod_client.suggested_params()

# Transaction details
data = {
    "sender": "EDMLR6324JAKRDGOOWWAYRI2GNCC73FURTF6PLUWRAET4BFXRDCXNT4SKI",
    "receiver": "EDMLR6324JAKRDGOOWWAYRI2GNCC73FURTF6PLUWRAET4BFXRDCXNT4SKI",
    "amt": 1,
    "note": "very important sentence".encode(),
    "flat_fee": True,
    "fee": params.min_fee,
    "first": params.first,
    "last": params.last,
    "gh": params.gh,
}

txn = transaction.PaymentTxn(**data)
print("\ntxid", ":", txn.get_txid(), "\n")

for key in txn.__dict__:
    print(key, ":", txn.__dict__[key])

dir_path = os.path.dirname(os.path.realpath(__file__))
transaction.write_to_file([txn], dir_path + "/unsigned.txn")
print("\n")
